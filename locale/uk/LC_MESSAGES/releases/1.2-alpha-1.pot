# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.2-alpha-1.txt:3
# 77727607497946ef93a8a6b3e5256efc
msgid "Django 1.2 alpha 1 release notes"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:5
# 569b2f436fec4e9c88eef059db961c1e
msgid "January 5, 2010"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:7
# 0e644bb9fef94bfa820468036f58af52
msgid "Welcome to Django 1.2 alpha 1!"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:9
# b2a9c2f306974973bc5c435bd296b28d
msgid "This is the first in a series of preview/development releases leading up to the eventual release of Django 1.2, currently scheduled to take place in March 2010. This release is primarily targeted at developers who are interested in trying out new features and testing the Django codebase to help identify and resolve bugs prior to the final 1.2 release."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:15
# ccb940ba8d94460bb9107568af25be26
msgid "As such, this release is *not* intended for production use, and any such use is discouraged."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:20
# caf3e877e4104811a4083e44c4e5693b
msgid "Backwards-incompatible changes in 1.2"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:23
# 340d2d10866742b4b0c2c321e636f64c
msgid "CSRF Protection"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:25
# 580df4eb9dd84e178205a855618230c2
msgid "There have been large changes to the way that CSRF protection works, detailed in :doc:`the CSRF documentation </ref/contrib/csrf>`.  The following are the major changes that developers must be aware of:"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:29
# a2a42eefe3704640ae172d9f5a6ff57c
msgid "``CsrfResponseMiddleware`` and ``CsrfMiddleware`` have been deprecated, and **will be removed completely in Django 1.4**, in favor of a template tag that should be inserted into forms."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:33
# 7dda66f5fbba44a1908ed94f491d1579
msgid "All contrib apps use a ``csrf_protect`` decorator to protect the view. This requires the use of the ``csrf_token`` template tag in the template, so if you have used custom templates for contrib views, you MUST READ THE UPGRADE INSTRUCTIONS to fix those templates."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:38
#: ../../releases/1.2-alpha-1.txt:206
# 0356dfb42a4d471091d64fbbf81f07c4
# 8d9d930e96074b118e5977e7e088a6cc
msgid "Documentation removed"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:40
#: ../../releases/1.2-alpha-1.txt:208
# a4b059fc22e049faa93656cf00d9544a
# 68b88bad84c34fd9bb6ecdb91b94db7c
msgid "The upgrade notes have been removed in current Django docs. Please refer to the docs for Django 1.3 or older to find these instructions."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:43
# 12db7a1ebe824c51ab265451392e48ed
msgid "``CsrfViewMiddleware`` is included in :setting:`MIDDLEWARE_CLASSES` by default. This turns on CSRF protection by default, so that views that accept POST requests need to be written to work with the middleware. Instructions on how to do this are found in the CSRF docs."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:48
# c2ae8973b6c64346b3fba8a94d61ea2e
msgid "CSRF-related code has moved from ``contrib`` to ``core`` (with backwards compatible imports in the old locations, which are deprecated)."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:53
# 5cfdea56adbd403c90b3209d03bd4d82
msgid ":ttag:`if` tag changes"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:55
# a596bcc50ff24209a0477b8467957bd4
msgid "Due to new features in the :ttag:`if` template tag, it no longer accepts 'and', 'or' and 'not' as valid **variable** names.  Previously that worked in some cases even though these strings were normally treated as keywords.  Now, the keyword status is always enforced, and template code like ``{% if not %}`` or ``{% if and %}`` will throw a TemplateSyntaxError."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:62
# 8018b5098d5141458926f354120da7aa
msgid "``LazyObject``"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:64
# 69b0f17888ad43c59819a754cf4bb911
msgid "``LazyObject`` is an undocumented utility class used for lazily wrapping other objects of unknown type.  In Django 1.1 and earlier, it handled introspection in a non-standard way, depending on wrapped objects implementing a public method ``get_all_members()``. Since this could easily lead to name clashes, it has been changed to use the standard method, involving ``__members__`` and ``__dir__()``. If you used ``LazyObject`` in your own code, and implemented the ``get_all_members()`` method for wrapped objects, you need to make the following changes:"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:73
# 74547e17fe7648478d518e95f916d6af
msgid "If your class does not have special requirements for introspection (i.e. you have not implemented ``__getattr__()`` or other methods that allow for attributes not discoverable by normal mechanisms), you can simply remove the ``get_all_members()`` method.  The default implementation on ``LazyObject`` will do the right thing."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:79
# aba334d3e4574f6f99d149e8632bab6b
msgid "If you have more complex requirements for introspection, first rename the ``get_all_members()`` method to ``__dir__()``.  This is the standard method, from Python 2.6 onwards, for supporting introspection.  If you are require support for Python < 2.6, add the following code to the class::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:87
# 7b8ed85ff50744db898d91fa0f22c367
msgid "``__dict__`` on Model instances"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:89
# c947e02e2b814be1925d47f6929c95bb
msgid "Historically, the ``__dict__`` attribute of a model instance has only contained attributes corresponding to the fields on a model."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:92
# 894945257c6d476cbc04351ef176d907
msgid "In order to support multiple database configurations, Django 1.2 has added a ``_state`` attribute to object instances. This attribute will appear in ``__dict__`` for a model instance. If your code relies on iterating over __dict__ to obtain a list of fields, you must now filter the ``_state`` attribute of out ``__dict__``."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:99
# 9db33e30fd1749739443ab5ecafe38a9
msgid "``get_db_prep_*()`` methods on Field"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:101
# b942ea10c6b94f60b48c808b3f23a413
msgid "Prior to v1.2, a custom field had the option of defining several functions to support conversion of Python values into database-compatible values. A custom field might look something like::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:117
# 548f2e08b63343a68aa9221ae377aaa0
msgid "In 1.2, these three methods have undergone a change in prototype, and two extra methods have been introduced::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:138
# 69acc03d7eb54749932bf57b8bb40634
msgid "These changes are required to support multiple databases: ``get_db_prep_*`` can no longer make any assumptions regarding the database for which it is preparing. The ``connection`` argument now provides the preparation methods with the specific connection for which the value is being prepared."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:144
# 5a34cbc5f91346afab4e1b2e7424a207
msgid "The two new methods exist to differentiate general data preparation requirements, and requirements that are database-specific. The ``prepared`` argument is used to indicate to the database preparation methods whether generic value preparation has been performed. If an unprepared (i.e., ``prepared=False``) value is provided to the ``get_db_prep_*()`` calls, they should invoke the corresponding ``get_prep_*()`` calls to perform generic data preparation."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:152
# c22c23cc1088461597394b504c5fe759
msgid "Conversion functions has been provided which will transparently convert functions adhering to the old prototype into functions compatible with the new prototype. However, this conversion function will be removed in Django 1.4, so you should upgrade your Field definitions to use the new prototype."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:158
# 062fa1d75e4c4518a4e04755ba2e0a67
msgid "If your ``get_db_prep_*()`` methods made no use of the database connection, you should be able to upgrade by renaming ``get_db_prep_value()`` to ``get_prep_value()`` and ``get_db_prep_lookup()`` to ``get_prep_lookup()`. If you require database specific conversions, then you will need to provide an implementation ``get_db_prep_*`` that uses the ``connection`` argument to resolve database-specific values."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:167
# a641b570d5c745ac8fadec81d06668a3
msgid "Stateful template tags"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:169
# 74623b13815b47b383898869e3330936
msgid "Template tags that store rendering state on the node itself may experience problems if they are used with the new :ref:`cached template loader<template-loaders>`."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:173
# 8eeb162129f7493384e0e4a634503734
msgid "All of the built-in Django template tags are safe to use with the cached loader, but if you're using custom template tags that come from third party packages, or that you wrote yourself, you should ensure that the ``Node`` implementation for each tag is thread-safe. For more information, see :ref:`template tag thread safety considerations<template_tag_thread_safety>`."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:181
# 51355875d2f047ac89ac557c352aa2ec
msgid "Test runner exit status code"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:183
# 5b420278c225498499e46da7905b617a
msgid "The exit status code of the test runners (``tests/runtests.py`` and ``python manage.py test``) no longer represents the number of failed tests, since a failure of 256 or more tests resulted in a wrong exit status code.  The exit status code for the test runner is now 0 for success (no failing tests) and 1 for any number of test failures.  If needed, the number of test failures can be found at the end of the test runner's output."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:191
# e566f29e390e464f98d73d4b5cc78749
msgid "Features deprecated in 1.2"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:194
# 61dbf8d3a2504cb69922a034a49945b6
msgid "CSRF response rewriting middleware"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:196
# ebe29a48b4344745a7b3a0e3f79d4240
msgid "``CsrfResponseMiddleware``, the middleware that automatically inserted CSRF tokens into POST forms in outgoing pages, has been deprecated in favor of a template tag method (see above), and will be removed completely in Django 1.4. ``CsrfMiddleware``, which includes the functionality of ``CsrfResponseMiddleware`` and ``CsrfViewMiddleware`` has likewise been deprecated."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:203
# 1f6a935296d8444482d478fc7e5e3678
msgid "Also, the CSRF module has moved from contrib to core, and the old imports are deprecated, as described in the upgrading notes."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:212
# 4d7fea473f3f45489a297451dce3a860
msgid "``SMTPConnection``"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:214
# b15a84aecf994eb28888a1d4504af08e
msgid "The ``SMTPConnection`` class has been deprecated in favor of a generic Email backend API. Old code that explicitly instantiated an instance of an SMTPConnection::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:223
# 57d87a4e230a4210aec31911873f8827
msgid "should now call :meth:`~django.core.mail.get_connection()` to instantiate a generic email connection::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:231
# c135fa25c8a845fab44316c29738870b
msgid "Depending on the value of the :setting:`EMAIL_BACKEND` setting, this may not return an SMTP connection. If you explicitly require an SMTP connection with which to send email, you can explicitly request an SMTP connection::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:241
# 51ccaedf40714679a89edcb8cd9029fe
msgid "If your call to construct an instance of ``SMTPConnection`` required additional arguments, those arguments can be passed to the :meth:`~django.core.mail.get_connection()` call::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:248
# 0b6fa577d5984947b095515be128696d
msgid "Specifying databases"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:250
# db260372a321401ba4c33edf6e85879f
msgid "Prior to Django 1.1, Django used a number of settings to control access to a single database. Django 1.2 introduces support for multiple databases, and as a result, the way you define database settings has changed."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:254
# 32a218823db6497fb409410f0d3cd7ff
msgid "**Any existing Django settings file will continue to work as expected until Django 1.4.** Old-style database settings will be automatically translated to the new-style format."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:258
# 198561fc918c4e9aa8a49d676aa39dc6
msgid "In the old-style (pre 1.2) format, there were a number of ``DATABASE_`` settings at the top level of your settings file. For example::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:267
# c63f9b5c87514603802dabf29a71503f
msgid "These settings are now contained inside a dictionary named :setting:`DATABASES`. Each item in the dictionary corresponds to a single database connection, with the name ``'default'`` describing the default database connection. The setting names have also been shortened to reflect the fact that they are stored in a dictionary. The sample settings given previously would now be stored using::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:283
# 3d752891d15a49ef9a8984220549a98f
msgid "This affects the following settings:"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:286
# 406341c24ddc4ec582ba84cbe55d71b5
msgid "Old setting"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:286
# 9bedb08d8e504ecdac86b2d971907620
msgid "New Setting"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:288
# 1c0886dc21764e49ae7fda733f117b26
msgid "`DATABASE_ENGINE`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:288
# 364e71470ba14bd897a65f7931bbae3f
msgid ":setting:`ENGINE <DATABASE-ENGINE>`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:289
# dc1ae500e13242c38e5a9140b72e6a67
msgid "`DATABASE_HOST`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:289
# 90299ca630454edfa6a5252a924ca184
msgid ":setting:`HOST`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:290
# daeec08a380d4e18b9282478c49f95a8
msgid "`DATABASE_NAME`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:290
# b5e19f8b9a92405f96f9a7f969bbfe94
msgid ":setting:`NAME`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:291
# ae1c101510d44113a8a0f1a40f2e4594
msgid "`DATABASE_OPTIONS`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:291
# 9ed42951ddb34bef8a6e21cc09374292
msgid ":setting:`OPTIONS`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:292
# 9ea6619b480746e9a9256c8d82491417
msgid "`DATABASE_PASSWORD`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:292
# 99c2e8eaa69942e19042bc217ffca393
msgid ":setting:`PASSWORD`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:293
# dec2b75e7a894b15af0979bd8db7de0e
msgid "`DATABASE_PORT`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:293
# ea8ac9ce459d4c30983a4b36833da78a
msgid ":setting:`PORT`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:294
# a79362b38b6446a29b44eaaf09808d22
msgid "`DATABASE_USER`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:294
# dcb953d0adf6413cb3b9250f7e819c1b
msgid ":setting:`USER`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:295
# 092076b8eab54bfe97315c7b35c12196
msgid "`TEST_DATABASE_CHARSET`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:295
# 7829c506f32b47759b897e1faffd53f8
msgid ":setting:`TEST_CHARSET`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:296
# 52cca77edbf7465bb6d05f9048e9dc9a
msgid "`TEST_DATABASE_COLLATION`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:296
# a16da312a19c444fbb46c2fdb2ad3c5d
msgid ":setting:`TEST_COLLATION`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:297
# cb1db1f36cc74f9da56179d1bb41e415
msgid "`TEST_DATABASE_NAME`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:297
# bf5a1807a73a41649caf7eed9d395c6d
msgid ":setting:`TEST_NAME`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:300
# c4c034d6db74429a8f6fdd594a31ee56
msgid "These changes are also required if you have manually created a database connection using ``DatabaseWrapper()`` from your database backend of choice."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:303
# 1f3a8667d9db458a9bfc6ff3de420a1d
msgid "In addition to the change in structure, Django 1.2 removes the special handling for the built-in database backends. All database backends must now be specified by a fully qualified module name (i.e., ``django.db.backends.postgresql_psycopg2``, rather than just ``postgresql_psycopg2``)."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:310
# fab9c8707d9d412ab70edff36094e4e4
msgid "User Messages API"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:312
# cd63150252044d02a91b666fa454cdac
msgid "The API for storing messages in the user ``Message`` model (via ``user.message_set.create``) is now deprecated and will be removed in Django 1.4 according to the standard :doc:`release process </internals/release-process>`."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:316
# c9dc343f90f340339694253bb74923b9
msgid "To upgrade your code, you need to replace any instances of::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:320
# f3cee392002f4260bc33aa2a85b2e0cb
msgid "with the following::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:325
# de8f96a884fd44919df5d6036b7859ca
msgid "Additionally, if you make use of the method, you need to replace the following::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:331
# a87255aae4e547ef9c743459e161f050
msgid "with::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:337
# 4461c48dce02420c9638aa636c641371
msgid "For more information, see the full :doc:`messages documentation </ref/contrib/messages>`. You should begin to update your code to use the new API immediately."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:342
# c611ef9ad160432dadd5e4b4f49e5442
msgid "Date format helper functions"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:344
# 510638dffd4d496bafc57ad11c01e878
msgid "``django.utils.translation.get_date_formats()`` and ``django.utils.translation.get_partial_date_formats()`` have been deprecated in favor of the appropriate calls to ``django.utils.formats.get_format()`` which is locale aware when :setting:`USE_L10N` is set to ``True``, and falls back to default settings if set to ``False``."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:350
# a59cfc2aca9745c6b6fbadaf7ce83fa9
msgid "To get the different date formats, instead of writing::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:355
# b4c1126f2ce346d6889fd8cd077afd1f
msgid "use::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:363
# 092d2e78e42f49bcbe8de7b207013eb7
msgid "or, when directly formatting a date value::"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:368
# 9a75b018761d47c399f3e5d7f36cf74d
msgid "The same applies to the globals found in ``django.forms.fields``:"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:370
# 554eb6fab2274743a055502d6c2eac4a
msgid "``DEFAULT_DATE_INPUT_FORMATS``"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:371
# 802c3ca3cb0e46f2baa90cf51a7b99c1
msgid "``DEFAULT_TIME_INPUT_FORMATS``"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:372
# 1883155ca8874a81a025c911375f8b84
msgid "``DEFAULT_DATETIME_INPUT_FORMATS``"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:374
# 126ff406b9b344d0935e07dc48db978c
msgid "Use ``django.utils.formats.get_format()`` to get the appropriate formats."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:378
# 21882c59c6f446d698ceea03645f30b8
msgid "What's new in Django 1.2 alpha 1"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:380
# 0e8d5e5e975541bd9169af61c8ab952e
msgid "The following new features are present as of this alpha release; this release also marks the end of major feature development for the 1.2 release cycle. Some minor features will continue development until the 1.2 beta release, however."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:387
# 53b0f6e7b68d4ab2a19955c2220ef43f
msgid "CSRF support"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:389
# e7cf6cf6192b4130adc3e5795a0391aa
msgid "Django now has much improved protection against :doc:`Cross-Site Request Forgery (CSRF) attacks</ref/contrib/csrf>`. This type of attack occurs when a malicious Web site contains a link, a form button or some javascript that is intended to perform some action on your Web site, using the credentials of a logged-in user who visits the malicious site in their browser. A related type of attack, 'login CSRF', where an attacking site tricks a user's browser into logging into a site with someone else's credentials, is also covered."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:399
# 2e0a85a1420043d9a1b1ab2b5e45cc30
msgid "Email Backends"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:401
# 1f9e7dd98e8541a6950a9ce5feccbd03
msgid "You can now :ref:`configure the way that Django sends email <topic-email-backends>`. Instead of using SMTP to send all email, you can now choose a configurable email backend to send messages. If your hosting provider uses a sandbox or some other non-SMTP technique for sending mail, you can now construct an email backend that will allow Django's standard :doc:`mail sending methods</topics/email>` to use those facilities."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:409
# a04209a9bfb44aef8152c4d7036ea6ed
msgid "This also makes it easier to debug mail sending - Django ships with backend implementations that allow you to send email to a :ref:`file<topic-email-file-backend>`, to the :ref:`console<topic-email-console-backend>`, or to :ref:`memory<topic-email-memory-backend>` - you can even configure all email to be :ref:`thrown away<topic-email-dummy-backend>`."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:417
# 3816339992d4488ba637379ec0b56093
msgid "Messages Framework"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:419
# 906cbecdf9df4e7493b3ae9eb5f9f939
msgid "Django now includes a robust and configurable :doc:`messages framework </ref/contrib/messages>` with built-in support for cookie- and session-based messaging, for both anonymous and authenticated clients. The messages framework replaces the deprecated user message API and allows you to temporarily store messages in one request and retrieve them for display in a subsequent request (usually the next one)."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:427
# e544af9edf744496ba5a8a720b51d21f
msgid "Support for multiple databases"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:429
# 1a400b8fce1545e191256a6cd1c56855
msgid "Django 1.2 adds the ability to use :doc:`more than one database </topics/db/multi-db>` in your Django project. Queries can be issued at a specific database with the ``using()`` method on querysets; individual objects can be saved to a specific database by providing a ``using`` argument when you save the instance."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:436
# 12550e47c5244e3b8df82e3ff6f5d533
msgid "'Smart' if tag"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:438
# 3b4c55b509064c839a94c26f3474ea3c
msgid "The :ttag:`if` tag has been upgraded to be much more powerful.  First, support for comparison operators has been added. No longer will you have to type:"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:447
# 545441527fa949ebb9c43d3c69fa1f40
msgid "...as you can now do:"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:455
# 7f3ec36e02ab49918e33d7af2000fa26
msgid "The operators supported are ``==``, ``!=``, ``<``, ``>``, ``<=``, ``>=`` and ``in``, all of which work like the Python operators, in addition to ``and``, ``or`` and ``not`` which were already supported."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:459
# d29ccbeac0c449438cf19e277be20849
msgid "Also, filters may now be used in the ``if`` expression. For example:"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:470
# 09f6e1c8c88d4f09aabfa866d84e43f1
msgid "Template caching"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:472
# 720cc9ee329444788e3e51fbc9a43a56
msgid "In previous versions of Django, every time you rendered a template it would be reloaded from disk. In Django 1.2, you can use a :ref:`cached template loader <template-loaders>` to load templates once, then use the cached result for every subsequent render. This can lead to a significant performance improvement if your templates are broken into lots of smaller subtemplates (using the ``{% extends %}`` or ``{% include %}`` tags)."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:480
# 36fd879ae54941e58577b4cb6c3e0b82
msgid "As a side effect, it is now much easier to support non-Django template languages. For more details, see the :ref:`notes on supporting non-Django template languages<topic-template-alternate-language>`."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:485
# de34fdccb28f41eba60167ee0004da14
msgid "Natural keys in fixtures"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:487
# e0eb4cfdd98741668d176c6ec7fb0870
msgid "Fixtures can refer to remote objects using :ref:`topics-serialization-natural-keys`. This lookup scheme is an alternative to the normal primary-key based object references in a fixture, improving readability, and resolving problems referring to objects whose primary key value may not be predictable or known."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:494
# a076ce6c4c4348498dc8ef29ce550a6b
msgid "``BigIntegerField``"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:496
# 2d1268636d864d2480dff4cd8d0ba95b
msgid "Models can now use a 64 bit :class:`~django.db.models.BigIntegerField` type."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:499
# 6023f5b3e2f34deab413eed36b7aaada
msgid "Fast Failure for Tests"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:501
# 36222860b2fb49ad981acc2b4d70cc59
msgid "The :djadmin:`test` subcommand of ``django-admin.py``, and the ``runtests.py`` script used to run Django's own test suite, support a new ``--failfast`` option. When specified, this option causes the test runner to exit after encountering a failure instead of continuing with the test run.  In addition, the handling of ``Ctrl-C`` during a test run has been improved to trigger a graceful exit from the test run that reports details of the tests run before the interruption."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:509
# ac7d0874807b4a20a9e3465458566fa8
msgid "Improved localization"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:511
# 198ff69b0c0e4f45ab50abcdfda058f4
msgid "Django's :doc:`internationalization framework </topics/i18n/index>` has been expanded by locale aware formatting and form processing. That means, if enabled, dates and numbers on templates will be displayed using the format specified for the current locale. Django will also use localized formats when parsing data in forms. See :ref:`Format localization <format-localization>` for more details."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:519
# 359561c06ecb4a118dff902521c5bd89
msgid "Added ``readonly_fields`` to ``ModelAdmin``"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:521
# d2ee56d0979249bba6361429c2356e8a
msgid ":attr:`django.contrib.admin.ModelAdmin.readonly_fields` has been added to enable non-editable fields in add/change pages for models and inlines. Field and calculated values can be displayed alongside editable fields."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:526
# 7a12bbdd897b47ffbccaa11250eb9e93
msgid "Customizable syntax highlighting"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:528
# 2cd998e9f7074d218fde8fef4fa277f3
msgid "You can now use the ``DJANGO_COLORS`` environment variable to modify or disable the colors used by ``django-admin.py`` to provide :ref:`syntax highlighting <syntax-coloring>`."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:534
# 42a0a19389274430afd557666671e55e
msgid "The Django 1.2 roadmap"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:536
# 0d915eddf1be4be99864567547e2103e
msgid "Before the final Django 1.2 release, several other preview/development releases will be made available. The current schedule consists of at least the following:"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:540
# 0d9e8a52ced94675bf3efb8d62ab4fbe
msgid "Week of **January 26, 2010**: First Django 1.2 beta release. Final feature freeze for Django 1.2."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:543
# ecf5bbf598d74709908fbd7934f66a2c
msgid "Week of **March 2, 2010**: First Django 1.2 release candidate. String freeze for translations."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:546
# e14098f9c65f47e7bf133962e549f6f9
msgid "Week of **March 9, 2010**: Django 1.2 final release."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:548
# 47aac264cc21406daffd9e0a67c4d24e
msgid "If necessary, additional alpha, beta or release-candidate packages will be issued prior to the final 1.2 release. Django 1.2 will be released approximately one week after the final release candidate."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:554
# 3294a7614d214cc689ee67e3b71cc200
msgid "What you can do to help"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:556
# 76c84cbcc25f4c6c9a1bcad493ee6e0f
msgid "In order to provide a high-quality 1.2 release, we need your help. Although this alpha release is, again, *not* intended for production use, you can help the Django team by trying out the alpha codebase in a safe test environment and reporting any bugs or issues you encounter. The Django ticket tracker is the central place to search for open issues:"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:562
# ec6a7d9b22be4f5cbb0cb4996ec48511
msgid "https://code.djangoproject.com/timeline"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:564
# 3833f82b1ceb4fefb7b3866bce7bdc07
msgid "Please open new tickets if no existing ticket corresponds to a problem you're running into."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:567
# 30aec825cf0842db9d27c42edac8091a
msgid "Additionally, discussion of Django development, including progress toward the 1.2 release, takes place daily on the django-developers mailing list:"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:570
# f896503c45a54a9f8d499cdaa9cc2f85
msgid "http://groups.google.com/group/django-developers"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:572
# 5236bdd1472c47ab98b236eb58374581
msgid "... and in the ``#django-dev`` IRC channel on ``irc.freenode.net``. If you're interested in helping out with Django's development, feel free to join the discussions there."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:576
# 66b02a8c4f9b4a2794c45476e02c62a0
msgid "Django's online documentation also includes pointers on how to contribute to Django:"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:579
# 73569401424f4159830a675a7109d5cc
msgid ":doc:`How to contribute to Django </internals/contributing/index>`"
msgstr ""

#: ../../releases/1.2-alpha-1.txt:581
# 53b0e8b72f55460c9b4ae3d017f43203
msgid "Contributions on any level -- developing code, writing documentation or simply triaging tickets and helping to test proposed bugfixes -- are always welcome and appreciated."
msgstr ""

#: ../../releases/1.2-alpha-1.txt:585
# 2302527e413a473eae0dc6e3d90a815f
msgid "Development sprints for Django 1.2 will also be taking place at PyCon US 2010, on the dedicated sprint days (February 22 through 25), and anyone who wants to help out is welcome to join in, either in person at PyCon or virtually in the IRC channel or on the mailing list."
msgstr ""

