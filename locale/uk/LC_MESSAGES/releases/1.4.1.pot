# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.4.1.txt:3
# 07ae386277324769866084db457ac626
msgid "Django 1.4.1 release notes"
msgstr ""

#: ../../releases/1.4.1.txt:5
# eb8668b6602a4be5a3e65908c43b5529
msgid "*July 30, 2012*"
msgstr ""

#: ../../releases/1.4.1.txt:7
# 354e56cab3794188b13f64acd21745f1
msgid "This is the first security release in the Django 1.4 series, fixing several security issues in Django 1.4. Django 1.4.1 is a recommended upgrade for all users of Django 1.4."
msgstr ""

#: ../../releases/1.4.1.txt:11
# 118ae07e917449f9b9f885beae6bb678
msgid "For a full list of issues addressed in this release, see the `security advisory`_."
msgstr ""

