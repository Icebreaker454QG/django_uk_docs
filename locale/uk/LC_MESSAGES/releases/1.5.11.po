# 
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-08 21:09+0300\n"
"Last-Translator: Volodymyr Cherepanyak <chervol@gmail.com>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"Content-Transfer-Encoding: 8bit\n"" Content-Type: text/plain; charset=UTF-8\n"
"MIME-Version: 1.0\n"
"Language: uk\n"

# 4fad9080b9dd482d8a23265c23a1c28d
#: ../../releases/1.5.11.txt:3
msgid "Django 1.5.11 release notes"
msgstr ""

# 241c46cde32842d7b1de15fabd88b7b7
#: ../../releases/1.5.11.txt:5
msgid "*Under development*"
msgstr ""

# d5977e813b0947b7a4642109617481b7
#: ../../releases/1.5.11.txt:7
msgid ""
"Django 1.5.11 fixes a couple regressions in the 1.5.9 security release."
msgstr ""

# 764cb410687547559950df3e8fdbe566
#: ../../releases/1.5.11.txt:10
msgid "Bugfixes"
msgstr ""

#: ../../releases/1.5.11.txt:12
msgid ""
"Allowed related many-to-many fields to be referenced in the admin (`#23604 "
"<http://code.djangoproject.com/ticket/23604>`_)."
msgstr ""

#: ../../releases/1.5.11.txt:15
msgid ""
"Allowed inline and hidden references to admin fields (`#23431 "
"<http://code.djangoproject.com/ticket/23431>`_)."
msgstr ""
