# 
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"Last-Translator: Volodymyr Cherepanyak <chervol@gmail.com>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"Content-Transfer-Encoding: 8bit\n"" Content-Type: text/plain; charset=UTF-8\n"
"MIME-Version: 1.0\n"
"Language: uk\n"

# 1280d283091149fea7d4f79568f92c61
#: ../../releases/1.1-beta-1.txt:3
msgid "Django 1.1 beta 1 release notes"
msgstr ""

# 8d79a9569de3490595471f72f24f6856
#: ../../releases/1.1-beta-1.txt:5
msgid "March 23, 2009"
msgstr ""

# 57f88bbe39e045a79b088a69b47042e3
#: ../../releases/1.1-beta-1.txt:7
msgid "Welcome to Django 1.1 beta 1!"
msgstr ""

# e1788fae049f4adfb41f889205bf3bb2
#: ../../releases/1.1-beta-1.txt:9
msgid ""
"This is the second in a series of preview/development releases leading up to"
" the eventual release of Django 1.1, currently scheduled to take place in "
"April 2009. This release is primarily targeted at developers who are "
"interested in trying out new features and testing the Django codebase to "
"help identify and resolve bugs prior to the final 1.1 release."
msgstr ""

# ee237100ebf04216b2bb1c1e389e59a0
#: ../../releases/1.1-beta-1.txt:15
msgid ""
"As such, this release is *not* intended for production use, and any such use"
" is discouraged."
msgstr ""

# 7c8cdd3ba26a44b6a883d51ce50dda23
#: ../../releases/1.1-beta-1.txt:19
msgid "What's new in Django 1.1 beta 1"
msgstr ""

# 3ab1cd58abd54fb9a1d95ed0e4536b96
#: ../../releases/1.1-beta-1.txt:23
msgid ""
"The :doc:`1.1 alpha release notes </releases/1.1-alpha-1>`, which has a list"
" of everything new between Django 1.0 and Django 1.1 alpha."
msgstr ""

# 053df441d31242b6960b7d7d666c8715
#: ../../releases/1.1-beta-1.txt:27
msgid "Model improvements"
msgstr ""

# 8e30912fb8a94620b91bcebbc96ba1ee
#: ../../releases/1.1-beta-1.txt:31
msgid "A number of features have been added to Django's model layer:"
msgstr ""

# 368b4806641943acab534ffd717e5c3e
#: ../../releases/1.1-beta-1.txt:34
msgid "\"Unmanaged\" models"
msgstr ""

# ddd4c185c9c7420db9d0a0d006c2c3df
#: ../../releases/1.1-beta-1.txt:36
msgid ""
"You can now control whether or not Django creates database tables for a "
"model using the :attr:`~Options.managed` model option. This defaults to "
"``True``, meaning that Django will create the appropriate database tables in"
" :djadmin:`syncdb` and remove them as part of ``reset`` command. That is, "
"Django *manages* the database table's lifecycle."
msgstr ""

# 2c10d03a90cc487b950a90f2dcf23f0a
#: ../../releases/1.1-beta-1.txt:42
msgid ""
"If you set this to ``False``, however, no database table creating or "
"deletion will be automatically performed for this model. This is useful if "
"the model represents an existing table or a database view that has been "
"created by some other means."
msgstr ""

# 11deab368e7047bcb0cd8a966e09b199
#: ../../releases/1.1-beta-1.txt:47
msgid ""
"For more details, see the documentation for the :attr:`~Options.managed` "
"option."
msgstr ""

# 40877bfa237441cd88bc9c2f9442d543
#: ../../releases/1.1-beta-1.txt:51
msgid "Proxy models"
msgstr ""

# 3a9f3413ec134701ac77b36a228adf3e
#: ../../releases/1.1-beta-1.txt:53
msgid ""
"You can now create :ref:`proxy models <proxy-models>`: subclasses of "
"existing models that only add Python behavior and aren't represented by a "
"new table. That is, the new model is a *proxy* for some underlying model, "
"which stores all the real data."
msgstr ""

# 73b0f00f19b543cb9db2065d60433a21
#: ../../releases/1.1-beta-1.txt:58
msgid ""
"All the details can be found in the :ref:`proxy models documentation <proxy-"
"models>`. This feature is similar on the surface to unmanaged models, so the"
" documentation has an explanation of :ref:`how proxy models differ from "
"unmanaged models <proxy-vs-unmanaged-models>`."
msgstr ""

# f04f859665e44b119d2fc78635029cfe
#: ../../releases/1.1-beta-1.txt:64
msgid "Deferred fields"
msgstr ""

# a83d306586be4fcca0665b2504da1140
#: ../../releases/1.1-beta-1.txt:66
msgid ""
"In some complex situations, your models might contain fields which could "
"contain a lot of data (for example, large text fields), or require expensive"
" processing to convert them to Python objects. If you know you don't need "
"those particular fields, you can now tell Django not to retrieve them from "
"the database."
msgstr ""

# ecee395d0a31465782fdc34fdb3eeaa0
#: ../../releases/1.1-beta-1.txt:72
msgid ""
"You'll do this with the new queryset methods "
":meth:`~django.db.models.query.QuerySet.defer` and "
":meth:`~django.db.models.query.QuerySet.only`."
msgstr ""

# d30c7fd20b6e40f49c558bbb23e88075
#: ../../releases/1.1-beta-1.txt:77
msgid "New admin features"
msgstr ""

# bf0dd78707cc40d5844fe21fcbc0ecd1
#: ../../releases/1.1-beta-1.txt:79
msgid ""
"Since 1.1 alpha, a couple of new features have been added to Django's admin "
"application:"
msgstr ""

# 999451fb45514527a06436549afda2bf
#: ../../releases/1.1-beta-1.txt:83
msgid "Editable fields on the change list"
msgstr ""

# 610e89a97b1a40bb8d69e08c77a39eda
#: ../../releases/1.1-beta-1.txt:85
msgid ""
"You can now make fields editable on the admin list views via the new "
":ref:`list_editable <admin-list-editable>` admin option. These fields will "
"show up as form widgets on the list pages, and can be edited and saved in "
"bulk."
msgstr ""

# e03e279743954922ade1e03311e63830
#: ../../releases/1.1-beta-1.txt:90
msgid "Admin \"actions\""
msgstr ""

# 0635422a0e854bd0b7a572fe8cf3b988
#: ../../releases/1.1-beta-1.txt:92
msgid ""
"You can now define :doc:`admin actions </ref/contrib/admin/actions>` that "
"can perform some action to a group of models in bulk. Users will be able to "
"select objects on the change list page and then apply these bulk actions to "
"all selected objects."
msgstr ""

# 2acddde4ec8746828883f7b68f16e3b7
#: ../../releases/1.1-beta-1.txt:96
msgid ""
"Django ships with one pre-defined admin action to delete a group of objects "
"in one fell swoop."
msgstr ""

# 96ea43c044f54becae613d4745f59fee
#: ../../releases/1.1-beta-1.txt:100
msgid "Testing improvements"
msgstr ""

# ac6ef6f14aa04367a2f63d61ef8134b1
#: ../../releases/1.1-beta-1.txt:104
msgid ""
"A couple of small but very useful improvements have been made to the "
":doc:`testing framework </topics/testing/index>`:"
msgstr ""

# 8212552fb79a43f5a17430293d683708
#: ../../releases/1.1-beta-1.txt:107
msgid ""
"The test :class:`Client` now can automatically follow redirects with the "
"``follow`` argument to :meth:`Client.get` and :meth:`Client.post`. This "
"makes testing views that issue redirects simpler."
msgstr ""

# baac3c4bfb6444e7a0f38df3d27b4c50
#: ../../releases/1.1-beta-1.txt:111
msgid ""
"It's now easier to get at the template context in the response returned the "
"test client: you'll simply access the context as ``request.context[key]``. "
"The old way, which treats ``request.context`` as a list of contexts, one for"
" each rendered template, is still available if you need it."
msgstr ""

# 7481f435fb6842bbb2e25f79eda69740
#: ../../releases/1.1-beta-1.txt:118
msgid "Conditional view processing"
msgstr ""

# 6f6d68ba36f14cc99fb6bb4f88aab525
#: ../../releases/1.1-beta-1.txt:120
msgid ""
"Django now has much better support for :doc:`conditional view processing "
"</topics/conditional-view-processing>` using the standard ``ETag`` and "
"``Last-Modified`` HTTP headers. This means you can now easily short-circuit "
"view processing by testing less-expensive conditions. For many views this "
"can lead to a serious improvement in speed and reduction in bandwidth."
msgstr ""

# 050a92c5b4ea4032b25c849ae583e4fe
#: ../../releases/1.1-beta-1.txt:127
msgid "Other improvements"
msgstr ""

# 10ebde9932624dfa95ca32f324af65e6
#: ../../releases/1.1-beta-1.txt:129
msgid ""
"Finally, a grab-bag of other neat features made their way into this beta "
"release, including:"
msgstr ""

# 988b21b487c1480aa660548b9bc3bb19
#: ../../releases/1.1-beta-1.txt:132
msgid ""
"The :djadmin:`dumpdata` management command now accepts individual model "
"names as arguments, allowing you to export the data just from particular "
"models."
msgstr ""

# d19bac33f9d546af8ff11bf0ec30e4a4
#: ../../releases/1.1-beta-1.txt:136
msgid ""
"There's a new :tfilter:`safeseq` template filter which works just like "
":tfilter:`safe` for lists, marking each item in the list as safe."
msgstr ""

# 85585c9f674340829015f082a2d1b727
#: ../../releases/1.1-beta-1.txt:139
msgid ""
":doc:`Cache backends </topics/cache>` now support ``incr()`` and ``decr()`` "
"commands to increment and decrement the value of a cache key. On cache "
"backends that support atomic increment/decrement -- most notably, the "
"memcached backend -- these operations will be atomic, and quite fast."
msgstr ""

# 2018a0c461304660b2a4b6ae01f5dbf7
#: ../../releases/1.1-beta-1.txt:145
msgid ""
"Django now can :doc:`easily delegate authentication to the Web server "
"</howto/auth-remote-user>` via a new authentication backend that supports "
"the standard ``REMOTE_USER`` environment variable used for this purpose."
msgstr ""

# 70e84eca801342efa45c35b253ba36e7
#: ../../releases/1.1-beta-1.txt:149
msgid ""
"There's a new :func:`django.shortcuts.redirect` function that makes it "
"easier to issue redirects given an object, a view name, or a URL."
msgstr ""

# 4b6613dbed684bf0963bf65018384a51
#: ../../releases/1.1-beta-1.txt:152
msgid ""
"The ``postgresql_psycopg2`` backend now supports :ref:`native PostgreSQL "
"autocommit <postgresql-notes>`. This is an advanced, PostgreSQL-specific "
"feature, that can make certain read-heavy applications a good deal faster."
msgstr ""

# 6d8a4a46616d48448b2efbdad03bb136
#: ../../releases/1.1-beta-1.txt:158
msgid "The Django 1.1 roadmap"
msgstr ""

# 006d9f25f5e1408485932f52ac0e6145
#: ../../releases/1.1-beta-1.txt:160
msgid ""
"Before Django 1.1 goes final, at least one other preview/development release"
" will be made available. The current schedule consists of at least the "
"following:"
msgstr ""

# 12c772e001414cd482f90a9e19bf5998
#: ../../releases/1.1-beta-1.txt:164
msgid ""
"Week of *April 2, 2009:* Django 1.1 release candidate. At this point all "
"strings marked for translation must freeze to allow translations to be "
"submitted in advance of the final release."
msgstr ""

# 6063f490bede4a85a12c9ec0c7b5dd2e
#: ../../releases/1.1-beta-1.txt:168
msgid "Week of *April 13, 2009:* Django 1.1 final."
msgstr ""

# 81b0a01c23ae446da9f7108227ab5b61
#: ../../releases/1.1-beta-1.txt:170
msgid ""
"If deemed necessary, additional beta or release candidate packages will be "
"issued prior to the final 1.1 release."
msgstr ""

# 5400fa4fb2044e8ba269be56b9c60f5f
#: ../../releases/1.1-beta-1.txt:174
msgid "What you can do to help"
msgstr ""

# 9b739ad570a24d7088aa7bddb683f5d1
#: ../../releases/1.1-beta-1.txt:176
msgid ""
"In order to provide a high-quality 1.1 release, we need your help. Although "
"this beta release is, again, *not* intended for production use, you can help"
" the Django team by trying out the beta codebase in a safe test environment "
"and reporting any bugs or issues you encounter. The Django ticket tracker is"
" the central place to search for open issues:"
msgstr ""

# 5221eb369ae64854bc8ddeef4593b8d9
#: ../../releases/1.1-beta-1.txt:182
msgid "https://code.djangoproject.com/timeline"
msgstr ""

# fe60129458464044809d04edd61d246a
#: ../../releases/1.1-beta-1.txt:184
msgid ""
"Please open new tickets if no existing ticket corresponds to a problem "
"you're running into."
msgstr ""

# ccfe69dbe1ae436eb8b4be4564305b10
#: ../../releases/1.1-beta-1.txt:187
msgid ""
"Additionally, discussion of Django development, including progress toward "
"the 1.1 release, takes place daily on the django-developers mailing list:"
msgstr ""

# eb854f1482744e0691218bb675a9415b
#: ../../releases/1.1-beta-1.txt:190
msgid "http://groups.google.com/group/django-developers"
msgstr ""

# a1bc18d40abc48719b9a43655ce983c5
#: ../../releases/1.1-beta-1.txt:192
msgid ""
"... and in the ``#django-dev`` IRC channel on ``irc.freenode.net``. If "
"you're interested in helping out with Django's development, feel free to "
"join the discussions there."
msgstr ""

# 65469e63736346a6a4c0ef996c31b587
#: ../../releases/1.1-beta-1.txt:196
msgid ""
"Django's online documentation also includes pointers on how to contribute to"
" Django:"
msgstr ""

# ae78ac97e4bd433fb3c2dad14fed8513
#: ../../releases/1.1-beta-1.txt:199
msgid ":doc:`How to contribute to Django </internals/contributing/index>`"
msgstr ""

# bc9043c9d429405592105b47fb5fb8ef
#: ../../releases/1.1-beta-1.txt:201
msgid ""
"Contributions on any level -- developing code, writing documentation or "
"simply triaging tickets and helping to test proposed bugfixes -- are always "
"welcome and appreciated."
msgstr ""

# 69899c65b02d4eab804fba508615f794
#: ../../releases/1.1-beta-1.txt:205
msgid ""
"Development sprints for Django 1.1 will also be taking place at PyCon US "
"2009, on the dedicated sprint days (March 30 through April 2), and anyone "
"who wants to help out is welcome to join in, either in person at PyCon or "
"virtually in the IRC channel or on the mailing list."
msgstr ""
