# 
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"Last-Translator: Volodymyr Cherepanyak <chervol@gmail.com>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"Content-Transfer-Encoding: 8bit\n"" Content-Type: text/plain; charset=UTF-8\n"
"MIME-Version: 1.0\n"
"Language: uk\n"

# add546cf46b84550a96d6ed5d183ba25
#: ../../releases/1.4.10.txt:3
msgid "Django 1.4.10 release notes"
msgstr ""

# 231eaf7635f6444f9ae7a109601e3431
#: ../../releases/1.4.10.txt:5
msgid "*November 6, 2013*"
msgstr ""

# 1de43fcef5e44f4ebb60d68e8163b51e
#: ../../releases/1.4.10.txt:7
msgid "Django 1.4.10 fixes a Python-compatibility bug in the 1.4 series."
msgstr ""

# 4d1462672f7949dab37270c720bc4529
#: ../../releases/1.4.10.txt:10
msgid "Python compatibility"
msgstr ""

# 3a50b3e83f514643bf0aea6878bfe5d1
#: ../../releases/1.4.10.txt:12
msgid ""
"Django 1.4.9 inadvertently introduced issues with Python 2.5 compatibility. "
"Django 1.4.10 restores Python 2.5 compatibility. This was issue #21362 in "
"Django's Trac."
msgstr ""
