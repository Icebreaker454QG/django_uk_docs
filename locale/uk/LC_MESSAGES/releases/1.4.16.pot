# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.8\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-08 21:09+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.4.16.txt:3
msgid "Django 1.4.16 release notes"
msgstr ""

#: ../../releases/1.4.16.txt:5
msgid "*Under development*"
msgstr ""

#: ../../releases/1.4.16.txt:7
msgid ""
"Django 1.4.16 fixes a couple regressions in the 1.4.14 security release and "
"a bug preventing the use of some GEOS versions with GeoDjango."
msgstr ""

#: ../../releases/1.4.16.txt:11
msgid "Bugfixes"
msgstr ""

#: ../../releases/1.4.16.txt:13
msgid ""
"Allowed related many-to-many fields to be referenced in the admin (`#23604 "
"<http://code.djangoproject.com/ticket/23604>`_)."
msgstr ""

#: ../../releases/1.4.16.txt:16
msgid ""
"Allowed inline and hidden references to admin fields (`#23431 "
"<http://code.djangoproject.com/ticket/23431>`_)."
msgstr ""

#: ../../releases/1.4.16.txt:19
msgid ""
"Fixed parsing of the GEOS version string (`#20036 "
"<http://code.djangoproject.com/ticket/20036>`_)."
msgstr ""
