# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.6.4.txt:3
# 727e2bbca3d640fb8a210e50a85fbd1f
msgid "Django 1.6.4 release notes"
msgstr ""

#: ../../releases/1.6.4.txt:5
# 462f48c434ca4882ad0fe5ed62e7a1d7
msgid "*April 28, 2014*"
msgstr ""

#: ../../releases/1.6.4.txt:7
# 9921541aa6f044c4a95a70fa8ef29bf8
msgid "Django 1.6.4 fixes several bugs in 1.6.3."
msgstr ""

#: ../../releases/1.6.4.txt:10
# de7725edfc1e479da203d4e839f2c441
msgid "Bugfixes"
msgstr ""

#: ../../releases/1.6.4.txt:12
# b314ed3f5d1640279984dfa06221c232
msgid "Added backwards compatibility support for the :mod:`django.contrib.messages` cookie format of Django 1.4 and earlier to facilitate upgrading to 1.6 from 1.4 (`#22426 <http://code.djangoproject.com/ticket/22426>`_)."
msgstr ""

#: ../../releases/1.6.4.txt:16
# e336ce2729d44e858ed07c953ed9d847
msgid "Restored the ability to :meth:`~django.core.urlresolvers.reverse` views created using :func:`functools.partial()` (`#22486 <http://code.djangoproject.com/ticket/22486>`_)."
msgstr ""

#: ../../releases/1.6.4.txt:20
# 54cc627784664746a7a3158ba4261b77
msgid "Fixed the ``object_id`` of the ``LogEntry`` that's created after a user password change in the admin (`#22515 <http://code.djangoproject.com/ticket/22515>`_)."
msgstr ""

