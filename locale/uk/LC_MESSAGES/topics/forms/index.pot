# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../topics/forms/index.txt:3
# 04cfbd797ec14d4a9e05050443904dca
msgid "Working with forms"
msgstr ""

#: ../../topics/forms/index.txt:7
# c2a8c0dc0b1e447dbc7fe03af2269cb2
msgid "About this document"
msgstr ""

#: ../../topics/forms/index.txt:9
# 4b25347b0cee4b17965143c9ddcebe29
msgid "This document provides an introduction to the basics of web forms and how they are handled in Django. For a more detailed look at specific areas of the forms API, see :doc:`/ref/forms/api`, :doc:`/ref/forms/fields`, and :doc:`/ref/forms/validation`."
msgstr ""

#: ../../topics/forms/index.txt:14
# a301e63e68784fddb52e635bd6c08da8
msgid "Unless you're planning to build Web sites and applications that do nothing but publish content, and don't accept input from your visitors, you're going to need to understand and use forms."
msgstr ""

#: ../../topics/forms/index.txt:18
# f1f147c5762c4a468e1188d0676efa47
msgid "Django provides a range of tools and libraries to help you build forms to accept input from site visitors, and then process and respond to the input."
msgstr ""

#: ../../topics/forms/index.txt:22
# 61a81480760b48aeaaff799b6439156c
msgid "HTML forms"
msgstr ""

#: ../../topics/forms/index.txt:24
# 519a0e6bcae9427192a44d65119d01ad
msgid "In HTML, a form is a collection of elements inside ``<form>...</form>`` that allow a visitor to do things like enter text, select options, manipulate objects or controls, and so on, and then send that information back to the server."
msgstr ""

#: ../../topics/forms/index.txt:29
# eb2f26ef31244220bcd73e8915080a22
msgid "Some of these form interface elements - text input or checkboxes - are fairly simple and built-in to HTML itself. Others are much more complex; an interface that pops up a date picker or allows you to move a slider or manipulate controls will typically use JavaScript and CSS as well as HTML form ``<input>`` elements to achieve these effects."
msgstr ""

#: ../../topics/forms/index.txt:35
# 33a50fb83bdc49d1ae1426ce806994a0
msgid "As well as its ``<input>`` elements, a form must specify two things:"
msgstr ""

#: ../../topics/forms/index.txt:37
# 0266522fc62545cd81b58eae25d67763
msgid "*where*: the URL to which the data corresponding to the user's input should be returned"
msgstr ""

#: ../../topics/forms/index.txt:40
# 1c0105ce204e414981965b5a0875e70d
msgid "*how*: the HTTP method the data should be returned by"
msgstr ""

#: ../../topics/forms/index.txt:42
# 1134384e61b34ee8bfc9ed866f8f48cb
msgid "As an example, the login form for the Django admin contains several ``<input>`` elements: one of ``type=\"text\"`` for the username, one of ``type=\"password\"`` for the password, and one of ``type=\"submit\"`` for the \"Log in\" button. It also contains some hidden text fields that the user doesn't see, which Django uses to determine what to do next."
msgstr ""

#: ../../topics/forms/index.txt:48
# 59629e32c5dc43ba9238fa896a1cd7d2
msgid "It also tells the browser that the form data should be sent to the URL specified in the ``<form>``’s ``action`` attribute - ``/admin/`` - and that it should be sent using the HTTP mechanism specified by the ``method`` attribute - ``post``."
msgstr ""

#: ../../topics/forms/index.txt:53
# 3e497f1c1c7b41898c7cbfed98b79385
msgid "When the ``<input type=\"submit\" value=\"Log in\">`` element is triggered, the data is returned to ``/admin/``."
msgstr ""

#: ../../topics/forms/index.txt:57
# 8189ef68fa7c4248aff71056701d83b6
msgid "``GET`` and ``POST``"
msgstr ""

#: ../../topics/forms/index.txt:59
# 971ca328565c46709f72eb2206a04794
msgid "``GET`` and ``POST`` are the only HTTP methods to use when dealing with forms."
msgstr ""

#: ../../topics/forms/index.txt:61
# 45831d83e0734ba49baa1da9f16328e7
msgid "Django's login form is returned using the ``POST`` method, in which the browser bundles up the form data, encodes it for transmission, sends it back to the server, and then receives its response."
msgstr ""

#: ../../topics/forms/index.txt:65
# 1f40221857c44c768675df1c8ec86844
msgid "``GET``, by contrast, bundles the submitted data into a string, and uses this to compose a URL. The URL contains the address where the data must be sent, as well as the data keys and values. You can see this in action if you do a search in the Django documentation, which will produce a URL of the form ``https://docs.djangoproject.com/search/?q=forms&release=1``."
msgstr ""

#: ../../topics/forms/index.txt:71
# ff903e97edfd4cfe9adf687559bfd6c3
msgid "``GET`` and ``POST`` are typically used for different purposes."
msgstr ""

#: ../../topics/forms/index.txt:73
# 08bd5b5520b34b289431c6527fe73b11
msgid "Any request that could be used to change the state of the system - for example, a request that makes changes in the database - should use ``POST``. ``GET`` should be used only for requests that do not affect the state of the system."
msgstr ""

#: ../../topics/forms/index.txt:77
# fc26e224ac094046a9ecb6653bdfab22
msgid "``GET`` would also be unsuitable for a password form, because the password would appear in the URL, and thus, also in browser history and server logs, all in plain text. Neither would it be suitable for large quantities of data, or for binary data, such as an image. A Web application that uses ``GET`` requests for admin forms is a security risk: it can be easy for an attacker to mimic a form's request to gain access to sensitive parts of the system. ``POST``, coupled with other protections like Django's :doc:`CSRF protection </ref/contrib/csrf/>` offers more control over access."
msgstr ""

#: ../../topics/forms/index.txt:86
# cd6016ce9a1540a6b39a7a05c6a4e765
msgid "On the other hand, ``GET`` is suitable for things like a web search form, because the URLs that represent a ``GET`` request can easily be bookmarked, shared, or resubmitted."
msgstr ""

#: ../../topics/forms/index.txt:91
# accd4518972f4bf8b137b010eeada322
msgid "Django's role in forms"
msgstr ""

#: ../../topics/forms/index.txt:93
# 74e74b78762b4652856fec7cd01520c0
msgid "Handling forms is a complex business. Consider Django's admin, where numerous items of data of several different types may need to be prepared for display in a form, rendered as HTML, edited using a convenient interface, returned to the server, validated and cleaned up, and then saved or passed on for further processing."
msgstr ""

#: ../../topics/forms/index.txt:99
# eb45cbe2fa2d45d8a460b4cad35be933
msgid "Django's form functionality can simplify and automate vast portions of this work, and can also do it more securely than most programmers would be able to do in code they wrote themselves."
msgstr ""

#: ../../topics/forms/index.txt:103
# 285cae8d80b84271ae3bd01563d82bf4
msgid "Django handles three distinct parts of the work involved in forms:"
msgstr ""

#: ../../topics/forms/index.txt:105
# 886e12833aff48be94ec996cbf1c6262
msgid "preparing and restructuring data ready for rendering"
msgstr ""

#: ../../topics/forms/index.txt:106
# ebe5f39e93ad4b35b10bad891a7b6984
msgid "creating HTML forms for the data"
msgstr ""

#: ../../topics/forms/index.txt:107
# 303e605bf9ba4da5a929dec15d9682a3
msgid "receiving and processing submitted forms and data from the client"
msgstr ""

#: ../../topics/forms/index.txt:109
# f9c4441816ef4960858dff183cd3c8bf
msgid "It is *possible* to write code that does all of this manually, but Django can take care of it all for you."
msgstr ""

#: ../../topics/forms/index.txt:113
# e8cdb978da71403e98516725d81a53ef
msgid "Forms in Django"
msgstr ""

#: ../../topics/forms/index.txt:115
# 4ce00bd030954082af370933e359d30b
msgid "We've described HTML forms briefly, but an HTML ``<form>`` is just one part of the machinery required."
msgstr ""

#: ../../topics/forms/index.txt:118
# eed173f5c4ba44d1823fb4d9e16520a2
msgid "In the context of a Web application, 'form' might refer to that HTML ``<form>``, or to the Django :class:`Form` that produces it, or to the structured data returned when it is submitted, or to the end-to-end working collection of these parts."
msgstr ""

#: ../../topics/forms/index.txt:124
# 122a506772bd4070ba63133daeeefb8d
msgid "The Django :class:`Form` class"
msgstr ""

#: ../../topics/forms/index.txt:126
# edbbd10b9c714c99a5c661b92f0bf9ef
msgid "At the heart of this system of components is Django's :class:`Form` class. In much the same way that a Django model describes the logical structure of an object, its behavior, and the way its parts are represented to us, a :class:`Form` class describes a form and determines how it works and appears."
msgstr ""

#: ../../topics/forms/index.txt:131
# 7f4534c759ee4b8180e90edafddc817f
msgid "In a similar way that a model class's fields map to database fields, a form class's fields map to HTML form ``<input>`` elements. (A :class:`ModelForm` maps a model class's fields to HTML form ``<input>`` elements via a :class:`Form`; this is what the Django admin is based upon.)"
msgstr ""

#: ../../topics/forms/index.txt:136
# cbf93e0a977d4f4db457d17a0c26b35d
msgid "A form's fields are themselves classes; they manage form data and perform validation when a form is submitted. A :class:`DateField` and a :class:`FileField` handle very different kinds of data and have to do different things with it."
msgstr ""

#: ../../topics/forms/index.txt:141
# 3bc5d8ed738343c1a9b5e4668c192818
msgid "A form field is represented to a user in the browser as a HTML \"widget\" - a piece of user interface machinery. Each field type has an appropriate default :doc:`Widget class </ref/forms/widgets/>`, but these can be overridden as required."
msgstr ""

#: ../../topics/forms/index.txt:147
# 5c428fc71ae84d239dfc7d19e1988b89
msgid "Instantiating, processing, and rendering forms"
msgstr ""

#: ../../topics/forms/index.txt:149
# 11d3380e626a456c9293927ce6c9e04c
msgid "When rendering an object in Django, we generally:"
msgstr ""

#: ../../topics/forms/index.txt:151
# 2dc601359ec64c93a7f089111e1a9b94
msgid "get hold of it in the view (fetch it from the database, for example)"
msgstr ""

#: ../../topics/forms/index.txt:152
# 9a8e452f9a2448488dfb9e9509531dfd
msgid "pass it to the template context"
msgstr ""

#: ../../topics/forms/index.txt:153
# 297c3048443c4d72b944a9949310f5ab
msgid "expand it to HTML markup using template variables"
msgstr ""

#: ../../topics/forms/index.txt:155
# 7a0e9d438ae6457d8bb8392a7c40e7f9
msgid "Rendering a form in a template involves nearly the same work as rendering any other kind of object, but there are some key differences."
msgstr ""

#: ../../topics/forms/index.txt:158
# a2fe11b2881a4e4ebcc7b608f1e8513f
msgid "In the case of a model instance that contained no data, it would rarely if ever be useful to do anything with one in a template. On the other hand, it makes perfect sense to render an unpopulated form - that's what we do when we want the user to populate it."
msgstr ""

#: ../../topics/forms/index.txt:163
# ca485e0af60443b2947ad6ee9eb43fa3
msgid "So when we handle a model instance in a view, we typically retrieve it from the database. When we're dealing with a form we typically instantiate it in the view."
msgstr ""

#: ../../topics/forms/index.txt:167
# 05f4ac7f9f014b9b9c64f58b52890780
msgid "When we instantiate a form, we can opt to leave it empty or pre-populate it, for example with:"
msgstr ""

#: ../../topics/forms/index.txt:170
# 01d8578166bd42bda0bf41ae6fb1c70b
msgid "data from a saved model instance (as in the case of admin forms for editing)"
msgstr ""

#: ../../topics/forms/index.txt:171
# 20bb80ce7b6d47f1b1463e2213580a0f
msgid "data that we have collated from other sources"
msgstr ""

#: ../../topics/forms/index.txt:172
# 8b69ee413f1a4ac8860da7ef5c8c476a
msgid "data received from a previous HTML form submission"
msgstr ""

#: ../../topics/forms/index.txt:174
# 1576c933ba4c46d39b2ea55df2f6ee3e
msgid "The last of these cases is the most interesting, because it's what makes it possible for users not just to read a Web site, but to send information back to it too."
msgstr ""

#: ../../topics/forms/index.txt:179
# f0331e0ed67d49c6b5993f79377fc236
msgid "Building a form"
msgstr ""

#: ../../topics/forms/index.txt:182
# 42935a759c544647b64544e93aa04357
msgid "The work that needs to be done"
msgstr ""

#: ../../topics/forms/index.txt:184
# 7cb7974080df479888e1f258590ad025
msgid "Suppose you want to create a simple form on your Web site, in order to obtain the user's name. You'd need something like this in your template:"
msgstr ""

#: ../../topics/forms/index.txt:195
# 7fbf6679227c4430b34277f252218bdb
msgid "This tells the browser to return the form data to the URL ``/your-name/``, using the ``POST`` method. It will display a text field, labeled \"Your name:\", and a button marked \"OK\". If the template context contains a ``current_name`` variable, that will be used to pre-fill the ``your_name`` field."
msgstr ""

#: ../../topics/forms/index.txt:200
# 677ff554a25a4caaa9ebe6dfe6a1808d
msgid "You'll need a view that renders the template containing the HTML form, and that can supply the ``current_name`` field as appropriate."
msgstr ""

#: ../../topics/forms/index.txt:203
# 1badc3065caa4d84bcc3c0e850c0bfbb
msgid "When the form is submitted, the ``POST`` request which is sent to the server will contain the form data."
msgstr ""

#: ../../topics/forms/index.txt:206
# 9f847fe83a09484eacafbae920bd36a8
msgid "Now you'll also need a view corresponding to that ``/your-name/`` URL which will find the appropriate key/value pairs in the request, and then process them."
msgstr ""

#: ../../topics/forms/index.txt:209
# 5cf4f7c0729c4fcb9d74fe1bdca2a03b
msgid "This is a very simple form. In practice, a form might contain dozens or hundreds of fields, many of which might need to be pre-populated, and we might expect the user to work through the edit-submit cycle several times before concluding the operation."
msgstr ""

#: ../../topics/forms/index.txt:214
# d6d6c458c1734b28a9eb227b7de461ec
msgid "We might require some validation to occur in the browser, even before the form is submitted; we might want to use much more complex fields, that allow the user to do things like pick dates from a calendar and so on."
msgstr ""

#: ../../topics/forms/index.txt:218
# 71b92319c1b84f1999a04dfe87f54bd1
msgid "At this point it's much easier to get Django to do most of this work for us."
msgstr ""

#: ../../topics/forms/index.txt:221
# 756fe2c005c54e7e87c34766431de0e6
msgid "Building a form in Django"
msgstr ""

#: ../../topics/forms/index.txt:224
# e51c8d8769d4458b87cddc39cddfbc9a
msgid "The :class:`Form` class"
msgstr ""

#: ../../topics/forms/index.txt:226
# bb1fd5671048468bbdc929060b750cb2
msgid "We already know what we want our HTML form to look like. Our starting point for it in Django is this:"
msgstr ""

#: ../../topics/forms/index.txt:236
# 2d83fa6728cd4f1b972fb6abfe1be95c
msgid "This defines a :class:`Form` class with a single field (``your_name``). We've applied a human-friendly label to the field, which will appear in the ``<label>`` when it's rendered (although in this case, the :attr:`~Field.label` we specified is actually the same one that would be generated automatically if we had omitted it)."
msgstr ""

#: ../../topics/forms/index.txt:242
# f75330cf83a5470fa643a7897b5f6571
msgid "The field's maximum allowable length is defined by :attr:`~CharField.max_length`. This does two things. It puts a ``maxlength=\"100\"`` on the HTML ``<input>`` (so the browser should prevent the user from entering more than that number of characters in the first place). It also means that when Django receives the form back from the browser, it will validate the length of the data."
msgstr ""

#: ../../topics/forms/index.txt:249
# a7e0693ba9a74110bd6debf593648207
msgid "A :class:`Form` instance has an :meth:`~Form.is_valid()` method, which runs validation routines for all its fields. When this method is called, if all fields contain valid data, it will:"
msgstr ""

#: ../../topics/forms/index.txt:253
# 7dd4590a317f403bb6a9f57b8330def1
msgid "return ``True``"
msgstr ""

#: ../../topics/forms/index.txt:254
# c9067d76b2f34fef915bec83c32fb01a
msgid "place the form's data in its :attr:`~Form.cleaned_data` attribute."
msgstr ""

#: ../../topics/forms/index.txt:256
# 584f8ebb15be4bc2b222051849585a8a
msgid "The whole form, when rendered for the first time, will look like:"
msgstr ""

#: ../../topics/forms/index.txt:263
# f99243b237fb448a8bc17bce1b520f33
msgid "Note that it **does not** include the ``<form>`` tags, or a submit button. We'll have to provide those ourselves in the template."
msgstr ""

#: ../../topics/forms/index.txt:269
# 2318f3a45d184cc3ae4e677673a693ba
msgid "The view"
msgstr ""

#: ../../topics/forms/index.txt:271
# fa6f20122097484f95f84a82e93419d9
msgid "Form data sent back to a Django Web site is processed by a view, generally the same view which published the form. This allows us to reuse some of the same logic."
msgstr ""

#: ../../topics/forms/index.txt:275
# 62b511b35690466198f3e4e2331937ce
msgid "To handle the form we need to instantiate it in the view for the URL where we want it to be published:"
msgstr ""

#: ../../topics/forms/index.txt:301
# 8c117e9d7e0b4c869cbdd79dcedc0e8d
msgid "If we arrive at this view with a ``GET`` request, it will create an empty form instance and place it in the template context to be rendered. This is what we can expect to happen the first time we visit the URL."
msgstr ""

#: ../../topics/forms/index.txt:305
# 28b75009d390485abdf4f0eb2d2966b5
msgid "If the form is submitted using a ``POST`` request, the view will once again create a form instance and populate it with data from the request: ``form = NameForm(request.POST)`` This is called \"binding data to the form\" (it is now a *bound* form)."
msgstr ""

#: ../../topics/forms/index.txt:310
# ffdda5ad6eee4f878223d1b8558a8c68
msgid "We call the form's ``is_valid()`` method; if it's not ``True``, we go back to the template with the form. This time the form is no longer empty (*unbound*) so the HTML form will be populated with the data previously submitted, where it can be edited and corrected as required."
msgstr ""

#: ../../topics/forms/index.txt:315
# 9236c61a72014e8c90d76937d5b364af
msgid "If ``is_valid()`` is ``True``, we'll now be able to find all the validated form data in its ``cleaned_data`` attribute. We can use this data to update the database or do other processing before sending an HTTP redirect to the browser telling it where to go next."
msgstr ""

#: ../../topics/forms/index.txt:323
# a8a27da8d8a442f8b04faf1d38235c0f
msgid "The template"
msgstr ""

#: ../../topics/forms/index.txt:325
# 75468c99b3064af6aa00b89809453989
msgid "We don't need to do much in our ``name.html`` template. The simplest example is:"
msgstr ""

#: ../../topics/forms/index.txt:336
# 3d52d998fcf2464faae7590b8e11df03
msgid "All the form's fields and their attributes will be unpacked into HTML markup from that ``{{ form }}`` by Django's template language."
msgstr ""

#: ../../topics/forms/index.txt:339
# 80ffd835ad1b4c6094a084173e2ad831
msgid "Forms and Cross Site Request Forgery protection"
msgstr ""

#: ../../topics/forms/index.txt:341
# 66ce27b1abd74ae18b67c32f02c1173a
msgid "Django ships with an easy-to-use :doc:`protection against Cross Site Request Forgeries </ref/contrib/csrf>`. When submitting a form via ``POST`` with CSRF protection enabled you must use the :ttag:`csrf_token` template tag as in the preceding example. However, since CSRF protection is not directly tied to forms in templates, this tag is omitted from the following examples in this document."
msgstr ""

#: ../../topics/forms/index.txt:348
# f8398fb7b0e544efbc2682384d92ee8c
msgid "HTML5 input types and browser validation"
msgstr ""

#: ../../topics/forms/index.txt:350
# 2e6d42240f61467e859bef5738e1f446
msgid "If your form includes a :class:`~django.forms.URLField`, an :class:`~django.forms.EmailField` or any integer field type, Django will use the ``url``, ``email`` and ``number`` HTML5 input types. By default, browsers may apply their own validation on these fields, which may be stricter than Django's validation. If you would like to disable this behavior, set the `novalidate` attribute on the ``form`` tag, or specify a different widget on the field, like :class:`TextInput`."
msgstr ""

#: ../../topics/forms/index.txt:358
# 40a29ea4393043498a8b999c161cfdb0
msgid "We now have a working web form, described by a Django :class:`Form`, processed by a view, and rendered as an HTML ``<form>``."
msgstr ""

#: ../../topics/forms/index.txt:361
# d3fe49184f1a44a6a566949d6e407334
msgid "That's all you need to get started, but the forms framework puts a lot more at your fingertips. Once you understand the basics of the process described above, you should be aware of what else is readily available in the forms system and know a little bit about some of the underlying machinery."
msgstr ""

#: ../../topics/forms/index.txt:367
# a222e33ab96d49d4b672503974939274
msgid "More about Django :class:`Form` classes"
msgstr ""

#: ../../topics/forms/index.txt:369
# abdfe31d986045599db65acb1db98564
msgid "All form classes are created as subclasses of :class:`django.forms.Form`, including the :doc:`ModelForm </topics/forms/modelforms>`, which you encounter in Django's admin."
msgstr ""

#: ../../topics/forms/index.txt:373
# 50898882214e4072bac524090f2c830b
msgid "Models and Forms"
msgstr ""

#: ../../topics/forms/index.txt:375
# 62e9e46d332341d8b1054bbc3d81a9cd
msgid "In fact if your form is going to be used to directly add or edit a Django model, a :doc:`ModelForm </topics/forms/modelforms>` can save you a great deal of time, effort, and code, because it will build a form, along with the appropriate fields and their attributes, from a ``Model`` class."
msgstr ""

#: ../../topics/forms/index.txt:381
# 6e0ca9f3c7234bcca8e35c8f74f64307
msgid "Bound and unbound form instances"
msgstr ""

#: ../../topics/forms/index.txt:383
# ae051a1986b7421e9e4897873dca4d63
msgid "The distinction between :ref:`ref-forms-api-bound-unbound` is important:"
msgstr ""

#: ../../topics/forms/index.txt:385
# d6937c3eb1cc441b8b5d490ae5604226
msgid "An unbound form has no data associated with it. When rendered to the user, it will be empty or will contain default values."
msgstr ""

#: ../../topics/forms/index.txt:388
# 070869e9d69a43248c61301bf5f8654a
msgid "A bound form has submitted data, and hence can be used to tell if that data is valid. If an invalid bound form is rendered, it can include inline error messages telling the user what data to correct."
msgstr ""

#: ../../topics/forms/index.txt:392
# 76b171877f2a4caab564dc160556271a
msgid "The form's ``is_bound()`` method will tell you whether a form has data bound to it or not."
msgstr ""

#: ../../topics/forms/index.txt:396
# b80b117bab8245508af8060af99cd610
msgid "More on fields"
msgstr ""

#: ../../topics/forms/index.txt:398
# 77dec4ac190647dab85735a0ddf11689
msgid "Consider a more useful form than our minimal example above, which we could use to implement \"contact me\" functionality on a personal Web site:"
msgstr ""

#: ../../topics/forms/index.txt:411
# 110a5c1e9e8747d49d10588c757d583c
msgid "Our earlier form used a single field, ``your_name``, a :class:`CharField`. In this case, our form has four fields: ``subject``, ``message``, ``sender`` and ``cc_myself``. :class:`CharField`, :class:`EmailField` and :class:`BooleanField` are just three of the available field types; a full list can be found in :doc:`/ref/forms/fields`."
msgstr ""

#: ../../topics/forms/index.txt:418
# 60f9bc2a79f3430c942e07b74eb1a32e
msgid "Widgets"
msgstr ""

#: ../../topics/forms/index.txt:420
# 26ee309ba3064bbfb31dbf2436e04da3
msgid "Each form field has a corresponding :doc:`Widget class </ref/forms/widgets/>`, which in turn corresponds to an HTML form widget such as ``<input type=\"text\">``."
msgstr ""

#: ../../topics/forms/index.txt:424
# e46b62c8e0884eec9376450ced195052
msgid "In most cases, the field will have a sensible default widget. For example, by default, a :class:`CharField` will have a :class:`TextInput` widget, that produces an ``<input type=\"text\">`` in the HTML. If you needed ``<input type=\"textarea\">`` instead, you'd specify the appropriate widget when defining your form field, as we have done for the ``message`` field."
msgstr ""

#: ../../topics/forms/index.txt:431
# 8e77c2d50b1d4e2bb9edce841ac17650
msgid "Field data"
msgstr ""

#: ../../topics/forms/index.txt:433
# 10386facfbbb4031a90992fed7578dfb
msgid "Whatever the data submitted with a form, once it has been successfully validated by calling ``is_valid()`` (and ``is_valid()`` has returned ``True``), the validated form data will be in the ``form.cleaned_data`` dictionary. This data will have been nicely converted into Python types for you."
msgstr ""

#: ../../topics/forms/index.txt:440
# 946f7b06462b4e26b8338fa68f0848e0
msgid "You can still access the unvalidated data directly from ``request.POST`` at this point, but the validated data is better."
msgstr ""

#: ../../topics/forms/index.txt:443
# 9d458a38e0764df99ab84afbe5cde365
msgid "In the contact form example above, ``cc_myself`` will be a boolean value. Likewise, fields such as :class:`IntegerField` and :class:`FloatField` convert values to a Python ``int`` and ``float`` respectively."
msgstr ""

#: ../../topics/forms/index.txt:447
# 173e7a83276b4db39018eae400ddeb20
msgid "Here's how the form data could be processed in the view that handles this form:"
msgstr ""

#: ../../topics/forms/index.txt:468
# 368ef7f8a0ce4406a17c89436911bd4e
msgid "For more on sending email from Django, see :doc:`/topics/email`."
msgstr ""

#: ../../topics/forms/index.txt:470
# ef8f3eb401c24b4f89696a1bcebf3e8d
msgid "Some field types need some extra handling. For example, files that are uploaded using a form need to be handled differently (they can be retrieved from ``request.FILES``, rather than ``request.POST``). For details of how to handle file uploads with your form, see :ref:`binding-uploaded-files`."
msgstr ""

#: ../../topics/forms/index.txt:476
# 4cb15e6c263b47d6a7da8e193a4cad2f
msgid "Working with form templates"
msgstr ""

#: ../../topics/forms/index.txt:478
# db38a7f1626c47e193edf90b43ef9ff0
msgid "All you need to do to get your form into a template is to place the form instance into the template context. So if your form is called ``form`` in the context, ``{{ form }}`` will render its ``<label>`` and ``<input>`` elements appropriately."
msgstr ""

#: ../../topics/forms/index.txt:484
# f540e893f8624e10b64cf5b4113655f1
msgid "Form rendering options"
msgstr ""

#: ../../topics/forms/index.txt:486
# 8dedfd66b6724f079a5b2a7e2258d2f2
msgid "Additional form template furniture"
msgstr ""

#: ../../topics/forms/index.txt:488
# 2140e0c153e140039066c8135c19cd7c
msgid "Don't forget that a form's output does *not* include the surrounding ``<form>`` tags, or the form's ``submit`` control. You will have to provide these yourself."
msgstr ""

#: ../../topics/forms/index.txt:492
# 95b83eaa8e1d421bb8d96efa1d33138e
msgid "There are other output options though for the ``<label>``/``<input>`` pairs:"
msgstr ""

#: ../../topics/forms/index.txt:494
# 83ddeb7b8d20456d97fcdc3653394268
msgid "``{{ form.as_table }}`` will render them as table cells wrapped in ``<tr>`` tags"
msgstr ""

#: ../../topics/forms/index.txt:497
# 47489a4a21b24581a8276c6474ea2387
msgid "``{{ form.as_p }}`` will render them wrapped in ``<p>`` tags"
msgstr ""

#: ../../topics/forms/index.txt:499
# 2b7109a845a745179aea84a9aefea1a6
msgid "``{{ form.as_ul }}`` will render them wrapped in ``<li>`` tags"
msgstr ""

#: ../../topics/forms/index.txt:501
# 4d618672a6404895addeadbbbbbfd05f
msgid "Note that you'll have to provide the surrounding ``<table>`` or ``<ul>`` elements yourself."
msgstr ""

#: ../../topics/forms/index.txt:504
# cfd0f2485db64d5b8b3cff8cfbfcb20c
msgid "Here's the output of ``{{ form.as_p }}`` for our ``ContactForm`` instance:"
msgstr ""

#: ../../topics/forms/index.txt:517
# bc27a5b5b1b543658cda10e734f27496
msgid "Note that each form field has an ID attribute set to ``id_<field-name>``, which is referenced by the accompanying label tag. This is important in ensuring that forms are accessible to assistive technology such as screen reader software. You can also :ref:`customize the way in which labels and ids are generated <ref-forms-api-configuring-label>`."
msgstr ""

#: ../../topics/forms/index.txt:523
# 3779fb0fa56245799b13a68aabcc1551
msgid "See :ref:`ref-forms-api-outputting-html` for more on this."
msgstr ""

#: ../../topics/forms/index.txt:526
# 797d0731d35241038bc8227ca0c87a97
msgid "Rendering fields manually"
msgstr ""

#: ../../topics/forms/index.txt:528
# 8ff22a3c31a14fddbaf69e879fb214c3
msgid "We don't have to let Django unpack the form's fields; we can do it manually if we like (allowing us to reorder the fields, for example). Each field is available as an attribute of the form using ``{{ form.name_of_field }}``, and in a Django template, will be rendered appropriately. For example:"
msgstr ""

#: ../../topics/forms/index.txt:558
# cf1cb0b665da4d0f96c99937067081e2
msgid "Rendering form error messages"
msgstr ""

#: ../../topics/forms/index.txt:560
# 197895d7ff8e4b3f977cd2c18d8a5e2d
msgid "Of course, the price of this flexibility is more work. Until now we haven't had to worry about how to display form errors, because that's taken care of for us. In this example we have had to make sure we take care of any errors for each field and any errors for the form as a whole. Note ``{{ form.non_field_errors }}`` at the top of the form and the template lookup for errors on each field."
msgstr ""

#: ../../topics/forms/index.txt:566
# d83d647e778c429fa32392199435acc3
msgid "Using ``{{ form.name_of_field.errors }}`` displays a list of form errors, rendered as an unordered list. This might look like:"
msgstr ""

#: ../../topics/forms/index.txt:575
# eb441f0aa965469db99cded858c33957
msgid "The list has a CSS class of ``errorlist`` to allow you to style its appearance. If you wish to further customize the display of errors you can do so by looping over them:"
msgstr ""

#: ../../topics/forms/index.txt:589
# a8d4ff7b71d34cb7b1c72fca9b4e2b68
msgid "See :doc:`/ref/forms/api` for more on errors, styling, and working with form attributes in templates."
msgstr ""

#: ../../topics/forms/index.txt:593
# 9d81c94f9d124e0e89ff09f4c1f402a8
msgid "Looping over the form's fields"
msgstr ""

#: ../../topics/forms/index.txt:595
# c2b8a78ca37647b6b977c947575e1fb7
msgid "If you're using the same HTML for each of your form fields, you can reduce duplicate code by looping through each field in turn using a ``{% for %}`` loop:"
msgstr ""

#: ../../topics/forms/index.txt:608
# bb73f9f01a394c80ae243cc4020e66fa
msgid "Useful attributes on ``{{ field }}`` include:"
msgstr ""

#: ../../topics/forms/index.txt:611
# 7b74f59b6f8e4480be9a3194ec930fd9
msgid "``{{ field.label }}``"
msgstr ""

#: ../../topics/forms/index.txt:611
# 882c77cf520b4b2ab9229b704b648baa
msgid "The label of the field, e.g. ``Email address``."
msgstr ""

#: ../../topics/forms/index.txt:621
# 31083fbb56544e52b853bbf3148326c8
msgid "``{{ field.label_tag }}``"
msgstr ""

#: ../../topics/forms/index.txt:614
# 41d6ceac11c743c58a46c5018e30bff2
msgid "The field's label wrapped in the appropriate HTML ``<label>`` tag."
msgstr ""

#: ../../topics/forms/index.txt:618
# 194f5901e89945b68b21ef8151a59dbc
msgid "This includes the form's :attr:`~django.forms.Form.label_suffix`. For example, the default ``label_suffix`` is a colon::"
msgstr ""

#: ../../topics/forms/index.txt:627
# 7e19fef1098348d9a0786a86fb3d06f1
msgid "``{{ field.id_for_label }}``"
msgstr ""

#: ../../topics/forms/index.txt:624
# a568630bce70427092da3770fcce842e
msgid "The ID that will be used for this field (``id_email`` in the example above). If you are constructing the label manually, you may want to use this in lieu of ``label_tag``. It's also useful, for example, if you have some inline JavaScript and want to avoid hardcoding the field's ID."
msgstr ""

#: ../../topics/forms/index.txt:630
# ad88503a8ef7449d8caf65943a024b2c
msgid "``{{ field.value }}``"
msgstr ""

#: ../../topics/forms/index.txt:630
# b31060805f524470bc6e9324347466b7
msgid "The value of the field. e.g ``someone@example.com``."
msgstr ""

#: ../../topics/forms/index.txt:634
# 7e96954b8b8b451b965442e11cb680cb
msgid "``{{ field.html_name }}``"
msgstr ""

#: ../../topics/forms/index.txt:633
# f330f294cf12483fb65d8fee9c59c096
msgid "The name of the field that will be used in the input element's name field. This takes the form prefix into account, if it has been set."
msgstr ""

#: ../../topics/forms/index.txt:637
# f4eb034dfac64721a96c116b88b28663
msgid "``{{ field.help_text }}``"
msgstr ""

#: ../../topics/forms/index.txt:637
# ada25e6d66704bc295a2263bad0ed071
msgid "Any help text that has been associated with the field."
msgstr ""

#: ../../topics/forms/index.txt:644
# 0c115a34df8c429b8e94feda89062ffc
msgid "``{{ field.errors }}``"
msgstr ""

#: ../../topics/forms/index.txt:640
# 6004444aaed1435c87bf8403b76851bb
msgid "Outputs a ``<ul class=\"errorlist\">`` containing any validation errors corresponding to this field. You can customize the presentation of the errors with a ``{% for error in field.errors %}`` loop. In this case, each object in the loop is a simple string containing the error message."
msgstr ""

#: ../../topics/forms/index.txt:649
# 51614de2ef01454e91eabb1f33da7848
msgid "``{{ field.is_hidden }}``"
msgstr ""

#: ../../topics/forms/index.txt:647
# 4f3ace394f984693a9929a7bf1125c2a
msgid "This attribute is ``True`` if the form field is a hidden field and ``False`` otherwise. It's not particularly useful as a template variable, but could be useful in conditional tests such as:"
msgstr ""

#: ../../topics/forms/index.txt:661
# 672bd98b175f4dec98eadfd511c9e06b
msgid "``{{ field.field }}``"
msgstr ""

#: ../../topics/forms/index.txt:658
# c08eb5227bfd4a619c55f6d1a254032f
msgid "The :class:`~django.forms.Field` instance from the form class that this :class:`~django.forms.BoundField` wraps. You can use it to access :class:`~django.forms.Field` attributes, e.g. ``{{ char_field.field.max_length }}``."
msgstr ""

#: ../../topics/forms/index.txt:664
# ac585a3079ea48f3a21f5e4fec3d7608
msgid "Looping over hidden and visible fields"
msgstr ""

#: ../../topics/forms/index.txt:666
# 56b57d8515fd4962afbfb40053d1ce18
msgid "If you're manually laying out a form in a template, as opposed to relying on Django's default form layout, you might want to treat ``<input type=\"hidden\">`` fields differently from non-hidden fields. For example, because hidden fields don't display anything, putting error messages \"next to\" the field could cause confusion for your users -- so errors for those fields should be handled differently."
msgstr ""

#: ../../topics/forms/index.txt:673
# 8302f9e153d04798b363deca4f2e0bce
msgid "Django provides two methods on a form that allow you to loop over the hidden and visible fields independently: ``hidden_fields()`` and ``visible_fields()``. Here's a modification of an earlier example that uses these two methods:"
msgstr ""

#: ../../topics/forms/index.txt:692
# d1a39c7da7dc428da54821bd8d7e0121
msgid "This example does not handle any errors in the hidden fields. Usually, an error in a hidden field is a sign of form tampering, since normal form interaction won't alter them. However, you could easily insert some error displays for those form errors, as well."
msgstr ""

#: ../../topics/forms/index.txt:698
# ed62091a2d5e42cd8e98d2c58d3148f0
msgid "Reusable form templates"
msgstr ""

#: ../../topics/forms/index.txt:700
# 76d0a8bd32684435ab859eeb19c9163b
msgid "If your site uses the same rendering logic for forms in multiple places, you can reduce duplication by saving the form's loop in a standalone template and using the :ttag:`include` tag to reuse it in other templates:"
msgstr ""

#: ../../topics/forms/index.txt:717
# 11df0629832e46e09b49add04193591f
msgid "If the form object passed to a template has a different name within the context, you can alias it using the ``with`` argument of the :ttag:`include` tag:"
msgstr ""

#: ../../topics/forms/index.txt:725
# 738222516c1a4e398d2680d2e530f532
msgid "If you find yourself doing this often, you might consider creating a custom :ref:`inclusion tag<howto-custom-template-tags-inclusion-tags>`."
msgstr ""

#: ../../topics/forms/index.txt:729
# 17bc2ac5e6574a98a0991bb187ac993c
msgid "Further topics"
msgstr ""

#: ../../topics/forms/index.txt:731
# 4b0956dcc4ea42dba64cd4de0d002ffb
msgid "This covers the basics, but forms can do a whole lot more:"
msgstr ""

#: ../../topics/forms/index.txt:743
# ff305b7f12604e6ab54a14580d9c13c5
msgid ":doc:`The Forms Reference </ref/forms/index>`"
msgstr ""

#: ../../topics/forms/index.txt:743
# 825a8ee9cb194322acf50905686fc8f5
msgid "Covers the full API reference, including form fields, form widgets, and form and field validation."
msgstr ""

