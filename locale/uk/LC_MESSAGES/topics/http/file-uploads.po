#
#
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"Last-Translator: Zoriana Zaiats <sorenabell@quintagroup.com>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"Content-Transfer-Encoding: 8bit\n"
"Content-Type: text/plain; charset=UTF-8\n"
"MIME-Version: 1.0\n"
"PO-Revision-Date: 2015-03-18 12:37+0200\n"
"Language: uk\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

# a5aff4664e9a477c918c1a1340a441a5
#: ../../topics/http/file-uploads.txt:3
msgid "File Uploads"
msgstr "Завантаження файлів"

# 60bc1c3c9b894c76b4ba2a75d07221b0
#: ../../topics/http/file-uploads.txt:7
msgid ""
"When Django handles a file upload, the file data ends up placed in "
":attr:`request.FILES <django.http.HttpRequest.FILES>` (for more on the "
"``request`` object see the documentation for :doc:`request and response "
"objects </ref/request-response>`). This document explains how files are "
"stored on disk and in memory, and how to customize the default behavior."
msgstr ""
"Коли Django обробляє завантаження файлу, дані про нього в кінці кінців "
"потрапляють в :attr:`request.FILES <django.http.HttpRequest.FILES>` "
"(докладніше про об'єкт ``request`` дивіться у :doc:`відповідному розділі "
"</ref/request-response>`). Цей розділ описує як файли зберігаються на "
"диск або в пам'яті і як перевизначити це."

# 06f398f16a9249919f2c3a8d0c06cc76
#: ../../topics/http/file-uploads.txt:15
msgid ""
"There are security risks if you are accepting uploaded content from "
"untrusted users! See the security guide's topic on :ref:`user-uploaded-"
"content-security` for mitigation details."
msgstr ""
"Приймати завантажений контент від неперевірених користувачів - небезпечно! "
"Подробиці дивіться в :ref:`user-uploaded-content-security`."

# 0e97955c48a346e4949c22a457e3e552
#: ../../topics/http/file-uploads.txt:20
msgid "Basic file uploads"
msgstr "Основи завантаження файлу"

# 61ae7a092c1442f9ab8143fc5666f746
#: ../../topics/http/file-uploads.txt:22
msgid "Consider a simple form containing a :class:`~django.forms.FileField`::"
msgstr "Розглянемо просту форму, що містить :class:`~django.forms.FileField`::"

# 49a9aa64922442d5a1e24097d0be60a4
#: ../../topics/http/file-uploads.txt:31
msgid ""
"A view handling this form will receive the file data in :attr:`request.FILES"
" <django.http.HttpRequest.FILES>`, which is a dictionary containing a key "
"for each :class:`~django.forms.FileField` (or "
":class:`~django.forms.ImageField`, or other :class:`~django.forms.FileField`"
" subclass) in the form. So the data from the above form would be accessible "
"as ``request.FILES['file']``."
msgstr ""
"Представлення, обробляє цю форму, отримує дані файлу з :attr:`request.FILES "
"<django.http.HttpRequest.FILES>`, який є словником, що містить ключ для "
"кожного "
"поля :class:`~django.forms.FileField` (або :class:`~django.forms.ImageField`,"
" або підкласу :class:`~django.forms.FileField`) у формі."

# 97024e1b6d694a8993f333cc8037f48f
#: ../../topics/http/file-uploads.txt:38
msgid ""
"Note that :attr:`request.FILES <django.http.HttpRequest.FILES>` will only "
"contain data if the request method was ``POST`` and the ``<form>`` that "
"posted the request has the attribute ``enctype=\"multipart/form-data\"``. "
"Otherwise, ``request.FILES`` will be empty."
msgstr ""
"Зауважимо, що атрибут :attr:`request.FILES <django.http.HttpRequest.FILES>` "
"міститиме дані тільки при ``POST`` запити і якщо форма ``<form>`` "
"містить ``enctype=\"multipart/form-data\"``. В іншому випадку, "
"``request.FILES`` буде порожнім."

# 99807f813f2d4980a830ca64f8e0cf8d
#: ../../topics/http/file-uploads.txt:43
msgid ""
"Most of the time, you'll simply pass the file data from ``request`` into the"
" form as described in :ref:`binding-uploaded-files`. This would look "
"something like::"
msgstr ""
"У більшості випадків ви просто передасте дані з ``request`` в форму, як "
"описано в розділі :ref:`binding-uploaded-files`. Це буде виглядати так::"

# de31af86cff04fbd98863b484aa3ba45
#: ../../topics/http/file-uploads.txt:64
msgid ""
"Notice that we have to pass :attr:`request.FILES "
"<django.http.HttpRequest.FILES>` into the form's constructor; this is how "
"file data gets bound into a form."
msgstr ""
"Зауважимо, що ви повинні передати :attr:`request.FILES "
"<django.http.HttpRequest.FILES>` в конструктор форми."

# 48d4c625947a496aaede655f2e20d4fb
#: ../../topics/http/file-uploads.txt:67
msgid "Here's a common way you might handle an uploaded file::"
msgstr "Ось приклад стандартної обробки завантаженого файлу::"

# ffa8122bb31a4affa859f2cb4423f3d5
#: ../../topics/http/file-uploads.txt:74
msgid ""
"Looping over ``UploadedFile.chunks()`` instead of using ``read()`` ensures "
"that large files don't overwhelm your system's memory."
msgstr ""
"Цикл по ``UploadedFile.chunks()``, замість використання ``read()``, "
"убезпечить вас від навантаження системи при завантаженні великого файлу."

# 84b7da24934b45ea81f5d5236e910bb7
#: ../../topics/http/file-uploads.txt:77
msgid ""
"There are a few other methods and attributes available on ``UploadedFile`` "
"objects; see :class:`UploadedFile` for a complete reference."
msgstr ""
"Об'єкт ``UploadedFile`` містить і інші методи та атрибути, дивіться "
"опис :class:`UploadedFile`."

# 60d0bee6df94437fb4398c1e7ec520fe
#: ../../topics/http/file-uploads.txt:81
msgid "Handling uploaded files with a model"
msgstr "Обробка завантажених файлів використовуючи модель"

# ac05c3635ee048878cddb97eabb78bf5
#: ../../topics/http/file-uploads.txt:83
msgid ""
"If you're saving a file on a :class:`~django.db.models.Model` with a "
":class:`~django.db.models.FileField`, using a "
":class:`~django.forms.ModelForm` makes this process much easier. The file "
"object will be saved to the location specified by the "
":attr:`~django.db.models.FileField.upload_to` argument of the corresponding "
":class:`~django.db.models.FileField` when calling ``form.save()``::"
msgstr ""
"Якщо ви зберігаєте файли для моделі :class:`~django.db.models.Model` "
"з :class:`~django.db.models.FileField`, використання "
":class:`~django.forms.ModelForm` значно спростить цей процес. Файл буде "
"збережений за вказаним в аргументі "
":attr:`~django.db.models.FileField.upload_to` поля "
":class:`~django.db.models.FileField` шляхом, при виклику ``form.save()``::"

# 5be2da6eb4d94064adb96c51ff3f7e1b
#: ../../topics/http/file-uploads.txt:105
msgid ""
"If you are constructing an object manually, you can simply assign the file "
"object from :attr:`request.FILES <django.http.HttpRequest.FILES>` to the "
"file field in the model::"
msgstr ""
"Якщо ви створюєте об'єкт самостійно, ви можете призначити файл з "
":attr:`request.FILES <django.http.HttpRequest.FILES>` відповідному полю "
"моделі::"

# 401a8221c20f48ac88a87e0acc3c6f6e
#: ../../topics/http/file-uploads.txt:126
msgid "Upload Handlers"
msgstr "Обробники завантаження"

# 75f3f9c6b05047cd8b0975bb825130d0
#: ../../topics/http/file-uploads.txt:130
msgid ""
"When a user uploads a file, Django passes off the file data to an *upload "
"handler* -- a small class that handles file data as it gets uploaded. Upload"
" handlers are initially defined in the :setting:`FILE_UPLOAD_HANDLERS` "
"setting, which defaults to::"
msgstr ""
"Коли користувач завантажує файл, Django передає вміст файлу в *обробник "
"завантаження* - невеликий клас, який обробляє завантажені дані. Обробники "
"завантаження визначені в налаштуванні :setting:`FILE_UPLOAD_HANDLERS`, за "
"замовчуванням::"

# 6672f7fe59334284a1ed228fe79d3247
#: ../../topics/http/file-uploads.txt:138
msgid ""
"Together :class:`MemoryFileUploadHandler` and "
":class:`TemporaryFileUploadHandler` provide Django's default file upload "
"behavior of reading small files into memory and large ones onto disk."
msgstr ""
":class:`MemoryFileUploadHandler` і :class:`TemporaryFileUploadHandler` "
"визначають обробку файлів за замовчуванням в Django: завантаження "
"невеликих файлів в пам'ять і великих на диск."

# 0b3a1f7946484add9e97bcdf6126cdc6
#: ../../topics/http/file-uploads.txt:142
msgid ""
"You can write custom handlers that customize how Django handles files. You "
"could, for example, use custom handlers to enforce user-level quotas, "
"compress data on the fly, render progress bars, and even send data to "
"another storage location directly without storing it locally. See "
":ref:`custom_upload_handlers` for details on how you can customize or "
"completely replace upload behavior."
msgstr ""
"Ви можете написати власний обробник завантаження файлів. Ви можете "
"використовувати його, наприклад, для визначення квоти для користувачів на "
"завантаження файлів, стиснення даних \"на льоту\", відображення прогресу "
"завантаження файлу, або пересилання файла в сховище даних без "
"збереження його локально."

# 81c5988277124670b2d67919b2c1869a
#: ../../topics/http/file-uploads.txt:151
msgid "Where uploaded data is stored"
msgstr "Де зберігаються завантажені дані"

# c0ea1ac51ee640aeb9d4bc392a9e1daa
#: ../../topics/http/file-uploads.txt:153
msgid "Before you save uploaded files, the data needs to be stored somewhere."
msgstr ""
"Перед тим, як ви збережете завантажений файл, завантажені дані повинні десь "
"зберігатися."

# 6d90c07a47624614b944d27a7e7d67bc
#: ../../topics/http/file-uploads.txt:155
msgid ""
"By default, if an uploaded file is smaller than 2.5 megabytes, Django will "
"hold the entire contents of the upload in memory. This means that saving the"
" file involves only a read from memory and a write to disk and thus is very "
"fast."
msgstr ""
"За замовчуванням, якщо завантажений файл менший 2.5 мегабайт, Django "
"помістить вміст файлу в пам'ять. Це означає, що при збереженні файлу "
"вміст прочитається з пам'яті і збережеться на диск, а це швидка операція."

# 8f999ae1cdc34374a629d2d631cec6bd
#: ../../topics/http/file-uploads.txt:159
msgid ""
"However, if an uploaded file is too large, Django will write the uploaded "
"file to a temporary file stored in your system's temporary directory. On a "
"Unix-like platform this means you can expect Django to generate a file "
"called something like ``/tmp/tmpzfp6I6.upload``. If an upload is large "
"enough, you can watch this file grow in size as Django streams the data onto"
" disk."
msgstr ""
"Однак, якщо приєднаний файл великий, Django запише його в тимчасовий "
"каталог вашої системи. На Unix-системах це означає, що Django створить, "
"наприклад, такий файл ``/tmp/tmpzfp6I6.upload``. Якщо завантажений файл "
"досить великий, ви можете відстежити, як збільшується розмір файлу, "
"в процесі завантаження даних."

# 12a4cd4b0a5d4261995d51f8bc7f10a2
#: ../../topics/http/file-uploads.txt:165
msgid ""
"These specifics -- 2.5 megabytes; ``/tmp``; etc. -- are simply \"reasonable "
"defaults\" which can be customized as described in the next section."
msgstr ""
"Ці значення - 2.5 мегабайта, ``/tmp`` та ін. - просто \"розумні значення за "
"замовчуванням\", які ви можете перевизначити. Про це в наступному розділі."

# 3b62a92e313740fb93bdc7dfc67cd814
#: ../../topics/http/file-uploads.txt:169
msgid "Changing upload handler behavior"
msgstr "Зміна процесу завантаження"

# 4a28c3380f6e4e8b9316c2c4f4a32df1
#: ../../topics/http/file-uploads.txt:171
msgid ""
"There are a few settings which control Django's file upload behavior. See "
":ref:`File Upload Settings <file-upload-settings>` for details."
msgstr ""
"Процес завантаження файлів використовує декілька налаштувань. Подробиці "
"дивіться в :ref:`розділі налаштувань <file-upload-settings>`."

# 41196b1567b147939bba89176e909de5
#: ../../topics/http/file-uploads.txt:175
msgid "Modifying upload handlers on the fly"
msgstr "Зміна обробників завантаження файлу \"на льоту\""

# d48a170e3c4448a8b36e77346c6e4203
#: ../../topics/http/file-uploads.txt:177
msgid ""
"Sometimes particular views require different upload behavior. In these "
"cases, you can override upload handlers on a per-request basis by modifying "
"``request.upload_handlers``. By default, this list will contain the upload "
"handlers given by :setting:`FILE_UPLOAD_HANDLERS`, but you can modify the "
"list as you would any other list."
msgstr ""
"Іноді різні представлення вимагають різної обробки завантажених файлів. "
"У таких випадках ви можете перевизначити обробники завантаження, змінивши "
"``request.upload_handlers``. За замовчуванням цей атрибут містить обробники "
"з налаштування :setting:`FILE_UPLOAD_HANDLERS`, але ви можете змінити його."

# 5cc879782f3b47a79c20b7ddc4b920a8
#: ../../topics/http/file-uploads.txt:183
msgid ""
"For instance, suppose you've written a ``ProgressBarUploadHandler`` that "
"provides feedback on upload progress to some sort of AJAX widget. You'd add "
"this handler to your upload handlers like this::"
msgstr ""
"Наприклад, ви створили ``ProgressBarUploadHandler``, який повідомляє "
"сторінку через AJAX-запити про прогрес завантаження файлу. Ви можете "
"використовувати його таким чином::"

# 8b306bfa45de4a5581d52ba91b74c19d
#: ../../topics/http/file-uploads.txt:189
msgid ""
"You'd probably want to use ``list.insert()`` in this case (instead of "
"``append()``) because a progress bar handler would need to run *before* any "
"other handlers. Remember, the upload handlers are processed in order."
msgstr ""
"Використання ``list.insert()`` в цьому випадку (замість ``append()``) буде "
"доречніше, оскільки обробник, що повідомляє про прогрес завантаження, повинен "
"використовуватися *перед* іншими обробниками. Запам'ятайте, обробники "
"завантаження використовуються по-порядку."

# 34a3d0077ca7473ba93650df9c20fafb
#: ../../topics/http/file-uploads.txt:193
msgid ""
"If you want to replace the upload handlers completely, you can just assign a"
" new list::"
msgstr ""
"Якщо ви хочете повністю перевизначити обробники, просто вкажіть новий "
"список::"

# ee456a37e8a645ef9ddb22f4096af6fe
#: ../../topics/http/file-uploads.txt:200
msgid ""
"You can only modify upload handlers *before* accessing ``request.POST`` or "
"``request.FILES`` -- it doesn't make sense to change upload handlers after "
"upload handling has already started. If you try to modify "
"``request.upload_handlers`` after reading from ``request.POST`` or "
"``request.FILES`` Django will throw an error."
msgstr ""
"Ви можете змінити обробники завантажень *перед* використанням "
"``request.POST`` або ``request.FILES`` - немає сенсу це робити після "
"початку завантаження. Якщо ви спробуєте змінити ``request.upload_handlers`` "
"після використання ``request.POST`` або ``request.FILES``, Django викличе "
"виняток."

# a2a3f82a9d104327882073aa60b79c86
#: ../../topics/http/file-uploads.txt:207
msgid ""
"Thus, you should always modify uploading handlers as early in your view as "
"possible."
msgstr ""
"Ви повинні змінювати обробники завантаження в представленні якомога раніше."

# 855cafb62d784018a7cda2ae890a833f
#: ../../topics/http/file-uploads.txt:210
msgid ""
"Also, ``request.POST`` is accessed by "
":class:`~django.middleware.csrf.CsrfViewMiddleware` which is enabled by "
"default. This means you will need to use "
":func:`~django.views.decorators.csrf.csrf_exempt` on your view to allow you "
"to change the upload handlers.  You will then need to use "
":func:`~django.views.decorators.csrf.csrf_protect` on the function that "
"actually processes the request.  Note that this means that the handlers may "
"start receiving the file upload before the CSRF checks have been done. "
"Example code:"
msgstr ""
"``request.POST`` використовується "
":class:`~django.middleware.csrf.CsrfViewMiddleware`, який включений за "
"замовчуванням. Це означає, що ви повинні використовувати "
":func:`~django.views.decorators.csrf.csrf_exempt` для представлення, в якому "
"ви збираєтеся змінити обробники завантаження. Також не забувайте "
"скористатися декоратором :func:`~django.views.decorators.csrf.csrf_protect` "
"для функції, яка дійсно обробляє запис. Зверніть увагу, що це означає, "
"що обробники можуть почати отримувати завантаження файла до того, як "
"закінчиться CSRF перевірка. Наприклад::"

