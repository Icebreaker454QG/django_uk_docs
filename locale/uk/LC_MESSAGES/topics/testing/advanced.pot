# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../topics/testing/advanced.txt:3
# 0b7e3dd12d274debae5a275828091f5a
msgid "Advanced testing topics"
msgstr ""

#: ../../topics/testing/advanced.txt:6
# 7e23873d68a94cea9b30fb48cc8262cc
msgid "The request factory"
msgstr ""

#: ../../topics/testing/advanced.txt:12
# e4574bda54594fdc961557a52c751297
msgid "The :class:`~django.test.RequestFactory` shares the same API as the test client. However, instead of behaving like a browser, the RequestFactory provides a way to generate a request instance that can be used as the first argument to any view. This means you can test a view function the same way as you would test any other function -- as a black box, with exactly known inputs, testing for specific outputs."
msgstr ""

#: ../../topics/testing/advanced.txt:19
# b4ca260b9e7c4e739bd91c00a32a71f8
msgid "The API for the :class:`~django.test.RequestFactory` is a slightly restricted subset of the test client API:"
msgstr ""

#: ../../topics/testing/advanced.txt:22
# b05d7607e8d940bdb196e7519885635c
msgid "It only has access to the HTTP methods :meth:`~Client.get()`, :meth:`~Client.post()`, :meth:`~Client.put()`, :meth:`~Client.delete()`, :meth:`~Client.head()` and :meth:`~Client.options()`."
msgstr ""

#: ../../topics/testing/advanced.txt:27
# fe96e52be3f6472faf30ae6424d97188
msgid "These methods accept all the same arguments *except* for ``follows``. Since this is just a factory for producing requests, it's up to you to handle the response."
msgstr ""

#: ../../topics/testing/advanced.txt:31
# 6f4bd4bf907a4f2490949296ae6e4790
msgid "It does not support middleware. Session and authentication attributes must be supplied by the test itself if required for the view to function properly."
msgstr ""

#: ../../topics/testing/advanced.txt:36
# 64c67284a9d64bb283f8509a9b847e90
msgid "Example"
msgstr ""

#: ../../topics/testing/advanced.txt:38
# 8b600de295074f9d8b4fe385cb248a91
msgid "The following is a simple unit test using the request factory::"
msgstr ""

#: ../../topics/testing/advanced.txt:65
# b0fdf7855f5d4a77adfe3952fae6a8ca
msgid "Tests and multiple databases"
msgstr ""

#: ../../topics/testing/advanced.txt:70
# ef12d7d34fa1494389fdc7575fc1c6f5
msgid "Testing master/slave configurations"
msgstr ""

#: ../../topics/testing/advanced.txt:72
# ff3f9f640baf4de0b92ad90f61b0bee6
msgid "If you're testing a multiple database configuration with master/slave replication, this strategy of creating test databases poses a problem. When the test databases are created, there won't be any replication, and as a result, data created on the master won't be seen on the slave."
msgstr ""

#: ../../topics/testing/advanced.txt:78
# aeecee40606b40be9fe46eaa7bab9e14
msgid "To compensate for this, Django allows you to define that a database is a *test mirror*. Consider the following (simplified) example database configuration::"
msgstr ""

#: ../../topics/testing/advanced.txt:98
# 8d5725c10df34c319c6c3c41616c8528
msgid "In this setup, we have two database servers: ``dbmaster``, described by the database alias ``default``, and ``dbslave`` described by the alias ``slave``. As you might expect, ``dbslave`` has been configured by the database administrator as a read slave of ``dbmaster``, so in normal activity, any write to ``default`` will appear on ``slave``."
msgstr ""

#: ../../topics/testing/advanced.txt:104
# c58a254e47bc466aaf32dd549b710315
msgid "If Django created two independent test databases, this would break any tests that expected replication to occur. However, the ``slave`` database has been configured as a test mirror (using the :setting:`TEST_MIRROR` setting), indicating that under testing, ``slave`` should be treated as a mirror of ``default``."
msgstr ""

#: ../../topics/testing/advanced.txt:110
# bcf2eec68c7b4f3a8f283646503a7ba2
msgid "When the test environment is configured, a test version of ``slave`` will *not* be created. Instead the connection to ``slave`` will be redirected to point at ``default``. As a result, writes to ``default`` will appear on ``slave`` -- but because they are actually the same database, not because there is data replication between the two databases."
msgstr ""

#: ../../topics/testing/advanced.txt:120
# bb46120ec9384059aa92fbf91fdd364c
msgid "Controlling creation order for test databases"
msgstr ""

#: ../../topics/testing/advanced.txt:122
# 94d48a9bac1b462ab24c3d368966a06a
msgid "By default, Django will assume all databases depend on the ``default`` database and therefore always create the ``default`` database first. However, no guarantees are made on the creation order of any other databases in your test setup."
msgstr ""

#: ../../topics/testing/advanced.txt:127
# f19c6cc1ddd84744bb31ba613b8781be
msgid "If your database configuration requires a specific creation order, you can specify the dependencies that exist using the :setting:`TEST_DEPENDENCIES` setting. Consider the following (simplified) example database configuration::"
msgstr ""

#: ../../topics/testing/advanced.txt:155
# a28c85bbdf6343329c91f9db6fe82d3b
msgid "Under this configuration, the ``diamonds`` database will be created first, as it is the only database alias without dependencies. The ``default`` and ``clubs`` alias will be created next (although the order of creation of this pair is not guaranteed); then ``hearts``; and finally ``spades``."
msgstr ""

#: ../../topics/testing/advanced.txt:160
# a661c5cd8453484d9fa881512d3321bc
msgid "If there are any circular dependencies in the :setting:`TEST_DEPENDENCIES` definition, an ``ImproperlyConfigured`` exception will be raised."
msgstr ""

#: ../../topics/testing/advanced.txt:165
# 9b0c69ebed47469388365b12d6cad553
msgid "Advanced features of ``TransactionTestCase``"
msgstr ""

#: ../../topics/testing/advanced.txt:173
# 5471736d9cb04517b815b5af8aeefb51
msgid "This attribute is a private API. It may be changed or removed without a deprecation period in the future, for instance to accommodate changes in application loading."
msgstr ""

#: ../../topics/testing/advanced.txt:177
# 6285a27b4fd44322968aa58f28111e95
msgid "It's used to optimize Django's own test suite, which contains hundreds of models but no relations between models in different applications."
msgstr ""

#: ../../topics/testing/advanced.txt:180
# 57fb09367f944d81a6a5cd3658ce14f3
msgid "By default, ``available_apps`` is set to ``None``. After each test, Django calls :djadmin:`flush` to reset the database state. This empties all tables and emits the :data:`~django.db.models.signals.post_migrate` signal, which re-creates one content type and three permissions for each model. This operation gets expensive proportionally to the number of models."
msgstr ""

#: ../../topics/testing/advanced.txt:186
# 6e34b4180be3447a8b8dac90300ac07c
msgid "Setting ``available_apps`` to a list of applications instructs Django to behave as if only the models from these applications were available. The behavior of ``TransactionTestCase`` changes as follows:"
msgstr ""

#: ../../topics/testing/advanced.txt:190
# cd73248a4d294b6ebc8721cba87c1bc8
msgid ":data:`~django.db.models.signals.post_migrate` is fired before each test to create the content types and permissions for each model in available apps, in case they're missing."
msgstr ""

#: ../../topics/testing/advanced.txt:193
# 7c12cb5e9ae1484ca46b8a032e6b7dfb
msgid "After each test, Django empties only tables corresponding to models in available apps. However, at the database level, truncation may cascade to related models in unavailable apps. Furthermore :data:`~django.db.models.signals.post_migrate` isn't fired; it will be fired by the next ``TransactionTestCase``, after the correct set of applications is selected."
msgstr ""

#: ../../topics/testing/advanced.txt:200
# 46972c5281024833a442ff65c69fb8c6
msgid "Since the database isn't fully flushed, if a test creates instances of models not included in ``available_apps``, they will leak and they may cause unrelated tests to fail. Be careful with tests that use sessions; the default session engine stores them in the database."
msgstr ""

#: ../../topics/testing/advanced.txt:205
# fd64def162c64dd2b8d69d06cd299b43
msgid "Since :data:`~django.db.models.signals.post_migrate` isn't emitted after flushing the database, its state after a ``TransactionTestCase`` isn't the same as after a ``TestCase``: it's missing the rows created by listeners to :data:`~django.db.models.signals.post_migrate`. Considering the :ref:`order in which tests are executed <order-of-tests>`, this isn't an issue, provided either all ``TransactionTestCase`` in a given test suite declare ``available_apps``, or none of them."
msgstr ""

#: ../../topics/testing/advanced.txt:213
# 73635766f77f4e78b35b2c1bbe0bf269
msgid "``available_apps`` is mandatory in Django's own test suite."
msgstr ""

#: ../../topics/testing/advanced.txt:217
# 7cf45843f541489a8d8bcdfd9739e36f
msgid "Setting ``reset_sequences = True`` on a ``TransactionTestCase`` will make sure sequences are always reset before the test run::"
msgstr ""

#: ../../topics/testing/advanced.txt:228
# e70aeaee42704c33a4e764c13203824a
msgid "Unless you are explicitly testing primary keys sequence numbers, it is recommended that you do not hard code primary key values in tests."
msgstr ""

#: ../../topics/testing/advanced.txt:231
# d01754492c174a0286a4691e83b6d173
msgid "Using ``reset_sequences = True`` will slow down the test, since the primary key reset is an relatively expensive database operation."
msgstr ""

#: ../../topics/testing/advanced.txt:235
# a1a83d785e3b4b09a0d124ecb52b7f5c
msgid "Running tests outside the test runner"
msgstr ""

#: ../../topics/testing/advanced.txt:237
# 5a0210fabf894631b5ea1c99d444273f
msgid "If you want to run tests outside of ``./manage.py test`` -- for example, from a shell prompt -- you will need to set up the test environment first. Django provides a convenience method to do this::"
msgstr ""

#: ../../topics/testing/advanced.txt:244
# 68a63424c4254b569e33a39ac0bd0b20
msgid ":func:`~django.test.utils.setup_test_environment` puts several Django features into modes that allow for repeatable testing, but does not create the test databases; :func:`django.test.runner.DiscoverRunner.setup_databases` takes care of that."
msgstr ""

#: ../../topics/testing/advanced.txt:249
# 73a02b970c7644d587ab7b93d032ac10
msgid "The call to :func:`~django.test.utils.setup_test_environment` is made automatically as part of the setup of ``./manage.py test``. You only need to manually invoke this method if you're not using running your tests via Django's test runner."
msgstr ""

#: ../../topics/testing/advanced.txt:256
# bed00d6a92534360a28ccbcd072c8480
msgid "If you are not using a management command to invoke the tests, you will also need to first setup Django itself using :func:`django.setup()`."
msgstr ""

#: ../../topics/testing/advanced.txt:262
# 33ee47ef704441ccb436c20e4fa45be3
msgid "Using different testing frameworks"
msgstr ""

#: ../../topics/testing/advanced.txt:264
# 0c9ab74a1a074924a223f9b3119b7692
msgid "Clearly, :mod:`unittest` is not the only Python testing framework. While Django doesn't provide explicit support for alternative frameworks, it does provide a way to invoke tests constructed for an alternative framework as if they were normal Django tests."
msgstr ""

#: ../../topics/testing/advanced.txt:269
# e3d0756fa78b4bdb90ec0ddd7f89406a
msgid "When you run ``./manage.py test``, Django looks at the :setting:`TEST_RUNNER` setting to determine what to do. By default, :setting:`TEST_RUNNER` points to ``'django.test.runner.DiscoverRunner'``. This class defines the default Django testing behavior. This behavior involves:"
msgstr ""

#: ../../topics/testing/advanced.txt:274
# 308d95584f70409f92e34dfaf7278860
msgid "Performing global pre-test setup."
msgstr ""

#: ../../topics/testing/advanced.txt:276
# 20c2f6eb1d134439b7647b4ae8dcaa76
msgid "Looking for tests in any file below the current directory whose name matches the pattern ``test*.py``."
msgstr ""

#: ../../topics/testing/advanced.txt:279
# c6522d4d31714de8856b27da9d6ed8a5
msgid "Creating the test databases."
msgstr ""

#: ../../topics/testing/advanced.txt:281
# 48d310e174454c98af1a1f8c87fc2eb1
msgid "Running ``migrate`` to install models and initial data into the test databases."
msgstr ""

#: ../../topics/testing/advanced.txt:284
# 9c4f302a567d4c9ca5dc9afac46bdaeb
msgid "Running the tests that were found."
msgstr ""

#: ../../topics/testing/advanced.txt:286
# ad750f663ece48ee9033637a5a7070db
msgid "Destroying the test databases."
msgstr ""

#: ../../topics/testing/advanced.txt:288
# 184c97aac9074791be4a1d049b89e080
msgid "Performing global post-test teardown."
msgstr ""

#: ../../topics/testing/advanced.txt:290
# 19d3abf772d344849b4852217991c95d
msgid "If you define your own test runner class and point :setting:`TEST_RUNNER` at that class, Django will execute your test runner whenever you run ``./manage.py test``. In this way, it is possible to use any test framework that can be executed from Python code, or to modify the Django test execution process to satisfy whatever testing requirements you may have."
msgstr ""

#: ../../topics/testing/advanced.txt:299
# a6bb183aa7164aeb8a035f187ca705cd
msgid "Defining a test runner"
msgstr ""

#: ../../topics/testing/advanced.txt:305
# 77e319fa4fda416d89663076aa2962ba
msgid "A test runner is a class defining a ``run_tests()`` method. Django ships with a ``DiscoverRunner`` class that defines the default Django testing behavior. This class defines the ``run_tests()`` entry point, plus a selection of other methods that are used to by ``run_tests()`` to set up, execute and tear down the test suite."
msgstr ""

#: ../../topics/testing/advanced.txt:313
# 5f792ee486b34ba89b9494f15d7f6a69
msgid "``DiscoverRunner`` will search for tests in any file matching ``pattern``."
msgstr ""

#: ../../topics/testing/advanced.txt:315
# e194edb96edf4b908839db1a36075eae
msgid "``top_level`` can be used to specify the directory containing your top-level Python modules. Usually Django can figure this out automatically, so it's not necessary to specify this option. If specified, it should generally be the directory containing your ``manage.py`` file."
msgstr ""

#: ../../topics/testing/advanced.txt:320
# fbb8fd0573ee4db7a902a5d27c8676f8
msgid "``verbosity`` determines the amount of notification and debug information that will be printed to the console; ``0`` is no output, ``1`` is normal output, and ``2`` is verbose output."
msgstr ""

#: ../../topics/testing/advanced.txt:324
# 5ce4093d921645a389041e0d5b8c5adf
msgid "If ``interactive`` is ``True``, the test suite has permission to ask the user for instructions when the test suite is executed. An example of this behavior would be asking for permission to delete an existing test database. If ``interactive`` is ``False``, the test suite must be able to run without any manual intervention."
msgstr ""

#: ../../topics/testing/advanced.txt:330
# 08ba59b7b88142a7b8d7ee4e2bd18af5
msgid "If ``failfast`` is ``True``, the test suite will stop running after the first test failure is detected."
msgstr ""

#: ../../topics/testing/advanced.txt:333
# b2217ee6534c4457897ce215484c8a88
msgid "Django may, from time to time, extend the capabilities of the test runner by adding new arguments. The ``**kwargs`` declaration allows for this expansion. If you subclass ``DiscoverRunner`` or write your own test runner, ensure it accepts ``**kwargs``."
msgstr ""

#: ../../topics/testing/advanced.txt:338
# 513aeb154a2d4a3c9834b410d9bfd9df
msgid "Your test runner may also define additional command-line options. If you add an ``option_list`` attribute to a subclassed test runner, those options will be added to the list of command-line options that the :djadmin:`test` command can use."
msgstr ""

#: ../../topics/testing/advanced.txt:344
# 4a57b9c65c0f4f46973daaae3abb730b
msgid "Attributes"
msgstr ""

#: ../../topics/testing/advanced.txt:350
# 4d22168900d54058bb2c5fc11af04bbd
msgid "The class used to build the test suite. By default it is set to ``unittest.TestSuite``. This can be overridden if you wish to implement different logic for collecting tests."
msgstr ""

#: ../../topics/testing/advanced.txt:358
# d52216862fe6401b993647107e52a649
msgid "This is the class of the low-level test runner which is used to execute the individual tests and format the results. By default it is set to ``unittest.TextTestRunner``. Despite the unfortunate similarity in naming conventions, this is not the same type of class as ``DiscoverRunner``, which covers a broader set of responsibilities. You can override this attribute to modify the way tests are run and reported."
msgstr ""

#: ../../topics/testing/advanced.txt:367
# 84f2267bf555415bb2ea03f54e858895
msgid "This is the class that loads tests, whether from TestCases or modules or otherwise and bundles them into test suites for the runner to execute. By default it is set to ``unittest.defaultTestLoader``. You can override this attribute if your tests are going to be loaded in unusual ways."
msgstr ""

#: ../../topics/testing/advanced.txt:374
# 7c0a2e2a241d4519a3da1192dc4f9fa6
msgid "This is the tuple of ``optparse`` options which will be fed into the management command's ``OptionParser`` for parsing arguments. See the documentation for Python's ``optparse`` module for more details."
msgstr ""

#: ../../topics/testing/advanced.txt:379
# f57a7a2beed64600ac157ec9dfd92768
msgid "Methods"
msgstr ""

#: ../../topics/testing/advanced.txt:383
# 42a60191aa0e43f383e5dd0bdf79882b
msgid "Run the test suite."
msgstr ""

#: ../../topics/testing/advanced.txt:385
# cb92fa1aed00474f8a4ef60702d4c339
msgid "``test_labels`` allows you to specify which tests to run and supports several formats (see :meth:`DiscoverRunner.build_suite` for a list of supported formats)."
msgstr ""

#: ../../topics/testing/advanced.txt:389
#: ../../topics/testing/advanced.txt:421
# ba585bae54cc4c4f90cc7f2308be2b62
# a3d795cbb6864e9fa9daf44c6a026c8f
msgid "``extra_tests`` is a list of extra ``TestCase`` instances to add to the suite that is executed by the test runner. These extra tests are run in addition to those discovered in the modules listed in ``test_labels``."
msgstr ""

#: ../../topics/testing/advanced.txt:393
# 71c22e309fd34bbc82f742f31f65e854
msgid "This method should return the number of tests that failed."
msgstr ""

#: ../../topics/testing/advanced.txt:397
# 47a5caf5dc4e4681ace351f9680245dd
msgid "Sets up the test environment by calling :func:`~django.test.utils.setup_test_environment` and setting :setting:`DEBUG` to ``False``."
msgstr ""

#: ../../topics/testing/advanced.txt:403
# 7e1678961a7f431581039866293d289b
msgid "Constructs a test suite that matches the test labels provided."
msgstr ""

#: ../../topics/testing/advanced.txt:405
# 3ce6eba7c2b64ce7a3b2094768745bde
msgid "``test_labels`` is a list of strings describing the tests to be run. A test label can take one of four forms:"
msgstr ""

#: ../../topics/testing/advanced.txt:408
# 52c03b9e22e645d895ffe273d67010fc
msgid "``path.to.test_module.TestCase.test_method`` -- Run a single test method in a test case."
msgstr ""

#: ../../topics/testing/advanced.txt:410
# 30962d78d3134401a7b66a179eb2fe88
msgid "``path.to.test_module.TestCase`` -- Run all the test methods in a test case."
msgstr ""

#: ../../topics/testing/advanced.txt:412
# cc9f3533e3134871b130663d30ba4088
msgid "``path.to.module`` -- Search for and run all tests in the named Python package or module."
msgstr ""

#: ../../topics/testing/advanced.txt:414
# 5ee9d7247c1649158481265487751e09
msgid "``path/to/directory`` -- Search for and run all tests below the named directory."
msgstr ""

#: ../../topics/testing/advanced.txt:417
# a3dc230c00ec47d7834942b4a9eab843
msgid "If ``test_labels`` has a value of ``None``, the test runner will search for tests in all files below the current directory whose names match its ``pattern`` (see above)."
msgstr ""

#: ../../topics/testing/advanced.txt:425
# 1ae50ffdbea04810b44a96cce66a8c17
msgid "Returns a ``TestSuite`` instance ready to be run."
msgstr ""

#: ../../topics/testing/advanced.txt:429
# c9f7a8a0e9184b0091e646f10ded491a
msgid "Creates the test databases."
msgstr ""

#: ../../topics/testing/advanced.txt:431
# 5328e88d501e4563915877d2868fb50e
msgid "Returns a data structure that provides enough detail to undo the changes that have been made. This data will be provided to the ``teardown_databases()`` function at the conclusion of testing."
msgstr ""

#: ../../topics/testing/advanced.txt:437
# be0aad4421254718b19e583d15d7397d
msgid "Runs the test suite."
msgstr ""

#: ../../topics/testing/advanced.txt:439
# 5c44e5fbd58147c1b0b0bc5d78e8be8e
msgid "Returns the result produced by the running the test suite."
msgstr ""

#: ../../topics/testing/advanced.txt:443
# 0a6d96fd0d924694b9146e43f9596bd5
msgid "Destroys the test databases, restoring pre-test conditions."
msgstr ""

#: ../../topics/testing/advanced.txt:445
# d600761fb80c43038ef9ac3b61d247b6
msgid "``old_config`` is a data structure defining the changes in the database configuration that need to be reversed. It is the return value of the ``setup_databases()`` method."
msgstr ""

#: ../../topics/testing/advanced.txt:451
# f8ac56cdfd56442f988275f409179ffe
msgid "Restores the pre-test environment."
msgstr ""

#: ../../topics/testing/advanced.txt:455
# 3fb07efe5aca4777bd4d8386c09c2294
msgid "Computes and returns a return code based on a test suite, and the result from that test suite."
msgstr ""

#: ../../topics/testing/advanced.txt:460
# 4b5f7c4463f2479886dcceb16ab63a2a
msgid "Testing utilities"
msgstr ""

#: ../../topics/testing/advanced.txt:463
# 7cf328e8eb494ca88dae7c9e1db6e31a
msgid "django.test.utils"
msgstr ""

#: ../../topics/testing/advanced.txt:468
# fed39c7658474a8db46960fb87abff81
msgid "To assist in the creation of your own test runner, Django provides a number of utility methods in the ``django.test.utils`` module."
msgstr ""

#: ../../topics/testing/advanced.txt:473
# 5f4f160a4ce44407b82ade8f0a6bfbbf
msgid "Performs any global pre-test setup, such as the installing the instrumentation of the template rendering system and setting up the dummy email outbox."
msgstr ""

#: ../../topics/testing/advanced.txt:479
# a24172ef8705496698f58cfd5d79bb59
msgid "Performs any global post-test teardown, such as removing the black magic hooks into the template system and restoring normal email services."
msgstr ""

#: ../../topics/testing/advanced.txt:484
# a49730f8bc814b009c9c09fdacf3477f
msgid "django.db.connection.creation"
msgstr ""

#: ../../topics/testing/advanced.txt:488
# cca86f10d72c4a0788f2b1a72ae95c20
msgid "The creation module of the database backend also provides some utilities that can be useful during testing."
msgstr ""

#: ../../topics/testing/advanced.txt:493
# 4d4dd1f64e534f8cacf4ef3da5cd202c
msgid "Creates a new test database and runs ``migrate`` against it."
msgstr ""

#: ../../topics/testing/advanced.txt:495
# afdc2fe4f21b4328b042fe78681cbbf9
msgid "``verbosity`` has the same behavior as in ``run_tests()``."
msgstr ""

#: ../../topics/testing/advanced.txt:497
# 5346b55d898a4b90912683e33883ca2f
msgid "``autoclobber`` describes the behavior that will occur if a database with the same name as the test database is discovered:"
msgstr ""

#: ../../topics/testing/advanced.txt:500
# fa88329f12684436acec0eeaf0fd38c5
msgid "If ``autoclobber`` is ``False``, the user will be asked to approve destroying the existing database. ``sys.exit`` is called if the user does not approve."
msgstr ""

#: ../../topics/testing/advanced.txt:504
# c2ed5ac4b1614cbb80957ff98a21cb8b
msgid "If autoclobber is ``True``, the database will be destroyed without consulting the user."
msgstr ""

#: ../../topics/testing/advanced.txt:507
# f73b6e7063da4553bd5ab5c94433376c
msgid "``serialize`` determines if Django serializes the database into an in-memory JSON string before running tests (used to restore the database state between tests if you don't have transactions). You can set this to False to significantly speed up creation time if you know you don't need data persistence outside of test fixtures."
msgstr ""

#: ../../topics/testing/advanced.txt:513
# 6ae05a3b603e485d819ff4cbd2e450f9
msgid "Returns the name of the test database that it created."
msgstr ""

#: ../../topics/testing/advanced.txt:515
# e3c3092e733c43af8b1fb7482917b3ff
msgid "``create_test_db()`` has the side effect of modifying the value of :setting:`NAME` in :setting:`DATABASES` to match the name of the test database."
msgstr ""

#: ../../topics/testing/advanced.txt:521
# 743c16f7e54847759d4b645f07336f45
msgid "The ``serialize`` argument was added."
msgstr ""

#: ../../topics/testing/advanced.txt:525
# aab81756555741678511c0ebf485f031
msgid "Destroys the database whose name is the value of :setting:`NAME` in :setting:`DATABASES`, and sets :setting:`NAME` to the value of ``old_database_name``."
msgstr ""

#: ../../topics/testing/advanced.txt:529
# dea7c1ef2e8e4d3a839fb078d779499d
msgid "The ``verbosity`` argument has the same behavior as for :class:`~django.test.runner.DiscoverRunner`."
msgstr ""

#: ../../topics/testing/advanced.txt:535
# 9feae26df887495887907a1ea2ddd833
msgid "Integration with coverage.py"
msgstr ""

#: ../../topics/testing/advanced.txt:537
# 61bd3431ff5940c6a2dadda0b6a386c5
msgid "Code coverage describes how much source code has been tested. It shows which parts of your code are being exercised by tests and which are not. It's an important part of testing applications, so it's strongly recommended to check the coverage of your tests."
msgstr ""

#: ../../topics/testing/advanced.txt:542
# 2f17f662ea504142bbeec4213561cea5
msgid "Django can be easily integrated with `coverage.py`_, a tool for measuring code coverage of Python programs. First, `install coverage.py`_. Next, run the following from your project folder containing ``manage.py``::"
msgstr ""

#: ../../topics/testing/advanced.txt:548
# 8d9812fb374b4672840568fc56fdaba6
msgid "This runs your tests and collects coverage data of the executed files in your project. You can see a report of this data by typing following command::"
msgstr ""

#: ../../topics/testing/advanced.txt:553
# e425ac445381456aa102853b9ea16019
msgid "Note that some Django code was executed while running tests, but it is not listed here because of the ``source`` flag passed to the previous command."
msgstr ""

#: ../../topics/testing/advanced.txt:556
# a109bfddf2a44935be7cc4c11cb0cd27
msgid "For more options like annotated HTML listings detailing missed lines, see the `coverage.py`_ docs."
msgstr ""

