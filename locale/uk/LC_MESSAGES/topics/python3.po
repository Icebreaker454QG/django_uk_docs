#
#
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"Last-Translator: Zoriana Zaiats <sorenabell@quintagroup.com>\n"
"Language-Team: American English <support@quintagroup.com>\n"
"Content-Transfer-Encoding: 8bit\n"
"Content-Type: text/plain; charset=UTF-8\n"
"MIME-Version: 1.0\n"
"PO-Revision-Date: 2015-06-15 16:16+0300\n"
"Language: uk\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<"
"=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

# 73b74eafe4664f6b99ab4197526c6224
#: ../../topics/python3.txt:3
msgid "Porting to Python 3"
msgstr "Портування на Python 3"

# 616d210d5e4b40d2a5207c907a4341a8
#: ../../topics/python3.txt:5
msgid ""
"Django 1.5 is the first version of Django to support Python 3. The same code"
" runs both on Python 2 (≥ 2.6.5) and Python 3 (≥ 3.2), thanks to the six_ "
"compatibility layer."
msgstr ""
"Django 1.5 - це перша версія Django, що підтримує Python 3. Ваш код може "
"працювати з обома версіями Python: Python 2 (≥ 2.6.5) і Python 3 (≥ 3.2), "
"завдяки додатку `six <http://pythonhosted.org/six/>`_."

# d2dbb5a04e044e3caf61ae874ecd3eea
#: ../../topics/python3.txt:11
msgid ""
"This document is primarily targeted at authors of pluggable application who "
"want to support both Python 2 and 3. It also describes guidelines that apply"
" to Django's code."
msgstr ""
"Цей документ призначений в першу чергу для авторів повторно використовуваних"
" додатків, які хочуть забезпечити підтримку як Python 2, "
"так і Python 3. Він також описує принципи написання сумісного коду на "
"Django."

# c1364463979543da8c954707056c7635
#: ../../topics/python3.txt:16
msgid "Philosophy"
msgstr "Філософія"

# cba7e8efb97e4682981ca13ddc5c0c99
#: ../../topics/python3.txt:18
msgid ""
"This document assumes that you are familiar with the changes between Python "
"2 and Python 3. If you aren't, read `Python's official porting guide`_ "
"first. Refreshing your knowledge of unicode handling on Python 2 and 3 will "
"help; the `Pragmatic Unicode`_ presentation is a good resource."
msgstr ""
"Передбачається, що ви знайомі з відмінностями версій Python 2 і 3. Якщо це "
"не так, спочатку прочитайте `керівництво з портування "
"<http://docs.python.org/3/howto/pyporting.html>`_."

# 180c9a013efd4757aee7f716e982e3f5
#: ../../topics/python3.txt:23
msgid ""
"Django uses the *Python 2/3 Compatible Source* strategy. Of course, you're "
"free to chose another strategy for your own code, especially if you don't "
"need to stay compatible with Python 2. But authors of pluggable applications"
" are encouraged to use the same porting strategy as Django itself."
msgstr ""
"Django дотримується стратегії використання сумісного коду. Звичайно, ви "
"можете обрати інший підхід при написанні вашого проекту, особливо у випадку,"
" якщо вам не потрібна підтримка Python 2. Але авторам повторно "
"використовуваних "
"додатків все ж рекомендується дотримуватися тієї ж стратегії, що і Django."

# 744be2aa87da4546b51afaeb3305d672
#: ../../topics/python3.txt:28
msgid ""
"Writing compatible code is much easier if you target Python ≥ 2.6. Django "
"1.5 introduces compatibility tools such as :mod:`django.utils.six`, which is"
" a customized version of the :mod:`six module <six>`. For convenience, "
"forwards-compatible aliases were introduced in Django 1.4.2. If your "
"application takes advantage of these tools, it will require Django ≥ 1.4.2."
msgstr ""
"Написання сумісного коду буде набагато легшим, якщо ви використовуєте Python "
"≥ 2.6. Django 1.5 включає деякі інструменти для сумісності додатків, такі "
"як :mod:`six module <six>`. Для зручності, деякі псевдоніми були включені"
" вже в Django 1.4.2. Якщо ваш додаток використовує ці інструменти, вам буде "
"потрібна версія Django ≥ 1.4.2."

# 6ef8a45b4d5f48d9ae3eb53d13530b51
#: ../../topics/python3.txt:34
msgid ""
"Obviously, writing compatible source code adds some overhead, and that can "
"cause frustration. Django's developers have found that attempting to write "
"Python 3 code that's compatible with Python 2 is much more rewarding than "
"the opposite. Not only does that make your code more future-proof, but "
"Python 3's advantages (like the saner string handling) start shining "
"quickly. Dealing with Python 2 becomes a backwards compatibility "
"requirement, and we as developers are used to dealing with such constraints."
msgstr ""
"Очевидно, написання сумісного коду вимагатиме додаткових сил, що може "
"призвести"
" до розчарування. Розробники Django виявили, що написання коду на Python 3, "
"який був би сумісний з Python 2, все ж приносить більше користі, ніж "
"навпаки."

# 4993bb058ae3440c862ed77944d46163
#: ../../topics/python3.txt:42
msgid ""
"Porting tools provided by Django are inspired by this philosophy, and it's "
"reflected throughout this guide."
msgstr ""
"Інструменти портування, що надаються Django, відповідають поданій "
"філософії та будуть описані в даній інструкції."

# 23ea834f20a447489a12c44baf92a325
#: ../../topics/python3.txt:49
msgid "Porting tips"
msgstr "Поради щодо портування"

# 1418559761a441229be0e20658c63ede
#: ../../topics/python3.txt:52
msgid "Unicode literals"
msgstr "Рядки Unicode"

# e39a977d118043d39a5d69105d8a785d
#: ../../topics/python3.txt:54
msgid "This step consists in:"
msgstr "Цей крок полягає в наступному:"

# ad70197c40944d68be3766b9fba1402f
#: ../../topics/python3.txt:56
msgid ""
"Adding ``from __future__ import unicode_literals`` at the top of your Python"
" modules -- it's best to put it in each and every module, otherwise you'll "
"keep checking the top of your files to see which mode is in effect;"
msgstr ""
"Додавання ``from __future__ import unicode_literals`` першим рядком в ваш "
"модуль Python - буде краще, якщо цей рядок присутній в кожному модулі, в "
"іншому випадку ви повинні простежити, який режим використовується;"

# aba2ca0afcc642d5b8e95741b1d7381f
#: ../../topics/python3.txt:59
msgid "Removing the ``u`` prefix before unicode strings;"
msgstr "Видалення префікса ``u`` перед рядками unicode;"

# fafef41992254e1b921dc8adb05501ab
#: ../../topics/python3.txt:60
msgid "Adding a ``b`` prefix before bytestrings."
msgstr "Додавання префікса ``b`` перед байтовими рядками (bytestrings)."

# 372b513dbedb44e18d233ca7df4a89e3
#: ../../topics/python3.txt:62
msgid ""
"Performing these changes systematically guarantees backwards compatibility."
msgstr "Систематичне виконання цього кроку гарантує зворотну сумісність."

# 79576245339640d3b437af0a2e44edae
#: ../../topics/python3.txt:64
msgid ""
"However, Django applications generally don't need bytestrings, since Django "
"only exposes unicode interfaces to the programmer. Python 3 discourages "
"using bytestrings, except for binary data or byte-oriented interfaces. "
"Python 2 makes bytestrings and unicode strings effectively interchangeable, "
"as long as they only contain ASCII data. Take advantage of this to use "
"unicode strings wherever possible and avoid the ``b`` prefixes."
msgstr ""
"Однак, в додатках Django зазвичай не використовуються байтові рядки, "
"оскільки Django надає лише інтерфейс unicode для програмістів. При "
"використанні Python 3 не рекомендується використовувати байтові рядки, за "
"винятком двійкових даних або байт-орієнтованих інтерфейсів."

# 1325739f74104c17811a36cb348e620b
#: ../../topics/python3.txt:73
msgid ""
"Python 2's ``u`` prefix is a syntax error in Python 3.2 but it will be "
"allowed again in Python 3.3 thanks to :pep:`414`. Thus, this transformation "
"is optional if you target Python ≥ 3.3. It's still recommended, per the "
"\"write Python 3 code\" philosophy."
msgstr ""
"Префікс ``u`` з Python 2 у версії Python 3.2 викличе синтаксичну помилку, "
"але це буде виправлено в Python 3.3 завдяки :pep:`414`. Таким чином, це "
"перетворення є опціональним для Python ≥ 3.3. Це все ще рекомендується в "
"філософії \"пишіть Python 3 код\" (\"write Python 3 code\")."

# e157eb064a4c4d799c3e5a7d8097522c
# a0a570b14fca44d4957b5083876a32d7
#: ../../topics/python3.txt:79 ../../topics/python3.txt:338
msgid "String handling"
msgstr "Обробка рядків"

# 37d1228739e545dfa657ef855680360c
#: ../../topics/python3.txt:81
msgid ""
"Python 2's `unicode`_ type was renamed :class:`str` in Python 3, ``str()`` "
"was renamed :func:`bytes`, and `basestring`_ disappeared. six_ provides "
":ref:`tools <string-handling-with-six>` to deal with these changes."
msgstr ""
"Тип рядка `unicode`_ в Python 2 був перейменований в тип :class:`str` для "
"Python 3, ``str()`` був перейменований в :func:`bytes`, зник тип "
"`basestring`_. `six <http://pythonhosted.org/six/>`_ "
"надає :ref:`інструменти <string-handling-with-six>`, що враховують ці"
" зміни."

# a30151ff8283405ca82953a73fd2c852
#: ../../topics/python3.txt:86
msgid ""
"Django also contains several string related classes and functions in the "
":mod:`django.utils.encoding` and :mod:`django.utils.safestring` modules. "
"Their names used the words ``str``, which doesn't mean the same thing in "
"Python 2 and Python 3, and ``unicode``, which doesn't exist in Python 3. In "
"order to avoid ambiguity and confusion these concepts were renamed ``bytes``"
" and ``text``."
msgstr ""
"Django також містить кілька пов'язаних класів і функцій в модулях "
":mod:`django.utils.encoding` і :mod:`django.utils.safestring`. Їх імена "
"використовують тип ``str``, який неоднаковий в Python 2 і Python 3, а також "
"``unicode``, якого просто немає в Python 3."

# 3d81ce001f2d478bb972c0a02477a4a7
#: ../../topics/python3.txt:93
msgid "Here are the name changes in :mod:`django.utils.encoding`:"
msgstr "Це імена, які були змінені в :mod:`django.utils.encoding`:"

# 366ec21f8a064be7a1f65e5ce9a19f7c
# 578ace3d415049fb9cd8d0990372ba3c
#: ../../topics/python3.txt:96 ../../topics/python3.txt:120
msgid "Old name"
msgstr "Старе ім'я"

# d28ca5a5ae2e46f7bda38d5869219664
# 63632d53ab2842a19aebab90e742b76f
#: ../../topics/python3.txt:96 ../../topics/python3.txt:120
msgid "New name"
msgstr "Нове ім'я"

# b326919a7cc540468f121bd9a379af26
#: ../../topics/python3.txt:98
msgid "``smart_str``"
msgstr "``smart_str``"

# dabca05d000040c28ea76f752fd92b2d
#: ../../topics/python3.txt:98
msgid "``smart_bytes``"
msgstr "``smart_bytes``"

# 8e6859c4c33b40239af72a736fa14abd
#: ../../topics/python3.txt:99
msgid "``smart_unicode``"
msgstr "``smart_unicode``"

# 4d490b3f87124030bba5423eab1a1d3a
#: ../../topics/python3.txt:99
msgid "``smart_text``"
msgstr "``smart_text``"

# 992d78e08b4745a7b7e8971306ea2129
#: ../../topics/python3.txt:100
msgid "``force_unicode``"
msgstr "``force_unicode``"

# ddfa65512f6f4c55b6b8489638f08b2e
#: ../../topics/python3.txt:100
msgid "``force_text``"
msgstr "``force_text``"

# fb51be5ccc7e4789b8009db0b1c5b875
#: ../../topics/python3.txt:103
msgid ""
"For backwards compatibility, the old names still work on Python 2. Under "
"Python 3, ``smart_str`` is an alias for ``smart_text``."
msgstr ""
"Для підтримки зворотної сумісності старі імена ще працюють на Python 2. У "
"Python 3 ``smart_str`` є псевдонімом ``smart_text``."

# bf266d7d44ae4b48999567bad3f26a21
# 2980257855b9490ba192e1716017cd9b
#: ../../topics/python3.txt:106 ../../topics/python3.txt:132
msgid "For forwards compatibility, the new names work as of Django 1.4.2."
msgstr ""
"Для забезпечення подальшої сумісності нові найменування працюють як в Django"
" 1.4.2."

# 1c6ea9d96c824ab08be13655bdff115c
#: ../../topics/python3.txt:110
msgid ""
":mod:`django.utils.encoding` was deeply refactored in Django 1.5 to provide "
"a more consistent API. Check its documentation for more information."
msgstr ""
":mod:`django.utils.encoding` був дуже сильно змінений в Django 1.5 для "
"поліпшення API. Перегляньте відповідну документацію для отримання більш "
"докладної інформації."

# 6c461bb1417448a4982c7a1a9d293798
#: ../../topics/python3.txt:114
msgid ""
":mod:`django.utils.safestring` is mostly used via the "
":func:`~django.utils.safestring.mark_safe` and "
":func:`~django.utils.safestring.mark_for_escaping` functions, which didn't "
"change. In case you're using the internals, here are the name changes:"
msgstr ""
":mod:`django.utils.safestring` в основному використовується через "
":func:`~django.utils.safestring.mark_safe` і "
":func:`~django.utils.safestring.mark_for_escaping` функції, які не "
"змінилися. У випадку, якщо ви використовуєте щось інше, див. таблицю "
"відповідностей:"

# 64a90fd909454a7a82b14af389d6bdb3
#: ../../topics/python3.txt:122
msgid "``EscapeString``"
msgstr "``EscapeString``"

# 10ca8ca379614d00bd5308f202e36a41
#: ../../topics/python3.txt:122
msgid "``EscapeBytes``"
msgstr "``EscapeBytes``"

# 78502a213f8a40d2856a502efe026b2c
#: ../../topics/python3.txt:123
msgid "``EscapeUnicode``"
msgstr "``EscapeUnicode``"

# e81e31286f16428584fef5351e7bc9dd
#: ../../topics/python3.txt:123
msgid "``EscapeText``"
msgstr "``EscapeText``"

# 7dd537aa4135405cb86fca3426e4bf1a
#: ../../topics/python3.txt:124
msgid "``SafeString``"
msgstr "``SafeString``"

# 918f7c8b19094525a5d6cebb623cf911
#: ../../topics/python3.txt:124
msgid "``SafeBytes``"
msgstr "``SafeBytes``"

# 5e7b3db091db4806a8658fdedeba5d97
#: ../../topics/python3.txt:125
msgid "``SafeUnicode``"
msgstr "``SafeUnicode``"

# fbe3d7db20ed4434b87b6aca8fc4fbfe
#: ../../topics/python3.txt:125
msgid "``SafeText``"
msgstr "``SafeText``"

# be005bc310b347aeaeb699a58b6fa833
#: ../../topics/python3.txt:128
msgid ""
"For backwards compatibility, the old names still work on Python 2. Under "
"Python 3, ``EscapeString`` and ``SafeString`` are aliases for ``EscapeText``"
" and ``SafeText`` respectively."
msgstr ""
"Для підтримки зворотної сумісності старі імена ще працюють на Python 2. У "
"Python 3, ``EscapeString`` і ``SafeString`` є псевдонімами для "
"``EscapeText`` і ``SafeText`` відповідно."

# 20811b9e032e4c9fa12087999ab2a928
#: ../../topics/python3.txt:135
msgid ":meth:`~object.__str__` and ` __unicode__()`_ methods"
msgstr ":meth:`~object.__str__` і `__unicode__()`_ методи"

# 99d74b0c87e74b9b8ed8293aa0ee407a
#: ../../topics/python3.txt:137
msgid ""
"In Python 2, the object model specifies :meth:`~object.__str__` and ` "
"__unicode__()`_ methods. If these methods exist, they must return ``str`` "
"(bytes) and ``unicode`` (text) respectively."
msgstr ""
"В Python 2 модель об'єкта має :meth:`~object.__str__` і `__unicode__()`_ "
"методи. Якщо ці методи існують, вони повинні повернути ``str`` (байт) і "
"``unicode`` (рядок) відповідно."

# 979bfb05799945589bd7e1522648a6e3
#: ../../topics/python3.txt:141
msgid ""
"The ``print`` statement and the :class:`str` built-in call "
":meth:`~object.__str__` to determine the human-readable representation of an"
" object. The ``unicode`` built-in calls ` __unicode__()`_ if it exists, and "
"otherwise falls back to :meth:`~object.__str__` and decodes the result with "
"the system encoding. Conversely, the :class:`~django.db.models.Model` base "
"class automatically derives :meth:`~object.__str__` from ` __unicode__()`_ "
"by encoding to UTF-8."
msgstr ""
"Оператор ``print`` і :class:`str` викличе вбудований метод "
":meth:`~object.__str__` для зручного представлення об'єкта. "
"``unicode`` викличе метод `__unicode__()`_, якщо він існує, в іншому випадку "
"поверне метод :meth:`~object.__str__` і декодує результат відповідно до "
"системних налаштувань. Базовий клас :class:`~django.db.models.Model` "
"автоматично отримає метод :meth:`~object.__str__` з ` __unicode__()`_ "
"шляхом кодування в UTF-8."

# f442de34e91f4d3ca111c13e3c9210e7
#: ../../topics/python3.txt:149
msgid ""
"In Python 3, there's simply :meth:`~object.__str__`, which must return "
"``str`` (text)."
msgstr ""
"У Python 3 є об'єкт :meth:`~object.__str__`, який повинен повернути "
"``str`` (рядок тексту)."

# 0195b1af1a2c47569b2877d7a6d07666
#: ../../topics/python3.txt:152
msgid ""
"(It is also possible to define :meth:`~object.__bytes__`, but Django "
"application have little use for that method, because they hardly ever deal "
"with ``bytes``.)"
msgstr ""
"(Також можна визначити :meth:`~object.__bytes__`, але додатки Django "
"практично не використовують цей метод, тому що, зазвичай, не мають справу з "
"байтами.)"

# 99c497358a4a4053b985b7f67457e904
#: ../../topics/python3.txt:155
msgid ""
"Django provides a simple way to define :meth:`~object.__str__` and ` "
"__unicode__()`_ methods that work on Python 2 and 3: you must define a "
":meth:`~object.__str__` method returning text and to apply the "
":func:`~django.utils.encoding.python_2_unicode_compatible` decorator."
msgstr ""
"Django надає простий спосіб визначити :meth:`~object.__str__` "
"і `__unicode__()` _ методи, які працюють на Python 2 і 3: необхідно визначити"
" метод :meth:`~object.__str__`, який повертає текст і застосувати декоратор "
":func:`~django.utils.encoding.python_2_unicode_compatible`."

# a4f613f16d7b4cab88f115f674a62079
#: ../../topics/python3.txt:160
msgid ""
"On Python 3, the decorator is a no-op. On Python 2, it defines appropriate `"
" __unicode__()`_ and :meth:`~object.__str__` methods (replacing the original"
" :meth:`~object.__str__` method in the process). Here's an example::"
msgstr ""
"В Python 3, декоратор нічого не виконує. В Python 2, він визначає відповідні"
" методи `__unicode__()`_ і :meth:`~object.__str__` (в процесі заміни "
"оригінального :meth:`~object.__str__` ). Ось приклад:"

# a656130e1b2549559b54359390642998
#: ../../topics/python3.txt:172
msgid "This technique is the best match for Django's porting philosophy."
msgstr "Цей метод є найбільш підходящим для портування в стилі Django."

# 01acecfa668b46d5a42e8bdaf168d0fe
#: ../../topics/python3.txt:174
msgid ""
"For forwards compatibility, this decorator is available as of Django 1.4.2."
msgstr ""
"Для забезпечення подальшої сумісності цей декоратор доступний з версії "
"Django 1.4.2."

# a4621b6ba60d460cb5d010525b45d80f
#: ../../topics/python3.txt:176
msgid ""
"Finally, note that :meth:`~object.__repr__` must return a ``str`` on all "
"versions of Python."
msgstr ""
"Нарешті, відзначимо, що метод :meth:`~object.__repr__` повинен повертати "
"``str`` у всіх версіях Python."

# 5f03d40bafa44ad680ce1d32e5278429
#: ../../topics/python3.txt:180
msgid ":class:`dict` and :class:`dict`-like classes"
msgstr ":class:`dict` і :class:`dict`-подібні класи"

# 3bbd7b9fc8a6424da80b6c6f45facfb4
#: ../../topics/python3.txt:182
msgid ""
":meth:`dict.keys`, :meth:`dict.items` and :meth:`dict.values` return lists "
"in Python 2 and iterators in Python 3. :class:`~django.http.QueryDict` and "
"the :class:`dict`-like classes defined in :mod:`django.utils.datastructures`"
" behave likewise in Python 3."
msgstr ""
":meth:`dict.keys`, :meth:`dict.items` і :meth:`dict.values` повертають "
"списки в Python 2 та ітератори в Python 3. :class:`~django.http.QueryDict` і "
":class:`dict`-подібні класи, визначені в :mod:`django.utils.datastructures` "
"ведуть себе аналогічно в Python 3."

# ff40a073c11b4e748a733564b02c868c
#: ../../topics/python3.txt:187
msgid ""
"six_ provides compatibility functions to work around this change: "
":func:`~six.iterkeys`, :func:`~six.iteritems`, and :func:`~six.itervalues`. "
"It also contains an undocumented ``iterlists`` function that works well for "
"``django.utils.datastructures.MultiValueDict`` and its subclasses."
msgstr ""
"`six <http://pythonhosted.org/six/>`_ надає інструменти, що враховують ці"
" зміни: :func:`~six.iterkeys`, :func:`~six.iteritems` і "
":func:`~six.itervalues`. "
"Разом з Django поставляється недокументована "
"функція :func:`~django.utils.six.iterlists` для "
"``django.utils.datastructures.MultiValueDict`` і його підкласів."

# 201b74e723644f9d8468496099e3c0fd
#: ../../topics/python3.txt:193
msgid ""
":class:`~django.http.HttpRequest` and :class:`~django.http.HttpResponse` "
"objects"
msgstr ""
"Об'єкти :class:`~django.http.HttpRequest` і "
":class:`~django.http.HttpResponse` "

# 3ceb87afb71f42e29cac0194dc9ff2f2
#: ../../topics/python3.txt:195
msgid "According to :pep:`3333`:"
msgstr "Відповідно до :pep:`3333`:"

# 215aaa72d8ad484699f1b83bf8fbeb84
#: ../../topics/python3.txt:197
msgid "headers are always ``str`` objects,"
msgstr "заголовки завжди є рядковими об'єктами (``str`` objects),"

# 0cb4c970d5b04dfa865c16612ca7cfb9
#: ../../topics/python3.txt:198
msgid "input and output streams are always ``bytes`` objects."
msgstr ""
"потік введення і виведення завжди є байтовим об'єктом (``bytes`` objects)."

# 88948cc07b1e4adb9dd50dde593caecf
#: ../../topics/python3.txt:200
msgid ""
"Specifically, :attr:`HttpResponse.content "
"<django.http.HttpResponse.content>` contains ``bytes``, which may become an "
"issue if you compare it with a ``str`` in your tests. The preferred solution"
" is to rely on :meth:`~django.test.SimpleTestCase.assertContains` and "
":meth:`~django.test.SimpleTestCase.assertNotContains`. These methods accept "
"a response and a unicode string as arguments."
msgstr ""
"Зокрема, :attr:`HttpResponse.content <django.http.HttpResponse.content>` "
"містить ``bytes``, що може стати джерелом проблем, якщо ви порівняєте його з"
" ``str`` у ваших тестах. Краще рішення в такому випадку: покладатися на"
" :meth:`~django.test.TestCase.assertContains` і "
":meth:`~django.test.TestCase.assertNotContains`. Ці методи приймають "
"відповідь та рядки в юнікод як аргументи."

# 1c03d78e51104a408969cd83e4c4cf1c
#: ../../topics/python3.txt:208
msgid "Coding guidelines"
msgstr "Рекомендації з написання коду"

# a8eadb52e65443d08115c17fe67b59c5
#: ../../topics/python3.txt:210
msgid ""
"The following guidelines are enforced in Django's source code. They're also "
"recommended for third-party application who follow the same porting "
"strategy."
msgstr ""
"Наступні рекомендації застосовуються у вихідних кодах Django. Також вони "
"рекомендуються для сторонніх додатків, які слідують аналогічній стратегії "
"портування."

# a869a3c7d68f405caf17caa17e1b87c5
#: ../../topics/python3.txt:214
msgid "Syntax requirements"
msgstr "Вимоги до синтаксису"

# ef474abd5d2e4b678d6f8e6f19a87105
#: ../../topics/python3.txt:217
msgid "Unicode"
msgstr "Юнікод (Unicode)"

# e077489ed6184df7b24dccae5dea27c1
#: ../../topics/python3.txt:219
msgid ""
"In Python 3, all strings are considered Unicode by default. The ``unicode`` "
"type from Python 2 is called ``str`` in Python 3, and ``str`` becomes "
"``bytes``."
msgstr ""
"В Python 3 всі рядки за замовчуванням вважаються рядками Unicode. Тип "
"``unicode`` з Python 2 аналогічний ``str`` в Python 3, а тип ``str`` стає "
"типом ``bytes``."

# 7f79a4c89f7a4098b5062a78af9d7624
#: ../../topics/python3.txt:223
msgid ""
"You mustn't use the ``u`` prefix before a unicode string literal because "
"it's a syntax error in Python 3.2. You must prefix byte strings with ``b``."
msgstr ""
"Ви не повинні використовувати префікс ``u`` перед рядками юнікода, оскільки "
"подібний синтаксис викличе помилку в Python 3.2. І ви зобов'язані ставити "
"префікс ``b`` перед об'єктом байта."

# ad9093d3eac74cd48c2b888c9a9bf4af
#: ../../topics/python3.txt:226
msgid ""
"In order to enable the same behavior in Python 2, every module must import "
"``unicode_literals`` from ``__future__``::"
msgstr ""
"Щоб в Python 2 з'явилася така ж поведінка, кожен модуль повинен імпортувати "
"``unicode_literals`` з ``__future__``::"

# 5886848f8cfd4457a85e7fdcffcc4632
#: ../../topics/python3.txt:234
msgid ""
"If you need a byte string literal under Python 2 and a unicode string "
"literal under Python 3, use the :class:`str` builtin::"
msgstr ""
"Якщо вам потрібні рядки байтів в Python 2 і рядки юнікода в Python 3, "
"використовуйте вбудовану функцію :class:`str`::"

# 3148ad32e6d847bd943491ea486e9547
#: ../../topics/python3.txt:239
msgid ""
"In Python 3, there aren't any automatic conversions between ``str`` and "
"``bytes``, and the :mod:`codecs` module became more strict. "
":meth:`str.encode` always returns ``bytes``, and ``bytes.decode`` always "
"returns ``str``. As a consequence, the following pattern is sometimes "
"necessary::"
msgstr ""
"В Python 3, немає ніяких автоматичних перетворень між ``str`` і ``bytes``, а"
" модуль :mod:`codecs` став більш суворим. Метод :meth:`str.decode` завжди "
"повертає тип ``bytes``, в свою чергу ``bytes.decode`` завжди повертає тип "
"``str``. В наслідок цього іноді може з'явитися така необхідність::"

# e7c59fa0e27d438a990736c729b1e58f
#: ../../topics/python3.txt:246
msgid "Be cautious if you have to `index bytestrings`_."
msgstr ""
"Будьте обережні з `index bytestrings "
"<http://docs.python.org/3/howto/pyporting.html#bytes-literals>`_."

# dfe0dc6ab43c4145a9589b2086c8f7fc
#: ../../topics/python3.txt:251
msgid "Exceptions"
msgstr "Винятки"

# 87625e6509cd42db888aa067759612e8
#: ../../topics/python3.txt:253
msgid "When you capture exceptions, use the ``as`` keyword::"
msgstr "При виклику винятка використовуйте як ключове слово ``as``::"

# 18f0a78a7d874300a05e30cde0339ea1
#: ../../topics/python3.txt:260
msgid "This older syntax was removed in Python 3::"
msgstr "Старий синтаксис був видалений з Python 3::"

# 5592d8aab856450c9c7f6a0ddae6a61b
#: ../../topics/python3.txt:267
msgid ""
"The syntax to reraise an exception with a different traceback also changed. "
"Use :func:`six.reraise`."
msgstr ""
"Синтаксис повторного виклику винятків також зазнав змін. Див. "
":func:`six.reraise`."

# bf7ff0fe5de748c99c803859f0dc87f3
#: ../../topics/python3.txt:271
msgid "Magic methods"
msgstr "Магічні методи (Magic methods)"

# f31b35e5a04b41388d93b2244a928015
#: ../../topics/python3.txt:273
msgid "Use the patterns below to handle magic methods renamed in Python 3."
msgstr ""
"Використовуйте шаблони, дані нижче, для обробки магічних методів, "
"перейменованих в Python 3."

# ee2917535bb84103b93d5cba85e08242
#: ../../topics/python3.txt:276
msgid "Iterators"
msgstr "Ітератори (Iterators)"

# 23a9254ad15643589bd5d32422364e71
#: ../../topics/python3.txt:288
msgid "Boolean evaluation"
msgstr "Булеві вирази (Boolean evaluation)"

# 40e3df19cda94b89b79c86882f5f9b8f
#: ../../topics/python3.txt:301
msgid "Division"
msgstr "Поділ"

# e5ebe7d2a9d74ce3b928632e7bf25db5
#: ../../topics/python3.txt:319
msgid ""
"Special methods are looked up on the class and not on the instance to "
"reflect the behavior of the Python interpreter."
msgstr "Спеціальні методи шукаються в класі, а не в об'єкті."

# d4367c8f1fe4427380f102a2758c99e9
#: ../../topics/python3.txt:325
msgid "Writing compatible code with six"
msgstr "Написання сумісного коду за допомогою six"

# ad450aefdd92418c9c4575108c7f319d
#: ../../topics/python3.txt:327
msgid ""
"six_ is the canonical compatibility library for supporting Python 2 and 3 in"
" a single codebase. Read its documentation!"
msgstr ""
"`six <http://pythonhosted.org/six/>`_ є канонічною бібліотекою для "
"одночасної підтримки Python 2 і 3. Ознайомтеся з його документацією!"

# 1e15b52ab73741dd9701e9a77a3cbd31
#: ../../topics/python3.txt:330
msgid ""
"A :mod:`customized version of six <django.utils.six>` is bundled with Django"
" as of version 1.4.2. You can import it as ``django.utils.six``."
msgstr ""
":mod:`Змінена версія six <django.utils.six>` входить в поставку Django "
"починаючи з версії 1.4.2. Ви можете імпортувати її як "
"``django.utils.six``."

# 54a7d4511ba0490b85ac31c651cf65ae
#: ../../topics/python3.txt:333
msgid "Here are the most common changes required to write compatible code."
msgstr ""
"Тут враховуються найбільш поширені зміни, що дозволить написати сумісний"
" код."

# e74ed84942aa49fa8f2fba3d626296b7
#: ../../topics/python3.txt:340
msgid ""
"The ``basestring`` and ``unicode`` types were removed in Python 3, and the "
"meaning of ``str`` changed. To test these types, use the following idioms::"
msgstr ""
"Типи ``basestring`` і ``unicode`` були видалені в Python 3, а метод ``str`` "
"змінений. Для перевірки цих типів, використовуйте наступні ідіоми::"

# 6969823ca2644fce91de291fc1aac7e3
#: ../../topics/python3.txt:347
msgid ""
"Python ≥ 2.6 provides ``bytes`` as an alias for ``str``, so you don't need "
":data:`six.binary_type`."
msgstr ""
"Python ≥ 2.6 надає тип ``bytes`` як псевдонім типу ``str``, так що вам не "
"потрібен :data:`six.binary_type`."

# 6afc9812203c48e6b9ea996244118c83
#: ../../topics/python3.txt:351
msgid "``long``"
msgstr "Тип ``long``"

# 4cdc5aee6f034faab9cbdfb6a94c3423
#: ../../topics/python3.txt:353
msgid ""
"The ``long`` type no longer exists in Python 3. ``1L`` is a syntax error. "
"Use :data:`six.integer_types` check if a value is an integer or a long::"
msgstr ""
"Типу ``long`` більше не існує в Python 3. ``1L`` викличе синтаксичну "
"помилку. Використовуйте :data:`six.integer_types`, щоб перевірити чи є число"
" простим цілим, чи це число типу ``long``::"

# 1d750d01c8d0465ebe6d7108b213317e
#: ../../topics/python3.txt:359
msgid "``xrange``"
msgstr "``xrange``"

# 7087e2f447874558aec14087637301b3
#: ../../topics/python3.txt:361
msgid "Import ``six.moves.xrange`` wherever you use ``xrange``."
msgstr ""
"Імпортуйте ``six.moves.xrange`` всюди, де ви використовували ``xrange``."

# 58eb2499e7624f93a7cdc27be97a7146
#: ../../topics/python3.txt:364
msgid "Moved modules"
msgstr "Переміщені модулі"

# 29f94e8e100046ffb2845e7a3c6a9c3e
#: ../../topics/python3.txt:366
msgid ""
"Some modules were renamed in Python 3. The ``django.utils.six.moves`` module"
" (based on the :mod:`six.moves module <six.moves>`) provides a compatible "
"location to import them."
msgstr ""
"Деякі модулі були перейменовані в Python 3. Модуль "
"``django.utils.six.moves`` (на основі :mod:`six.moves module <six.moves>`) "
"забезпечує сумісність з ними і ви можете імпортувати їх."

# 891baa21985340d9b849db5303b1ac63
#: ../../topics/python3.txt:371
msgid "PY2"
msgstr "PY2"

# 27115b7dedf842e3a671e94a07696fc2
#: ../../topics/python3.txt:373
msgid ""
"If you need different code in Python 2 and Python 3, check :data:`six.PY2`::"
msgstr ""
"Для отримання різного коду при використанні Python 2 і Python 3, перевірте "
":data:`six.PY2`::"

# eee549b9bb7c4f489062db05fb8e6582
#: ../../topics/python3.txt:378
msgid ""
"This is a last resort solution when :mod:`six` doesn't provide an "
"appropriate function."
msgstr ""
"Це рішення використовується в крайньому випадку, тільки коли :mod:`six` не"
" може забезпечити необхідний функціонал."

# 1710ef8805f54c339f0eeef05cd68a55
#: ../../topics/python3.txt:384
msgid "Django customized version of six"
msgstr "Модифікована версія six"

# b9d003a4b1c94bb19174f2933f0d3287
#: ../../topics/python3.txt:386
msgid ""
"The version of six bundled with Django (``django.utils.six``) includes a few"
" extras."
msgstr ""
"Версія six, що йде в поставці з Django (``django.utils.six``), має кілька "
"доповнень."

# e81cd7ed2fe64b79aecf38fccf144d16
#: ../../topics/python3.txt:391
msgid ""
"This replaces ``testcase.assertRaisesRegexp`` on Python 2, and "
"``testcase.assertRaisesRegex`` on Python 3. ``assertRaisesRegexp`` still "
"exists in current Python 3 versions, but issues a warning."
msgstr ""
"Це замінює ``testcase.assertRaisesRegexp`` в Python 2, і "
"``testcase.assertRaisesRegex`` в Python 3. ``assertRaisesRegexp`` все ще "
"доступний в поточній версії Python 3, але видасть попередження."

# e458ee3130004f14b415da001e5b00bc
#: ../../topics/python3.txt:397
msgid ""
"This replaces ``testcase.assertRegexpMatches`` on Python 2, and "
"``testcase.assertRegex`` on Python 3. ``assertRegexpMatches`` still exists "
"in current Python 3 versions, but issues a warning."
msgstr ""
"Це замінює ``testcase.assertRegexpMatches`` в Python 2, і "
"``testcase.assertRegex`` в Python 3. ``assertRegexpMatches`` все ще "
"доступний в поточній версії Python 3, але видасть попередження."

# 7dc889410f5d4d4e97adeed43d5d13e6
#: ../../topics/python3.txt:402
msgid ""
"In addition to six' defaults moves, Django's version provides ``thread`` as "
"``_thread`` and ``dummy_thread`` as ``_dummy_thread``."
msgstr ""
"На додаток до інструментів six Django надає ``thread`` як ``_thread`` і "
"``dummy_thread`` як ``_dummy_thread``."


