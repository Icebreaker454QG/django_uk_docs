#
#
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"Last-Translator: Zoriana Zaiats <sorenabell@quintagroup.com>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"Content-Transfer-Encoding: 8bit\n"
"Content-Type: text/plain; charset=UTF-8\n"
"MIME-Version: 1.0\n"
"PO-Revision-Date: 2015-03-18 17:14+0200\n"
"Language: uk\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

# ef3e98b540764808a9cd81adf4bf159e
#: ../../topics/i18n/index.txt:3
msgid "Internationalization and localization"
msgstr "Інтернаціоналізація та локалізація"

# 6f8badba115d4568867f3368a4a0f315
#: ../../topics/i18n/index.txt:14
msgid "Overview"
msgstr "Вступ"

# cd20d99195214e0b80f475ccb916f393
#: ../../topics/i18n/index.txt:16
msgid ""
"The goal of internationalization and localization is to allow a single Web "
"application to offer its content in languages and formats tailored to the "
"audience."
msgstr ""
"Метою інтернаціоналізації та локалізації є забезпечення можливості окремому "
"веб додатку надавати свій вміст на мові і у форматі, зрозумілому цільовій"
" аудиторії."

# 2137110052a24c1689ff00de18a53689
#: ../../topics/i18n/index.txt:20
msgid ""
"Django has full support for :doc:`translation of text "
"</topics/i18n/translation>`, :doc:`formatting of dates, times and numbers "
"</topics/i18n/formatting>`, and :doc:`time zones </topics/i18n/timezones>`."
msgstr ""
"Django володіє повним набором засобів для вирішення цього завдання: "
":doc:`переклад тексту </topics/i18n/translation>`, :doc:`форматування дати,"
" часу і чисел </topics/i18n/formatting>` і :doc:`підтримка часових поясів "
"</topics/i18n/timezones>`."

# b9e6cbe35805480f99d89dab9e1205c4
#: ../../topics/i18n/index.txt:24
msgid "Essentially, Django does two things:"
msgstr "По суті, Django забезпечує дві речі:"

# 3bc7637b22aa42d4973316149f8e7735
#: ../../topics/i18n/index.txt:26
msgid ""
"It allows developers and template authors to specify which parts of their "
"apps should be translated or formatted for local languages and cultures."
msgstr ""
"Він дозволяє розробникам та авторам шаблонів вказувати які саме частини їх "
"додатків повинні бути перекладені або відформатовані під використовувані мови"
" і традиції."

# f13158597f2747d1940134a4b7d1438f
#: ../../topics/i18n/index.txt:28
msgid ""
"It uses these hooks to localize Web apps for particular users according to "
"their preferences."
msgstr ""
"Він використовує ці мітки для локалізації веб додатків під конкретного "
"користувача, враховуючи його налаштування."

# 5575f9cba1a64c618df1f3967bb18153
#: ../../topics/i18n/index.txt:31
msgid ""
"Obviously, translation depends on the target language, and formatting "
"usually depends on the target country. This information is provided by "
"browsers in the ``Accept-Language`` header. However, the time zone isn't "
"readily available."
msgstr ""
"Очевидно, що переклад залежить від цільової мови, а форматування залежить "
"від цільової країни. Ця інформація надається переглядачами в заголовку "
"``Accept-Language``. Тим не менш, інформацію про часовий пояс не так просто "
"отримати."

# bd551c5e8b7b4706ba659d5f5c15322f
#: ../../topics/i18n/index.txt:36
msgid "Definitions"
msgstr "Термінологія"

# ce546a4ad3574464bcdc989ea34c4b6e
#: ../../topics/i18n/index.txt:38
msgid ""
"The words \"internationalization\" and \"localization\" often cause "
"confusion; here's a simplified definition:"
msgstr ""
"Слова \"інтернаціоналізація\" і \"локалізація\" часто викликають плутанину. "
"Зараз все буде зрозуміло:"

# a3dcdd658ada4d47b58ac5e67f15c073
#: ../../topics/i18n/index.txt:42
msgid "internationalization"
msgstr "інтернаціоналізація"

# e706a5a4a1344f45bd5268a29e84803a
#: ../../topics/i18n/index.txt:44
msgid "Preparing the software for localization. Usually done by developers."
msgstr ""
"Підготовка програмного забезпечення для локалізації. Зазвичай виконується "
"розробниками."

# 2c0f88021364449dbeb0357db6e70969
#: ../../topics/i18n/index.txt:45
msgid "localization"
msgstr "локалізація"

# 0ec88aa3468f415c9f7eec8777d330c8
#: ../../topics/i18n/index.txt:47
msgid ""
"Writing the translations and local formats. Usually done by translators."
msgstr ""
"Створення перекладів і локальних форматів. Зазвичай виконується "
"перекладачами."

# a82581e19ce34bee97a8d3cccdc0759a
#: ../../topics/i18n/index.txt:49
msgid ""
"More details can be found in the `W3C Web Internationalization FAQ`_, the "
"`Wikipedia article`_ or the `GNU gettext documentation`_."
msgstr ""
"Подробиці можна знайти в `ЧаВо W3C по інтернаціоналізації вебу "
"<http://www.w3.org/International/questions/qa-i18n>`_, у `Вікіпедії "
"<http://uk.wikipedia.org/wiki/Інтернаціоналізація>`_ або в `документації "
"GNU gettext <http://www.gnu.org/software/gettext/manual/gettext.html>`_. "

# d6f12e64fae6403baeca4eb09e617f22
#: ../../topics/i18n/index.txt:57
msgid ""
"Translation and formatting are controlled by :setting:`USE_I18N` and "
":setting:`USE_L10N` settings respectively. However, both features involve "
"internationalization and localization. The names of the settings are an "
"unfortunate result of Django's history."
msgstr ""
"Переклад і форматування контролюються параметрами :setting:`USE_I18N` і "
":setting:`USE_L10N` відповідно. Тим не менш, обидва функціонали беруть "
"участь в інтернаціоналізації та локалізації. Імена параметрів є не надто "
"вдалим результатом розвитку Django."

# 209d821919d44a0b89d72b323f92f6e7
#: ../../topics/i18n/index.txt:62
msgid ""
"Here are some other terms that will help us to handle a common language:"
msgstr ""
"Розглянемо ще кілька термінів, які допоможуть нам спілкуватися на зрозумілій"
" мові:"

# dfc9400f33b34aa889309139882d7fc4
#: ../../topics/i18n/index.txt:65
msgid "locale name"
msgstr "locale name"

# 53bb3cb5c1704ca9a4e217f5ef5d67ba
#: ../../topics/i18n/index.txt:67
msgid ""
"A locale name, either a language specification of the form ``ll`` or a "
"combined language and country specification of the form ``ll_CC``. Examples:"
" ``it``, ``de_AT``, ``es``, ``pt_BR``. The language part is always in lower "
"case and the country part in upper case. The separator is an underscore."
msgstr ""
"Ім'я локалі, або специфікація мови у вигляді ``ll`` або комбінація мови і "
"специфікації країни у вигляді ``ll_CC``. Приклади: ``it``, ``de_AT``, "
"``es``, ``pt_BR``. Мовна частина завжди вказується в нижньому регістрі, а "
"частина, що визначає країну, - у верхньому регістрі. Роздільником є символ "
"підкреслення."

# cef9261e71264aaf854c48cf1e9421ed
#: ../../topics/i18n/index.txt:72
msgid "language code"
msgstr "language code"

# 8ebaa6d9385c4ba59152d556dfb85add
#: ../../topics/i18n/index.txt:74
msgid ""
"Represents the name of a language. Browsers send the names of the languages "
"they accept in the ``Accept-Language`` HTTP header using this format. "
"Examples: ``it``, ``de-at``, ``es``, ``pt-br``. Both the language and the "
"country parts are in lower case. The separator is a dash."
msgstr ""
"Представляє ім'я мови. Використовуючи цей формат, переглядачі відправляють "
"імена мов, вміст на яких вони хочуть прийняти, в HTTP заголовку "
"``Accept-Language``. Приклади: ``it``, ``de-at``, ``es``, ``pt-br``. Обидві "
"частини (мова і країна) вказуються в нижньому регістрі. Роздільником є "
"символ тире."

# cdab16811c6249c382e9443c1800de95
#: ../../topics/i18n/index.txt:78
msgid "message file"
msgstr "message file"

# 0bcf1867ac904f2d999dc199dfe5ab98
#: ../../topics/i18n/index.txt:80
msgid ""
"A message file is a plain-text file, representing a single language, that "
"contains all available :term:`translation strings <translation string>` and "
"how they should be represented in the given language. Message files have a "
"``.po`` file extension."
msgstr ""
"Файл повідомлення є звичайним текстовим файлом, що представляє єдину мову, "
"який містить всі доступні :term:`рядки перекладу <translation string>` і"
" правила їх відображення для даної мови. Файли повідомлень мають розширення "
"``.po``."

# e901ad11230f42ae987a0de82ad97d69
#: ../../topics/i18n/index.txt:84
msgid "translation string"
msgstr "translation string"

# c6ef0e8935364491abc2f584505dde87
#: ../../topics/i18n/index.txt:86
msgid "A literal that can be translated."
msgstr "Рядок, який може бути перекладений."

# f10c0a4ae35e482092fd809d40df9f9d
#: ../../topics/i18n/index.txt:87
msgid "format file"
msgstr "format file"

# 7a4be2bd10fd457e8ae9bf43aaf2174b
#: ../../topics/i18n/index.txt:89
msgid ""
"A format file is a Python module that defines the data formats for a given "
"locale."
msgstr ""
"Файл формату є модулем мови Python і визначає формати даних для даної "
"локалі."


