# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../topics/i18n/timezones.txt:5
# 5ff7ed8509964e5b919ea57d5b92716b
msgid "Time zones"
msgstr ""

#: ../../topics/i18n/timezones.txt:10
# ab663c106d5b4dd5b3c3603d1500a474
msgid "Overview"
msgstr ""

#: ../../topics/i18n/timezones.txt:12
# 1643004f5a9e4473a4570c8710e76674
msgid "When support for time zones is enabled, Django stores date and time information in UTC in the database, uses time-zone-aware datetime objects internally, and translates them to the end user's time zone in templates and forms."
msgstr ""

#: ../../topics/i18n/timezones.txt:17
# 0fec79d892f54c34ae9bcb9194ba81d7
msgid "This is handy if your users live in more than one time zone and you want to display date and time information according to each user's wall clock."
msgstr ""

#: ../../topics/i18n/timezones.txt:20
# 89fd596336e54789a8f539ad1b9f03fb
msgid "Even if your Web site is available in only one time zone, it's still good practice to store data in UTC in your database. One main reason is Daylight Saving Time (DST). Many countries have a system of DST, where clocks are moved forward in spring and backward in autumn. If you're working in local time, you're likely to encounter errors twice a year, when the transitions happen. (The pytz_ documentation discusses `these issues`_ in greater detail.) This probably doesn't matter for your blog, but it's a problem if you over-bill or under-bill your customers by one hour, twice a year, every year. The solution to this problem is to use UTC in the code and use local time only when interacting with end users."
msgstr ""

#: ../../topics/i18n/timezones.txt:31
# 57f95c8c033e4037a31b53340fa522dd
msgid "Time zone support is disabled by default. To enable it, set :setting:`USE_TZ = True <USE_TZ>` in your settings file. Installing pytz_ is highly recommended, but may not be mandatory depending on your particular database backend, operating system and time zone. If you encounter an exception querying dates or times, please try installing it before filing a bug. It's as simple as:"
msgstr ""

#: ../../topics/i18n/timezones.txt:43
# 7e9313be56944ec7ab5ef12efcd8d368
msgid "The default :file:`settings.py` file created by :djadmin:`django-admin.py startproject <startproject>` includes :setting:`USE_TZ = True <USE_TZ>` for convenience."
msgstr ""

#: ../../topics/i18n/timezones.txt:49
# 0886c7d98cd24ff5b0ff5d3a0ed03fa0
msgid "There is also an independent but related :setting:`USE_L10N` setting that controls whether Django should activate format localization. See :doc:`/topics/i18n/formatting` for more details."
msgstr ""

#: ../../topics/i18n/timezones.txt:53
# 024c853892c64ad884a5fcc8571787f9
msgid "If you're wrestling with a particular problem, start with the :ref:`time zone FAQ <time-zones-faq>`."
msgstr ""

#: ../../topics/i18n/timezones.txt:57
# d498ef5bbfab4e71b09d1848e1e6b84a
msgid "Concepts"
msgstr ""

#: ../../topics/i18n/timezones.txt:62
# 3d916830f0f54869ab2983e114394483
msgid "Naive and aware datetime objects"
msgstr ""

#: ../../topics/i18n/timezones.txt:64
# f32b07a3a05b46889954cc5e53b7f6d3
msgid "Python's :class:`datetime.datetime` objects have a ``tzinfo`` attribute that can be used to store time zone information, represented as an instance of a subclass of :class:`datetime.tzinfo`. When this attribute is set and describes an offset, a datetime object is **aware**. Otherwise, it's **naive**."
msgstr ""

#: ../../topics/i18n/timezones.txt:69
# 3193e8b413414cfeb57cbf95159ca64f
msgid "You can use :func:`~django.utils.timezone.is_aware` and :func:`~django.utils.timezone.is_naive` to determine whether datetimes are aware or naive."
msgstr ""

#: ../../topics/i18n/timezones.txt:73
# 8d992d7be2de4e47b6120c4dc7c3ca2f
msgid "When time zone support is disabled, Django uses naive datetime objects in local time. This is simple and sufficient for many use cases. In this mode, to obtain the current time, you would write::"
msgstr ""

#: ../../topics/i18n/timezones.txt:81
# 76bcc575249b487da4a5282c163d6ee2
msgid "When time zone support is enabled (:setting:`USE_TZ=True <USE_TZ>`), Django uses time-zone-aware datetime objects. If your code creates datetime objects, they should be aware too. In this mode, the example above becomes::"
msgstr ""

#: ../../topics/i18n/timezones.txt:91
# 3d017bbcc5f843588ebb5210756cd165
msgid "Dealing with aware datetime objects isn't always intuitive. For instance, the ``tzinfo`` argument of the standard datetime constructor doesn't work reliably for time zones with DST. Using UTC is generally safe; if you're using other time zones, you should review the `pytz`_ documentation carefully."
msgstr ""

#: ../../topics/i18n/timezones.txt:99
# 89865493383345ecaca8b31e2673c686
msgid "Python's :class:`datetime.time` objects also feature a ``tzinfo`` attribute, and PostgreSQL has a matching ``time with time zone`` type. However, as PostgreSQL's docs put it, this type \"exhibits properties which lead to questionable usefulness\"."
msgstr ""

#: ../../topics/i18n/timezones.txt:104
# 5c7e111030a645ecb73a2a2a0e91c34a
msgid "Django only supports naive time objects and will raise an exception if you attempt to save an aware time object, as a timezone for a time with no associated date does not make sense."
msgstr ""

#: ../../topics/i18n/timezones.txt:111
# ef71c2da0c754a789e1653499464ca9a
msgid "Interpretation of naive datetime objects"
msgstr ""

#: ../../topics/i18n/timezones.txt:113
# f5112ffae15a408db796ef57be288ac7
msgid "When :setting:`USE_TZ` is ``True``, Django still accepts naive datetime objects, in order to preserve backwards-compatibility. When the database layer receives one, it attempts to make it aware by interpreting it in the :ref:`default time zone <default-current-time-zone>` and raises a warning."
msgstr ""

#: ../../topics/i18n/timezones.txt:118
# e40b4a44a9ec4cf997cfa3ec5177b2b2
msgid "Unfortunately, during DST transitions, some datetimes don't exist or are ambiguous. In such situations, pytz_ raises an exception. Other :class:`~datetime.tzinfo` implementations, such as the local time zone used as a fallback when pytz_ isn't installed, may raise an exception or return inaccurate results. That's why you should always create aware datetime objects when time zone support is enabled."
msgstr ""

#: ../../topics/i18n/timezones.txt:125
# a03ca3314b33484f9a0aad2989a71700
msgid "In practice, this is rarely an issue. Django gives you aware datetime objects in the models and forms, and most often, new datetime objects are created from existing ones through :class:`~datetime.timedelta` arithmetic. The only datetime that's often created in application code is the current time, and :func:`timezone.now() <django.utils.timezone.now>` automatically does the right thing."
msgstr ""

#: ../../topics/i18n/timezones.txt:135
# b79c72ca10544e9b89a7fea481559fe3
msgid "Default time zone and current time zone"
msgstr ""

#: ../../topics/i18n/timezones.txt:137
# c964c1806b7e4c02aa5bbf53862cfad2
msgid "The **default time zone** is the time zone defined by the :setting:`TIME_ZONE` setting."
msgstr ""

#: ../../topics/i18n/timezones.txt:140
# 2c4d99f1a5fe4e45b69a9b126defb254
msgid "The **current time zone** is the time zone that's used for rendering."
msgstr ""

#: ../../topics/i18n/timezones.txt:142
# a80f0a14a32646bbabf2c2a35f1253bb
msgid "You should set the current time zone to the end user's actual time zone with :func:`~django.utils.timezone.activate`. Otherwise, the default time zone is used."
msgstr ""

#: ../../topics/i18n/timezones.txt:148
# 4178d846fa6b44d38e1590b321091ac8
msgid "As explained in the documentation of :setting:`TIME_ZONE`, Django sets environment variables so that its process runs in the default time zone. This happens regardless of the value of :setting:`USE_TZ` and of the current time zone."
msgstr ""

#: ../../topics/i18n/timezones.txt:153
# 30c3ae36baa543d9bb3502b669890ffb
msgid "When :setting:`USE_TZ` is ``True``, this is useful to preserve backwards-compatibility with applications that still rely on local time. However, :ref:`as explained above <naive-datetime-objects>`, this isn't entirely reliable, and you should always work with aware datetimes in UTC in your own code. For instance, use :meth:`~datetime.datetime.utcfromtimestamp` instead of :meth:`~datetime.datetime.fromtimestamp` -- and don't forget to set ``tzinfo`` to :data:`~django.utils.timezone.utc`."
msgstr ""

#: ../../topics/i18n/timezones.txt:163
# e2b8aea497614627b442e7ac955a536c
msgid "Selecting the current time zone"
msgstr ""

#: ../../topics/i18n/timezones.txt:165
# dfcbcae340b54e79b4fc7801e902d8bf
msgid "The current time zone is the equivalent of the current :term:`locale <locale name>` for translations. However, there's no equivalent of the ``Accept-Language`` HTTP header that Django could use to determine the user's time zone automatically. Instead, Django provides :ref:`time zone selection functions <time-zone-selection-functions>`. Use them to build the time zone selection logic that makes sense for you."
msgstr ""

#: ../../topics/i18n/timezones.txt:172
# 6280666713a04bca980c5521034960e9
msgid "Most Web sites that care about time zones just ask users in which time zone they live and store this information in the user's profile. For anonymous users, they use the time zone of their primary audience or UTC. pytz_ provides helpers_, like a list of time zones per country, that you can use to pre-select the most likely choices."
msgstr ""

#: ../../topics/i18n/timezones.txt:178
# d44c870f46674d4db6b245c3bd54cd18
msgid "Here's an example that stores the current timezone in the session. (It skips error handling entirely for the sake of simplicity.)"
msgstr ""

#: ../../topics/i18n/timezones.txt:181
# ea57925688f4479eac5422a4a624a5f3
msgid "Add the following middleware to :setting:`MIDDLEWARE_CLASSES`::"
msgstr ""

#: ../../topics/i18n/timezones.txt:195
# c0d48cb6c36e4b0da55a06aa5f1d901b
msgid "Create a view that can set the current timezone::"
msgstr ""

#: ../../topics/i18n/timezones.txt:206
# 03f389a55421477da81ae63355a1c476
msgid "Include a form in ``template.html`` that will ``POST`` to this view:"
msgstr ""

#: ../../topics/i18n/timezones.txt:225
# 135318b6780841719868bcd2a18c2bbd
msgid "Time zone aware input in forms"
msgstr ""

#: ../../topics/i18n/timezones.txt:227
# c84e692538a0422d8fcec87523e7cf30
msgid "When you enable time zone support, Django interprets datetimes entered in forms in the :ref:`current time zone <default-current-time-zone>` and returns aware datetime objects in ``cleaned_data``."
msgstr ""

#: ../../topics/i18n/timezones.txt:231
# 52a352732f0246e09af7a5a703f54891
msgid "If the current time zone raises an exception for datetimes that don't exist or are ambiguous because they fall in a DST transition (the timezones provided by pytz_ do this), such datetimes will be reported as invalid values."
msgstr ""

#: ../../topics/i18n/timezones.txt:238
# f8feeeb5393240899d2349bfbf17e2ea
msgid "Time zone aware output in templates"
msgstr ""

#: ../../topics/i18n/timezones.txt:240
# 8d4f809a5715440894f8091231062dcf
msgid "When you enable time zone support, Django converts aware datetime objects to the :ref:`current time zone <default-current-time-zone>` when they're rendered in templates. This behaves very much like :doc:`format localization </topics/i18n/formatting>`."
msgstr ""

#: ../../topics/i18n/timezones.txt:247
# e9d4288f1d5644718e7daf729f2658d0
msgid "Django doesn't convert naive datetime objects, because they could be ambiguous, and because your code should never produce naive datetimes when time zone support is enabled. However, you can force conversion with the template filters described below."
msgstr ""

#: ../../topics/i18n/timezones.txt:252
# c04e55070178492eb861b11823e488a7
msgid "Conversion to local time isn't always appropriate -- you may be generating output for computers rather than for humans. The following filters and tags, provided by the ``tz`` template tag library, allow you to control the time zone conversions."
msgstr ""

#: ../../topics/i18n/timezones.txt:258
# 3393ef7ad3ef442cb558ec7e2e468b6c
msgid "Template tags"
msgstr ""

#: ../../topics/i18n/timezones.txt:263
#: ../../topics/i18n/timezones.txt:334
# 040dac48feb84df0be209525a440e4e5
# 56de24f3485444d79384a7ae730410f8
msgid "localtime"
msgstr ""

#: ../../topics/i18n/timezones.txt:265
# cb0f012179274eb6a50f6c6d58106dea
msgid "Enables or disables conversion of aware datetime objects to the current time zone in the contained block."
msgstr ""

#: ../../topics/i18n/timezones.txt:268
# 1f28a79b58db4967b363a94e39e013c9
msgid "This tag has exactly the same effects as the :setting:`USE_TZ` setting as far as the template engine is concerned. It allows a more fine grained control of conversion."
msgstr ""

#: ../../topics/i18n/timezones.txt:272
# 4116ff4b91e34c9899e5807fa5a5728a
msgid "To activate or deactivate conversion for a template block, use::"
msgstr ""

#: ../../topics/i18n/timezones.txt:286
# 744d6806d1354d739aabee3dd0a8d2f5
msgid "The value of :setting:`USE_TZ` isn't respected inside of a ``{% localtime %}`` block."
msgstr ""

#: ../../topics/i18n/timezones.txt:292
#: ../../topics/i18n/timezones.txt:360
# 9c2bf29039ba475fa1cbeed360a1f0bc
# daf93ef2095c45cbbf3fa55bbb082ef5
msgid "timezone"
msgstr ""

#: ../../topics/i18n/timezones.txt:294
# 2973e531c3304707a39be6c2f773896a
msgid "Sets or unsets the current time zone in the contained block. When the current time zone is unset, the default time zone applies."
msgstr ""

#: ../../topics/i18n/timezones.txt:312
# ab9f91a370e740e584d2b6cebfe26d9b
msgid "get_current_timezone"
msgstr ""

#: ../../topics/i18n/timezones.txt:314
# fdaa677aa3eb4ceb83d147aa15109aaa
msgid "When the ``django.core.context_processors.tz`` context processor is enabled -- by default, it is -- each :class:`~django.template.RequestContext` contains a ``TIME_ZONE`` variable that provides the name of the current time zone."
msgstr ""

#: ../../topics/i18n/timezones.txt:319
# 7b07173c6f9d43a0a606e848fc871020
msgid "If you don't use a :class:`~django.template.RequestContext`, you can obtain this value with the ``get_current_timezone`` tag::"
msgstr ""

#: ../../topics/i18n/timezones.txt:325
# fd98b0692a23450ca52e309512d825e8
msgid "Template filters"
msgstr ""

#: ../../topics/i18n/timezones.txt:327
# 92249c70b44047088d596134d93365c1
msgid "These filters accept both aware and naive datetimes. For conversion purposes, they assume that naive datetimes are in the default time zone. They always return aware datetimes."
msgstr ""

#: ../../topics/i18n/timezones.txt:336
# a9a1c2efef354f2bbcabd085d02b7103
msgid "Forces conversion of a single value to the current time zone."
msgstr ""

#: ../../topics/i18n/timezones.txt:338
#: ../../topics/i18n/timezones.txt:351
#: ../../topics/i18n/timezones.txt:367
# 5c5cd2240ce342499c4b21c690535653
# 90dcd533dbdd42f29df015eec4adafbd
# f6e51ce2c8ed44319147fb5fe3cb531f
msgid "For example::"
msgstr ""

#: ../../topics/i18n/timezones.txt:347
# 202626ce01834e1ea11b391788420467
msgid "utc"
msgstr ""

#: ../../topics/i18n/timezones.txt:349
# cc943cb4f51749de973d945c3c909d9e
msgid "Forces conversion of a single value to UTC."
msgstr ""

#: ../../topics/i18n/timezones.txt:362
# 0e601f3a853b45388f00820b41956a9b
msgid "Forces conversion of a single value to an arbitrary timezone."
msgstr ""

#: ../../topics/i18n/timezones.txt:364
# 0423be245b934a31a25c07f535428c65
msgid "The argument must be an instance of a :class:`~datetime.tzinfo` subclass or a time zone name. If it is a time zone name, pytz_ is required."
msgstr ""

#: ../../topics/i18n/timezones.txt:376
# 5ac30a056d8d4a91a397bef0427001ae
msgid "Migration guide"
msgstr ""

#: ../../topics/i18n/timezones.txt:378
# 9f3453953e7246498ec2d606a1e8c1ae
msgid "Here's how to migrate a project that was started before Django supported time zones."
msgstr ""

#: ../../topics/i18n/timezones.txt:382
# 53899a2b8ca24f1f89e3d93bd9f3fca8
msgid "Database"
msgstr ""

#: ../../topics/i18n/timezones.txt:385
# 4559700e624c4104a610cd180513fe1c
msgid "PostgreSQL"
msgstr ""

#: ../../topics/i18n/timezones.txt:387
# 9fb6603ba01f4e0395dfa82f1b916869
msgid "The PostgreSQL backend stores datetimes as ``timestamp with time zone``. In practice, this means it converts datetimes from the connection's time zone to UTC on storage, and from UTC to the connection's time zone on retrieval."
msgstr ""

#: ../../topics/i18n/timezones.txt:391
# bed370d718664502987a636b06747e5c
msgid "As a consequence, if you're using PostgreSQL, you can switch between ``USE_TZ = False`` and ``USE_TZ = True`` freely. The database connection's time zone will be set to :setting:`TIME_ZONE` or ``UTC`` respectively, so that Django obtains correct datetimes in all cases. You don't need to perform any data conversions."
msgstr ""

#: ../../topics/i18n/timezones.txt:398
# bc7f2691383d4e5bad648338bb44afd8
msgid "Other databases"
msgstr ""

#: ../../topics/i18n/timezones.txt:400
# 6b63d59e904846d586d6c23c953f05cf
msgid "Other backends store datetimes without time zone information. If you switch from ``USE_TZ = False`` to ``USE_TZ = True``, you must convert your data from local time to UTC -- which isn't deterministic if your local time has DST."
msgstr ""

#: ../../topics/i18n/timezones.txt:405
# 4165fe26163e42b3adddbb53dbc54535
msgid "Code"
msgstr ""

#: ../../topics/i18n/timezones.txt:407
# 5589df761f234a6692d2f0b772011163
msgid "The first step is to add :setting:`USE_TZ = True <USE_TZ>` to your settings file and install pytz_ (if possible). At this point, things should mostly work. If you create naive datetime objects in your code, Django makes them aware when necessary."
msgstr ""

#: ../../topics/i18n/timezones.txt:412
# 68d28a595cd0484b989ae5c8436770ec
msgid "However, these conversions may fail around DST transitions, which means you aren't getting the full benefits of time zone support yet. Also, you're likely to run into a few problems because it's impossible to compare a naive datetime with an aware datetime. Since Django now gives you aware datetimes, you'll get exceptions wherever you compare a datetime that comes from a model or a form with a naive datetime that you've created in your code."
msgstr ""

#: ../../topics/i18n/timezones.txt:419
# ecaf9629870b4173a43c8225191a5877
msgid "So the second step is to refactor your code wherever you instantiate datetime objects to make them aware. This can be done incrementally. :mod:`django.utils.timezone` defines some handy helpers for compatibility code: :func:`~django.utils.timezone.now`, :func:`~django.utils.timezone.is_aware`, :func:`~django.utils.timezone.is_naive`, :func:`~django.utils.timezone.make_aware`, and :func:`~django.utils.timezone.make_naive`."
msgstr ""

#: ../../topics/i18n/timezones.txt:428
# f683302728e34a5cb29fabce8e0ffe07
msgid "Finally, in order to help you locate code that needs upgrading, Django raises a warning when you attempt to save a naive datetime to the database::"
msgstr ""

#: ../../topics/i18n/timezones.txt:434
# 79e5f61e26ee40669a48e1c752e3285d
msgid "During development, you can turn such warnings into exceptions and get a traceback by adding the following to your settings file::"
msgstr ""

#: ../../topics/i18n/timezones.txt:443
# 7a0bda6675f64be3b8b7cf5d337fb778
msgid "Fixtures"
msgstr ""

#: ../../topics/i18n/timezones.txt:445
# 88123b4b5b3645aba8b00ddcdc51721b
msgid "When serializing an aware datetime, the UTC offset is included, like this::"
msgstr ""

#: ../../topics/i18n/timezones.txt:449
# efbac74b69b242c0b1306029bb7ac7bf
msgid "For a naive datetime, it obviously isn't::"
msgstr ""

#: ../../topics/i18n/timezones.txt:453
# f0cc5f396ed54c1983358bde28b11eb4
msgid "For models with :class:`~django.db.models.DateTimeField`\\ s, this difference makes it impossible to write a fixture that works both with and without time zone support."
msgstr ""

#: ../../topics/i18n/timezones.txt:457
# e2fd9d732f51482a8d834bef1892714e
msgid "Fixtures generated with ``USE_TZ = False``, or before Django 1.4, use the \"naive\" format. If your project contains such fixtures, after you enable time zone support, you'll see :exc:`RuntimeWarning`\\ s when you load them. To get rid of the warnings, you must convert your fixtures to the \"aware\" format."
msgstr ""

#: ../../topics/i18n/timezones.txt:462
# 94b9619b70344dca98da674af044b42e
msgid "You can regenerate fixtures with :djadmin:`loaddata` then :djadmin:`dumpdata`. Or, if they're small enough, you can simply edit them to add the UTC offset that matches your :setting:`TIME_ZONE` to each serialized datetime."
msgstr ""

#: ../../topics/i18n/timezones.txt:469
# a15c34ad5059417e89eebf46e0addf55
msgid "FAQ"
msgstr ""

#: ../../topics/i18n/timezones.txt:472
# 3a0d3a1b2b764d8a8c7b519bcde337fd
msgid "Setup"
msgstr ""

#: ../../topics/i18n/timezones.txt:474
# 70295ec03c184fe1b6768e97f23a7647
msgid "**I don't need multiple time zones. Should I enable time zone support?**"
msgstr ""

#: ../../topics/i18n/timezones.txt:476
# e686e86d09eb40bf99e64042d779c96c
msgid "Yes. When time zone support is enabled, Django uses a more accurate model of local time. This shields you from subtle and unreproducible bugs around Daylight Saving Time (DST) transitions."
msgstr ""

#: ../../topics/i18n/timezones.txt:480
# 2490780346374b39a47affdace8f5b40
msgid "In this regard, time zones are comparable to ``unicode`` in Python. At first it's hard. You get encoding and decoding errors. Then you learn the rules. And some problems disappear -- you never get mangled output again when your application receives non-ASCII input."
msgstr ""

#: ../../topics/i18n/timezones.txt:485
# e97fbe4c71a443649ff479608c86a147
msgid "When you enable time zone support, you'll encounter some errors because you're using naive datetimes where Django expects aware datetimes. Such errors show up when running tests and they're easy to fix. You'll quickly learn how to avoid invalid operations."
msgstr ""

#: ../../topics/i18n/timezones.txt:490
# 1bb6b981c28942d2bc056433fe9d77a9
msgid "On the other hand, bugs caused by the lack of time zone support are much harder to prevent, diagnose and fix. Anything that involves scheduled tasks or datetime arithmetic is a candidate for subtle bugs that will bite you only once or twice a year."
msgstr ""

#: ../../topics/i18n/timezones.txt:495
# 0e523cde70af4c128206329aff352f97
msgid "For these reasons, time zone support is enabled by default in new projects, and you should keep it unless you have a very good reason not to."
msgstr ""

#: ../../topics/i18n/timezones.txt:498
# a4d88ee000a540d6b94e9ff945f6a0af
msgid "**I've enabled time zone support. Am I safe?**"
msgstr ""

#: ../../topics/i18n/timezones.txt:500
# 2fdc2fe1e3654304aed7eaa3ea4b51f7
msgid "Maybe. You're better protected from DST-related bugs, but you can still shoot yourself in the foot by carelessly turning naive datetimes into aware datetimes, and vice-versa."
msgstr ""

#: ../../topics/i18n/timezones.txt:504
# dcb3ccb4923549b29a7407768ecf0c8f
msgid "If your application connects to other systems -- for instance, if it queries a Web service -- make sure datetimes are properly specified. To transmit datetimes safely, their representation should include the UTC offset, or their values should be in UTC (or both!)."
msgstr ""

#: ../../topics/i18n/timezones.txt:509
# 1c76420ebca54b62a35ca42dc334fbf1
msgid "Finally, our calendar system contains interesting traps for computers::"
msgstr ""

#: ../../topics/i18n/timezones.txt:521
# 08be8faca6604450a4cf240ddc37810b
msgid "(To implement this function, you must decide whether 2012-02-29 minus one year is 2011-02-28 or 2011-03-01, which depends on your business requirements.)"
msgstr ""

#: ../../topics/i18n/timezones.txt:525
# 9325a13ca5ee44e7993da68561ba9187
msgid "**Should I install pytz?**"
msgstr ""

#: ../../topics/i18n/timezones.txt:527
# 6b7e05d88b9542908936e232a982123e
msgid "Yes. Django has a policy of not requiring external dependencies, and for this reason pytz_ is optional. However, it's much safer to install it."
msgstr ""

#: ../../topics/i18n/timezones.txt:530
# 63ab3fdeafe84a728b47fe8bcd01aa55
msgid "As soon as you activate time zone support, Django needs a definition of the default time zone. When pytz is available, Django loads this definition from the `tz database`_. This is the most accurate solution. Otherwise, it relies on the difference between local time and UTC, as reported by the operating system, to compute conversions. This is less reliable, especially around DST transitions."
msgstr ""

#: ../../topics/i18n/timezones.txt:537
# 18ad2b73702c45fab4293c4fd37c46a8
msgid "Furthermore, if you want to support users in more than one time zone, pytz is the reference for time zone definitions."
msgstr ""

#: ../../topics/i18n/timezones.txt:541
# d91a6a9bef9e43ce925cc3b6437bf473
msgid "Troubleshooting"
msgstr ""

#: ../../topics/i18n/timezones.txt:543
# 3a7d257db04a4aabbbaa98e2623d2c2e
msgid "**My application crashes with** ``TypeError: can't compare offset-naive`` ``and offset-aware datetimes`` **-- what's wrong?**"
msgstr ""

#: ../../topics/i18n/timezones.txt:546
# 5ee6c20a3a304d8fafe49f7f7630c05f
msgid "Let's reproduce this error by comparing a naive and an aware datetime::"
msgstr ""

#: ../../topics/i18n/timezones.txt:557
# 2ed9ed64759e4ae393288643b53bd86a
msgid "If you encounter this error, most likely your code is comparing these two things:"
msgstr ""

#: ../../topics/i18n/timezones.txt:560
# ed132a812fa8460f8a3318211994886c
msgid "a datetime provided by Django -- for instance, a value read from a form or a model field. Since you enabled time zone support, it's aware."
msgstr ""

#: ../../topics/i18n/timezones.txt:562
# cb120fefda0f4752b76f8989121586f4
msgid "a datetime generated by your code, which is naive (or you wouldn't be reading this)."
msgstr ""

#: ../../topics/i18n/timezones.txt:565
# 9166074974d742c786d29fb14a2a3037
msgid "Generally, the correct solution is to change your code to use an aware datetime instead."
msgstr ""

#: ../../topics/i18n/timezones.txt:568
# 583532b2434042f3becb2ccf6906a49a
msgid "If you're writing a pluggable application that's expected to work independently of the value of :setting:`USE_TZ`, you may find :func:`django.utils.timezone.now` useful. This function returns the current date and time as a naive datetime when ``USE_TZ = False`` and as an aware datetime when ``USE_TZ = True``. You can add or subtract :class:`datetime.timedelta` as needed."
msgstr ""

#: ../../topics/i18n/timezones.txt:575
# 49039331e41d4ea59c48b72bf17b5eb3
msgid "**I see lots of** ``RuntimeWarning: DateTimeField received a naive datetime`` ``(YYYY-MM-DD HH:MM:SS)`` ``while time zone support is active`` **-- is that bad?**"
msgstr ""

#: ../../topics/i18n/timezones.txt:579
# aae4afb7d5f940259c6c501449f7e324
msgid "When time zone support is enabled, the database layer expects to receive only aware datetimes from your code. This warning occurs when it receives a naive datetime. This indicates that you haven't finished porting your code for time zone support. Please refer to the :ref:`migration guide <time-zones-migration-guide>` for tips on this process."
msgstr ""

#: ../../topics/i18n/timezones.txt:585
# e6bd87ac3c6d4ce4b50cfa4383d42a32
msgid "In the meantime, for backwards compatibility, the datetime is considered to be in the default time zone, which is generally what you expect."
msgstr ""

#: ../../topics/i18n/timezones.txt:588
# d03fd1d0fc3247a4b871d19c48265144
msgid "``now.date()`` **is yesterday! (or tomorrow)**"
msgstr ""

#: ../../topics/i18n/timezones.txt:590
# a522effe23124df4aaa9b75f99d8578a
msgid "If you've always used naive datetimes, you probably believe that you can convert a datetime to a date by calling its :meth:`~datetime.datetime.date` method. You also consider that a :class:`~datetime.date` is a lot like a :class:`~datetime.datetime`, except that it's less accurate."
msgstr ""

#: ../../topics/i18n/timezones.txt:595
# a1d6224f53d145b8bf2089613e10654b
msgid "None of this is true in a time zone aware environment::"
msgstr ""

#: ../../topics/i18n/timezones.txt:613
# 69ecc405a414414ba5cc2364a1d15015
msgid "As this example shows, the same datetime has a different date, depending on the time zone in which it is represented. But the real problem is more fundamental."
msgstr ""

#: ../../topics/i18n/timezones.txt:617
# e8b2e7c12ef14d26b7d621c4352830b3
msgid "A datetime represents a **point in time**. It's absolute: it doesn't depend on anything. On the contrary, a date is a **calendaring concept**. It's a period of time whose bounds depend on the time zone in which the date is considered. As you can see, these two concepts are fundamentally different, and converting a datetime to a date isn't a deterministic operation."
msgstr ""

#: ../../topics/i18n/timezones.txt:623
# e165ddb1b0174885a90279563c350013
msgid "What does this mean in practice?"
msgstr ""

#: ../../topics/i18n/timezones.txt:625
# bae740c4ed974fbc84cb8b8a208073e2
msgid "Generally, you should avoid converting a :class:`~datetime.datetime` to :class:`~datetime.date`. For instance, you can use the :tfilter:`date` template filter to only show the date part of a datetime. This filter will convert the datetime into the current time zone before formatting it, ensuring the results appear correctly."
msgstr ""

#: ../../topics/i18n/timezones.txt:631
# 32fa2db24a7949709f0d3d63716c6b49
msgid "If you really need to do the conversion yourself, you must ensure the datetime is converted to the appropriate time zone first. Usually, this will be the current timezone::"
msgstr ""

#: ../../topics/i18n/timezones.txt:647
# dbe7a3d5ec7544308f53ce12e96149a9
msgid "**I get an error** \"``Are time zone definitions for your database and pytz installed?``\" **pytz is installed, so I guess the problem is my database?**"
msgstr ""

#: ../../topics/i18n/timezones.txt:650
# 21763c44c5b3456abefb56f28ee84934
msgid "If you are using MySQL, see the :ref:`mysql-time-zone-definitions` section of the MySQL notes for instructions on loading time zone definitions."
msgstr ""

#: ../../topics/i18n/timezones.txt:654
# c6a45a500a4744dba179b2f3537aa6cc
msgid "Usage"
msgstr ""

#: ../../topics/i18n/timezones.txt:656
# 48637b1b95af433d868f19de91a079f8
msgid "**I have a string** ``\"2012-02-21 10:28:45\"`` **and I know it's in the** ``\"Europe/Helsinki\"`` **time zone. How do I turn that into an aware datetime?**"
msgstr ""

#: ../../topics/i18n/timezones.txt:660
# 425db6ec95b24b919ade1a0399dbb132
msgid "This is exactly what pytz_ is for."
msgstr ""

#: ../../topics/i18n/timezones.txt:668
# bee3b8c2fe6f48f6a31fb5b37e249937
msgid "Note that ``localize`` is a pytz extension to the :class:`~datetime.tzinfo` API. Also, you may want to catch ``pytz.InvalidTimeError``. The documentation of pytz contains `more examples`_. You should review it before attempting to manipulate aware datetimes."
msgstr ""

#: ../../topics/i18n/timezones.txt:673
# 3edcb3ca631c4fe78a58207693f736d8
msgid "**How can I obtain the local time in the current time zone?**"
msgstr ""

#: ../../topics/i18n/timezones.txt:675
# c63e98d4d858447dbc66fba49ef03e95
msgid "Well, the first question is, do you really need to?"
msgstr ""

#: ../../topics/i18n/timezones.txt:677
# ffe2ef52650047778525d1b0fed9296a
msgid "You should only use local time when you're interacting with humans, and the template layer provides :ref:`filters and tags <time-zones-in-templates>` to convert datetimes to the time zone of your choice."
msgstr ""

#: ../../topics/i18n/timezones.txt:681
# 9439223953f74a2f913e3c3aad7af307
msgid "Furthermore, Python knows how to compare aware datetimes, taking into account UTC offsets when necessary. It's much easier (and possibly faster) to write all your model and view code in UTC. So, in most circumstances, the datetime in UTC returned by :func:`django.utils.timezone.now` will be sufficient."
msgstr ""

#: ../../topics/i18n/timezones.txt:687
# 71794587b1bc4b0294764582fc01d62a
msgid "For the sake of completeness, though, if you really want the local time in the current time zone, here's how you can obtain it::"
msgstr ""

#: ../../topics/i18n/timezones.txt:694
# a83b77a5da6541509d2b071798f0c5e4
msgid "In this example, pytz_ is installed and the current time zone is ``\"Europe/Paris\"``."
msgstr ""

#: ../../topics/i18n/timezones.txt:697
# a2442999809c401a8cd756b6d27f9a68
msgid "**How can I see all available time zones?**"
msgstr ""

#: ../../topics/i18n/timezones.txt:699
# 851f0b25a6b84d3d8b23a438d09d1b5d
msgid "pytz_ provides helpers_, including a list of current time zones and a list of all available time zones -- some of which are only of historical interest."
msgstr ""

