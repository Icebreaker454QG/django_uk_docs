# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../ref/contrib/formtools/form-preview.txt:3
# f24290895b7f41d2b56a0a2500f7e80b
msgid "Form preview"
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:9
# ce462a8504e242b9b0579cb888352687
msgid "Django comes with an optional \"form preview\" application that helps automate the following workflow:"
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:12
# c1417684b8ff4c7d91b5c59815d3066b
msgid "\"Display an HTML form, force a preview, then do something with the submission.\""
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:14
# f8b3835c66074a90a95bec12d7d695e9
msgid "To force a preview of a form submission, all you have to do is write a short Python class."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:18
# 382d6ac1be004f90a7f2cdb88e80a406
msgid "Overview"
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:20
# 7a5b3e8dd02e4eaf818e9350c6f22252
msgid "Given a :class:`django.forms.Form` subclass that you define, this application takes care of the following workflow:"
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:23
# d94c924a38c64749815a0681671699dd
msgid "Displays the form as HTML on a Web page."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:24
# 1a92cc7d225947618ca9227f63f88932
msgid "Validates the form data when it's submitted via POST. a. If it's valid, displays a preview page. b. If it's not valid, redisplays the form with error messages."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:27
# ea13a7399b48485c929b945754d47c20
msgid "When the \"confirmation\" form is submitted from the preview page, calls a hook that you define -- a ``done()`` method that gets passed the valid data."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:31
# 16e3cbd7b21349f89c9578d98f0017f1
msgid "The framework enforces the required preview by passing a shared-secret hash to the preview page via hidden form fields. If somebody tweaks the form parameters on the preview page, the form submission will fail the hash-comparison test."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:36
# b8f5d828c0874a8db82081a6df9522a2
msgid "How to use ``FormPreview``"
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:38
# 202317cccfb7441893931ad3210e0ed4
msgid "Point Django at the default FormPreview templates. There are two ways to do this:"
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:41
# e07418a439684c7eb8c5e3a6762eb04a
msgid "Add ``'django.contrib.formtools'`` to your :setting:`INSTALLED_APPS` setting. This will work if your :setting:`TEMPLATE_LOADERS` setting includes the ``app_directories`` template loader (which is the case by default). See the :ref:`template loader docs <template-loaders>` for more."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:48
# 992c242cdb8e4076bbcfb4b5d3618c26
msgid "Otherwise, determine the full filesystem path to the :file:`django/contrib/formtools/templates` directory, and add that directory to your :setting:`TEMPLATE_DIRS` setting."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:52
# 80e566349b0242b39dbcc8d659d56675
msgid "Create a :class:`~django.contrib.formtools.preview.FormPreview` subclass that overrides the ``done()`` method::"
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:66
# 4dcc3e6f834d41468cc37e488a3a4f58
msgid "This method takes an :class:`~django.http.HttpRequest` object and a dictionary of the form data after it has been validated and cleaned. It should return an :class:`~django.http.HttpResponseRedirect` that is the end result of the form being submitted."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:71
# eb7ad20becbc49cfaf6849d3a37a639e
msgid "Change your URLconf to point to an instance of your :class:`~django.contrib.formtools.preview.FormPreview` subclass::"
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:78
# a31594c5ed424fa9b00dc8d9802f83ef
msgid "...and add the following line to the appropriate model in your URLconf::"
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:82
# 7443859c070548898fa713a582d44409
msgid "where ``SomeModelForm`` is a Form or ModelForm class for the model."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:84
# 0438c9bcf42c44b6b2091ec72052d66e
msgid "Run the Django server and visit :file:`/post/` in your browser."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:87
# bb16a703309d4302929f223b94bb5592
msgid "``FormPreview`` classes"
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:91
# abef59a7e19e4334ac9db680626f7617
msgid "A :class:`~django.contrib.formtools.preview.FormPreview` class is a simple Python class that represents the preview workflow. :class:`~django.contrib.formtools.preview.FormPreview` classes must subclass ``django.contrib.formtools.preview.FormPreview`` and override the ``done()`` method. They can live anywhere in your codebase."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:98
# 8f0628006e9440988f6906c7e5ad4d6b
msgid "``FormPreview`` templates"
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:103
# a12ca95f6270433ca2a8adb15649a8be
msgid "By default, the form is rendered via the template :file:`formtools/form.html`, and the preview page is rendered via the template :file:`formtools/preview.html`. These values can be overridden for a particular form preview by setting :attr:`~django.contrib.formtools.preview.FormPreview.preview_template` and :attr:`~django.contrib.formtools.preview.FormPreview.form_template` attributes on the FormPreview subclass. See :file:`django/contrib/formtools/templates` for the default templates."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:112
# 25fd58099fef4cd797f71d7291940735
msgid "Advanced ``FormPreview`` methods"
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:116
# 95c73ec72d1d4ad8a9159b753b240883
msgid "Given a validated form, performs any extra processing before displaying the preview page, and saves any extra data in context."
msgstr ""

#: ../../ref/contrib/formtools/form-preview.txt:119
# 12e4e3b593554971b9d341fa00c2fb10
msgid "By default, this method is empty.  It is called after the form is validated, but before the context is modified with hash information and rendered."
msgstr ""

