# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.8\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-08 21:09+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../ref/models/class.txt:3
msgid "Model class reference"
msgstr ""

#: ../../ref/models/class.txt:7
msgid ""
"This document covers features of the :class:`~django.db.models.Model` class."
" For more information about models, see :doc:`the complete list of Model "
"reference guides </ref/models/index>`."
msgstr ""

#: ../../ref/models/class.txt:12
msgid "Attributes"
msgstr ""

#: ../../ref/models/class.txt:15
msgid "``objects``"
msgstr ""

#: ../../ref/models/class.txt:19
msgid ""
"Each non-abstract :class:`~django.db.models.Model` class must have a "
":class:`~django.db.models.Manager` instance added to it. Django ensures that"
" in your model class you have  at least a default ``Manager`` specified. If "
"you don't add your own ``Manager``, Django will add an attribute ``objects``"
" containing default :class:`~django.db.models.Manager` instance. If you add "
"your own :class:`~django.db.models.Manager` instance attribute, the default "
"one does not appear. Consider the following example::"
msgstr ""

#: ../../ref/models/class.txt:34
msgid ""
"For more details on model managers see :doc:`Managers </topics/db/managers>`"
" and :ref:`Retrieving objects <retrieving-objects>`."
msgstr ""
