# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../ref/models/instances.txt:3
# 4942f56a63bc4b21b2b080407c6416e4
msgid "Model instance reference"
msgstr ""

#: ../../ref/models/instances.txt:7
# 38b4a62339124e079c871fb817f74e56
msgid "This document describes the details of the ``Model`` API. It builds on the material presented in the :doc:`model </topics/db/models>` and :doc:`database query </topics/db/queries>` guides, so you'll probably want to read and understand those documents before reading this one."
msgstr ""

#: ../../ref/models/instances.txt:12
# de0f1ae539bb4e37a3ac1a3138293e51
msgid "Throughout this reference we'll use the :ref:`example Weblog models <queryset-model-example>` presented in the :doc:`database query guide </topics/db/queries>`."
msgstr ""

#: ../../ref/models/instances.txt:17
# 8e6b04d6a58d416e888ce4408359170e
msgid "Creating objects"
msgstr ""

#: ../../ref/models/instances.txt:19
# b55208e2bd1742299e520e991226c836
msgid "To create a new instance of a model, just instantiate it like any other Python class:"
msgstr ""

#: ../../ref/models/instances.txt:24
# a72b88d83edd49bfa9b122dd3fa6026a
msgid "The keyword arguments are simply the names of the fields you've defined on your model. Note that instantiating a model in no way touches your database; for that, you need to :meth:`~Model.save()`."
msgstr ""

#: ../../ref/models/instances.txt:30
# 265b8d2c555b4dc8ac750d38834d8c41
msgid "You may be tempted to customize the model by overriding the ``__init__`` method. If you do so, however, take care not to change the calling signature as any change may prevent the model instance from being saved. Rather than overriding ``__init__``, try using one of these approaches:"
msgstr ""

#: ../../ref/models/instances.txt:35
# 46326f11176643e0bcc0c95fdd61b7ab
msgid "Add a classmethod on the model class::"
msgstr ""

#: ../../ref/models/instances.txt:50
# da9ca7ded775480ebe1f7236a53a0b69
msgid "Add a method on a custom manager (usually preferred)::"
msgstr ""

#: ../../ref/models/instances.txt:68
# 491aadd7f7a64cb49ff7b3d57308bb4c
msgid "Validating objects"
msgstr ""

#: ../../ref/models/instances.txt:70
# 8bd8b31f5b8940248678222f52edb5f5
msgid "There are three steps involved in validating a model:"
msgstr ""

#: ../../ref/models/instances.txt:72
# ff3223d966ad449b89b94de1b47cf52e
msgid "Validate the model fields - :meth:`Model.clean_fields()`"
msgstr ""

#: ../../ref/models/instances.txt:73
# 6c02bc4f6bdd47ccb1547197e4d96739
msgid "Validate the model as a whole - :meth:`Model.clean()`"
msgstr ""

#: ../../ref/models/instances.txt:74
# 8e2c3fc48ab1490c8a91d5414f62d955
msgid "Validate the field uniqueness - :meth:`Model.validate_unique()`"
msgstr ""

#: ../../ref/models/instances.txt:76
# 6429a287afb846cda9ac899ba277a5fc
msgid "All three steps are performed when you call a model's :meth:`~Model.full_clean()` method."
msgstr ""

#: ../../ref/models/instances.txt:79
# 5b6bb98510594a098b2b96d31f84dc92
msgid "When you use a :class:`~django.forms.ModelForm`, the call to :meth:`~django.forms.Form.is_valid()` will perform these validation steps for all the fields that are included on the form. See the :doc:`ModelForm documentation </topics/forms/modelforms>` for more information. You should only need to call a model's :meth:`~Model.full_clean()` method if you plan to handle validation errors yourself, or if you have excluded fields from the :class:`~django.forms.ModelForm` that require validation."
msgstr ""

#: ../../ref/models/instances.txt:91
# 0856fc54ed3c42b8adcfe732c3b8852e
msgid "The ``validate_unique`` parameter was added to allow skipping :meth:`Model.validate_unique()`. Previously, :meth:`Model.validate_unique()` was always called by ``full_clean``."
msgstr ""

#: ../../ref/models/instances.txt:95
# 06e1d6b3f8f042589a602b34a914fbc4
msgid "This method calls :meth:`Model.clean_fields()`, :meth:`Model.clean()`, and :meth:`Model.validate_unique()` (if ``validate_unique`` is ``True``, in that order and raises a :exc:`~django.core.exceptions.ValidationError` that has a ``message_dict`` attribute containing errors from all three stages."
msgstr ""

#: ../../ref/models/instances.txt:100
# ca050d3425ce479b86458b804e333369
msgid "The optional ``exclude`` argument can be used to provide a list of field names that can be excluded from validation and cleaning. :class:`~django.forms.ModelForm` uses this argument to exclude fields that aren't present on your form from being validated since any errors raised could not be corrected by the user."
msgstr ""

#: ../../ref/models/instances.txt:106
# ca2e7f25619c46e288adff47c37a5697
msgid "Note that ``full_clean()`` will *not* be called automatically when you call your model's :meth:`~Model.save()` method. You'll need to call it manually when you want to run one-step model validation for your own manually created models. For example::"
msgstr ""

#: ../../ref/models/instances.txt:119
# 54e4a5891ba6432791ae8895cd2e4981
msgid "The first step ``full_clean()`` performs is to clean each individual field."
msgstr ""

#: ../../ref/models/instances.txt:123
# 23bd0e513b1144428a9e26d0fa92457c
msgid "This method will validate all fields on your model. The optional ``exclude`` argument lets you provide a list of field names to exclude from validation. It will raise a :exc:`~django.core.exceptions.ValidationError` if any fields fail validation."
msgstr ""

#: ../../ref/models/instances.txt:128
# 8acf53562a704edeaf6ac5c7a2d172cf
msgid "The second step ``full_clean()`` performs is to call :meth:`Model.clean()`. This method should be overridden to perform custom validation on your model."
msgstr ""

#: ../../ref/models/instances.txt:133
# de02b3ddba6149468ececb4fe72b9114
msgid "This method should be used to provide custom model validation, and to modify attributes on your model if desired. For instance, you could use it to automatically provide a value for a field, or to do validation that requires access to more than a single field::"
msgstr ""

#: ../../ref/models/instances.txt:152
# fb656c6587444836a9191d3c16516704
msgid "Note, however, that like :meth:`Model.full_clean()`, a model's ``clean()`` method is not invoked when you call your model's :meth:`~Model.save()` method."
msgstr ""

#: ../../ref/models/instances.txt:155
# 70fa95969bed4412a07e50d63248cac6
msgid "Any :exc:`~django.core.exceptions.ValidationError` exceptions raised by ``Model.clean()`` will be stored in a special key error dictionary key, :data:`~django.core.exceptions.NON_FIELD_ERRORS`, that is used for errors that are tied to the entire model instead of to a specific field::"
msgstr ""

#: ../../ref/models/instances.txt:166
# 3747e5ae966e48259de363d0ae50eaa1
msgid "Finally, ``full_clean()`` will check any unique constraints on your model."
msgstr ""

#: ../../ref/models/instances.txt:170
# e8d1e64d419a435eb29c015855c65516
msgid "This method is similar to :meth:`~Model.clean_fields`, but validates all uniqueness constraints on your model instead of individual field values. The optional ``exclude`` argument allows you to provide a list of field names to exclude from validation. It will raise a :exc:`~django.core.exceptions.ValidationError` if any fields fail validation."
msgstr ""

#: ../../ref/models/instances.txt:176
# 7c2e0cc1342f4f48854657fd4d9929ab
msgid "Note that if you provide an ``exclude`` argument to ``validate_unique()``, any :attr:`~django.db.models.Options.unique_together` constraint involving one of the fields you provided will not be checked."
msgstr ""

#: ../../ref/models/instances.txt:182
# 6712d5d9ebdb48d4b2509c22012828b4
msgid "Saving objects"
msgstr ""

#: ../../ref/models/instances.txt:184
# ae5a6f6606f14c51ac3a812aa293ac49
msgid "To save an object back to the database, call ``save()``:"
msgstr ""

#: ../../ref/models/instances.txt:188
# 865edc9515c3464a8ddc708843def7f7
msgid "If you want customized saving behavior, you can override this ``save()`` method. See :ref:`overriding-model-methods` for more details."
msgstr ""

#: ../../ref/models/instances.txt:191
# 70d0b9c1b1374cda8b11750f04aa9758
msgid "The model save process also has some subtleties; see the sections below."
msgstr ""

#: ../../ref/models/instances.txt:194
# 77f4779119ac489182584d87f6790d09
msgid "Auto-incrementing primary keys"
msgstr ""

#: ../../ref/models/instances.txt:196
# b8aaf27435e94e4c843333661270e5ab
msgid "If a model has an :class:`~django.db.models.AutoField` — an auto-incrementing primary key — then that auto-incremented value will be calculated and saved as an attribute on your object the first time you call ``save()``::"
msgstr ""

#: ../../ref/models/instances.txt:205
# b08dca581fb446008a6cab0a24224100
msgid "There's no way to tell what the value of an ID will be before you call ``save()``, because that value is calculated by your database, not by Django."
msgstr ""

#: ../../ref/models/instances.txt:208
# f92a2c589fe043c9a774b200c707b007
msgid "For convenience, each model has an :class:`~django.db.models.AutoField` named ``id`` by default unless you explicitly specify ``primary_key=True`` on a field in your model. See the documentation for :class:`~django.db.models.AutoField` for more details."
msgstr ""

#: ../../ref/models/instances.txt:214
# a2a8a4623c454ae691f3c0ede9936b83
msgid "The ``pk`` property"
msgstr ""

#: ../../ref/models/instances.txt:218
# 3bb8c1615f644e788bf7d646a019e1c6
msgid "Regardless of whether you define a primary key field yourself, or let Django supply one for you, each model will have a property called ``pk``. It behaves like a normal attribute on the model, but is actually an alias for whichever attribute is the primary key field for the model. You can read and set this value, just as you would for any other attribute, and it will update the correct field in the model."
msgstr ""

#: ../../ref/models/instances.txt:226
# 96634ff26a9e43ef8bbf35f3a99a2821
msgid "Explicitly specifying auto-primary-key values"
msgstr ""

#: ../../ref/models/instances.txt:228
# 1ba9257769564ce9ab9cded4ad1a66e0
msgid "If a model has an :class:`~django.db.models.AutoField` but you want to define a new object's ID explicitly when saving, just define it explicitly before saving, rather than relying on the auto-assignment of the ID::"
msgstr ""

#: ../../ref/models/instances.txt:237
# bc58f51d66ec4a2fa675846e9037f902
msgid "If you assign auto-primary-key values manually, make sure not to use an already-existing primary-key value! If you create a new object with an explicit primary-key value that already exists in the database, Django will assume you're changing the existing record rather than creating a new one."
msgstr ""

#: ../../ref/models/instances.txt:242
# fe10b562e2524944a24320dec4ae7c10
msgid "Given the above ``'Cheddar Talk'`` blog example, this example would override the previous record in the database::"
msgstr ""

#: ../../ref/models/instances.txt:248
# 9c6054dd4e6d4d7ca4d2511ec496a591
msgid "See `How Django knows to UPDATE vs. INSERT`_, below, for the reason this happens."
msgstr ""

#: ../../ref/models/instances.txt:251
# b45261f54f954c1fba30cb7e375482b4
msgid "Explicitly specifying auto-primary-key values is mostly useful for bulk-saving objects, when you're confident you won't have primary-key collision."
msgstr ""

#: ../../ref/models/instances.txt:255
# c28d4f4554ba4ff4abc81242f024832f
msgid "What happens when you save?"
msgstr ""

#: ../../ref/models/instances.txt:257
# 748fc08bb9dc49cdbe35f90c22c33f67
msgid "When you save an object, Django performs the following steps:"
msgstr ""

#: ../../ref/models/instances.txt:259
# 5b7e3d8369ab437a9134e448a00d6a8a
msgid "**Emit a pre-save signal.** The :doc:`signal </ref/signals>` :attr:`django.db.models.signals.pre_save` is sent, allowing any functions listening for that signal to take some customized action."
msgstr ""

#: ../../ref/models/instances.txt:264
# 0a21e9609ba34b5c8423197cfee4cdf9
msgid "**Pre-process the data.** Each field on the object is asked to perform any automated data modification that the field may need to perform."
msgstr ""

#: ../../ref/models/instances.txt:268
# b8a99b053e6144dabe7e770ea9d86feb
msgid "Most fields do *no* pre-processing — the field data is kept as-is. Pre-processing is only used on fields that have special behavior.  For example, if your model has a :class:`~django.db.models.DateField` with ``auto_now=True``, the pre-save phase will alter the data in the object to ensure that the date field contains the current date stamp. (Our documentation doesn't yet include a list of all the fields with this \"special behavior.\")"
msgstr ""

#: ../../ref/models/instances.txt:276
# 7ee8c16eb67a41ee997ab9583c372cc5
msgid "**Prepare the data for the database.** Each field is asked to provide its current value in a data type that can be written to the database."
msgstr ""

#: ../../ref/models/instances.txt:279
# d7268e5ffd714f4b8e93e1134849f174
msgid "Most fields require *no* data preparation. Simple data types, such as integers and strings, are 'ready to write' as a Python object. However, more complex data types often require some modification."
msgstr ""

#: ../../ref/models/instances.txt:283
# cd868b2fe9e64a7ea5cf9920508f37d0
msgid "For example, :class:`~django.db.models.DateField` fields use a Python ``datetime`` object to store data. Databases don't store ``datetime`` objects, so the field value must be converted into an ISO-compliant date string for insertion into the database."
msgstr ""

#: ../../ref/models/instances.txt:288
# 77f16116544446dfa03e10841e3d31b4
msgid "**Insert the data into the database.** The pre-processed, prepared data is then composed into an SQL statement for insertion into the database."
msgstr ""

#: ../../ref/models/instances.txt:292
# 8e1acb0249c440c19877751bb706be49
msgid "**Emit a post-save signal.** The signal :attr:`django.db.models.signals.post_save` is sent, allowing any functions listening for that signal to take some customized action."
msgstr ""

#: ../../ref/models/instances.txt:298
# 272619b1fcb549738357bf7ae17bc0e7
msgid "How Django knows to UPDATE vs. INSERT"
msgstr ""

#: ../../ref/models/instances.txt:300
# 82b21a609e354fec9327d8b5624d9587
msgid "You may have noticed Django database objects use the same ``save()`` method for creating and changing objects. Django abstracts the need to use ``INSERT`` or ``UPDATE`` SQL statements. Specifically, when you call ``save()``, Django follows this algorithm:"
msgstr ""

#: ../../ref/models/instances.txt:305
# c119bff3cbe744ddb3fc6c8fcf3a197d
msgid "If the object's primary key attribute is set to a value that evaluates to ``True`` (i.e., a value other than ``None`` or the empty string), Django executes an ``UPDATE``."
msgstr ""

#: ../../ref/models/instances.txt:308
# 3046382d79db45819eb9f649b89a8150
msgid "If the object's primary key attribute is *not* set or if the ``UPDATE`` didn't update anything, Django executes an ``INSERT``."
msgstr ""

#: ../../ref/models/instances.txt:311
# 578a920c290b4d31be499db172a7a27a
msgid "The one gotcha here is that you should be careful not to specify a primary-key value explicitly when saving new objects, if you cannot guarantee the primary-key value is unused. For more on this nuance, see `Explicitly specifying auto-primary-key values`_ above and `Forcing an INSERT or UPDATE`_ below."
msgstr ""

#: ../../ref/models/instances.txt:318
# 00b91e39502e4a4e84373b173535a6ee
msgid "Previously Django did a ``SELECT`` when the primary key attribute was set. If the ``SELECT`` found a row, then Django did an ``UPDATE``, otherwise it did an ``INSERT``. The old algorithm results in one more query in the ``UPDATE`` case. There are some rare cases where the database doesn't report that a row was updated even if the database contains a row for the object's primary key value. An example is the PostgreSQL ``ON UPDATE`` trigger which returns ``NULL``. In such cases it is possible to revert to the old algorithm by setting the :attr:`~django.db.models.Options.select_on_save` option to ``True``."
msgstr ""

#: ../../ref/models/instances.txt:331
# 57accab22a3f43e287e3a03e93d2671f
msgid "Forcing an INSERT or UPDATE"
msgstr ""

#: ../../ref/models/instances.txt:333
# b7ec9c1be3dd4b46a27fcb404b6e556d
msgid "In some rare circumstances, it's necessary to be able to force the :meth:`~Model.save()` method to perform an SQL ``INSERT`` and not fall back to doing an ``UPDATE``. Or vice-versa: update, if possible, but not insert a new row. In these cases you can pass the ``force_insert=True`` or ``force_update=True`` parameters to the :meth:`~Model.save()` method. Obviously, passing both parameters is an error: you cannot both insert *and* update at the same time!"
msgstr ""

#: ../../ref/models/instances.txt:341
# 822a7e819c784d31b0e3a89ae4009cdb
msgid "It should be very rare that you'll need to use these parameters. Django will almost always do the right thing and trying to override that will lead to errors that are difficult to track down. This feature is for advanced use only."
msgstr ""

#: ../../ref/models/instances.txt:346
# fff60d8a0f9047b2a41ef7d3910cb27a
msgid "Using ``update_fields`` will force an update similarly to ``force_update``."
msgstr ""

#: ../../ref/models/instances.txt:351
# 69b4f05f876245449cbe8640294d7c89
msgid "Updating attributes based on existing fields"
msgstr ""

#: ../../ref/models/instances.txt:353
# b60e0ab0b1c64f4698a0b7455199836d
msgid "Sometimes you'll need to perform a simple arithmetic task on a field, such as incrementing or decrementing the current value. The obvious way to achieve this is to do something like::"
msgstr ""

#: ../../ref/models/instances.txt:361
# 5ea7f76d623f405497b57cf78bd0bdb0
msgid "If the old ``number_sold`` value retrieved from the database was 10, then the value of 11 will be written back to the database."
msgstr ""

#: ../../ref/models/instances.txt:364
# 2152a731032743baae97d729f02412b1
msgid "The process can be made robust, :ref:`avoiding a race condition <avoiding-race-conditions-using-f>`, as well as slightly faster by expressing the update relative to the original field value, rather than as an explicit assignment of a new value. Django provides :class:`F expressions <django.db.models.F>` for performing this kind of relative update. Using :class:`F expressions <django.db.models.F>`, the previous example is expressed as::"
msgstr ""

#: ../../ref/models/instances.txt:377
# a1b78983a74e40a6b84aa28c14aa1c8a
msgid "For more details, see the documentation on :class:`F expressions <django.db.models.F>` and their :ref:`use in update queries <topics-db-queries-update>`."
msgstr ""

#: ../../ref/models/instances.txt:382
# cd3354f210c1443f9f756a75bb734e8a
msgid "Specifying which fields to save"
msgstr ""

#: ../../ref/models/instances.txt:384
# 09bda0783ef04e659a8c4def0381258b
msgid "If ``save()`` is passed a list of field names in keyword argument ``update_fields``, only the fields named in that list will be updated. This may be desirable if you want to update just one or a few fields on an object. There will be a slight performance benefit from preventing all of the model fields from being updated in the database. For example::"
msgstr ""

#: ../../ref/models/instances.txt:393
# 961483f1567c4a549cebbdb619a70b41
msgid "The ``update_fields`` argument can be any iterable containing strings. An empty ``update_fields`` iterable will skip the save. A value of None will perform an update on all fields."
msgstr ""

#: ../../ref/models/instances.txt:397
# 37dd2b33ce584e77a38bf4443f4b9f99
msgid "Specifying ``update_fields`` will force an update."
msgstr ""

#: ../../ref/models/instances.txt:399
# ba2367cc757641dc8e5221ea48a4d206
msgid "When saving a model fetched through deferred model loading (:meth:`~django.db.models.query.QuerySet.only()` or :meth:`~django.db.models.query.QuerySet.defer()`) only the fields loaded from the DB will get updated. In effect there is an automatic ``update_fields`` in this case. If you assign or change any deferred field value, the field will be added to the updated fields."
msgstr ""

#: ../../ref/models/instances.txt:407
# 1e175cfae53e4f00970e98bfcd580d03
msgid "Deleting objects"
msgstr ""

#: ../../ref/models/instances.txt:411
# f82e70eaa7664fd5b7f993ca13ad92ec
msgid "Issues an SQL ``DELETE`` for the object. This only deletes the object in the database; the Python instance will still exist and will still have data in its fields."
msgstr ""

#: ../../ref/models/instances.txt:415
# 27259219db794203936b47bccf42f903
msgid "For more details, including how to delete objects in bulk, see :ref:`topics-db-queries-delete`."
msgstr ""

#: ../../ref/models/instances.txt:418
# 5a09bb26e6154eb3a5ffa010eb1e5988
msgid "If you want customized deletion behavior, you can override the ``delete()`` method. See :ref:`overriding-model-methods` for more details."
msgstr ""

#: ../../ref/models/instances.txt:424
# 0eb0a69eb7454c9196344ca79b301fde
msgid "Other model instance methods"
msgstr ""

#: ../../ref/models/instances.txt:426
# d442d4e12a67427485bd88ad987c3beb
msgid "A few object methods have special purposes."
msgstr ""

#: ../../ref/models/instances.txt:429
# 901aa3e3c72f42cdb0c78c6919e50bda
msgid "On Python 3, as all strings are natively considered Unicode, only use the ``__str__()`` method (the ``__unicode__()`` method is obsolete). If you'd like compatibility with Python 2, you can decorate your model class with :func:`~django.utils.encoding.python_2_unicode_compatible`."
msgstr ""

#: ../../ref/models/instances.txt:435
# 1f544db80fb24528907fab8b2354bd29
msgid "``__unicode__``"
msgstr ""

#: ../../ref/models/instances.txt:439
# ead5eae37de542299f1acab3b36de8eb
msgid "The ``__unicode__()`` method is called whenever you call ``unicode()`` on an object. Django uses ``unicode(obj)`` (or the related function, :meth:`str(obj) <Model.__str__>`) in a number of places. Most notably, to display an object in the Django admin site and as the value inserted into a template when it displays an object. Thus, you should always return a nice, human-readable representation of the model from the ``__unicode__()`` method."
msgstr ""

#: ../../ref/models/instances.txt:446
#: ../../ref/models/instances.txt:476
#: ../../ref/models/instances.txt:518
#: ../../ref/models/instances.txt:570
#: ../../ref/models/instances.txt:579
#: ../../ref/models/instances.txt:724
# f9b9d3cd187c4660b85bfff8d34e688c
# b1f5d01491a749d5a43939dcedf1cf7c
# 82ed0f2e814243ebb95dc44b410a162b
# 3130693202ed4c08944f1741e9d8ddfd
# 1e614b06a219451ba1eec9cb86eebce4
# eca36fd1a6c04c68b7a834d6a1aab0b1
msgid "For example::"
msgstr ""

#: ../../ref/models/instances.txt:457
# a435aee85a124f0da8ee56c86b7a35ef
msgid "If you define a ``__unicode__()`` method on your model and not a :meth:`~Model.__str__()` method, Django will automatically provide you with a :meth:`~Model.__str__()` that calls ``__unicode__()`` and then converts the result correctly to a UTF-8 encoded string object. This is recommended development practice: define only ``__unicode__()`` and let Django take care of the conversion to string objects when required."
msgstr ""

#: ../../ref/models/instances.txt:465
# 6e94e243044847758234aa7a2085c957
msgid "``__str__``"
msgstr ""

#: ../../ref/models/instances.txt:469
# dc999dd456ff43beb061c663df26db82
msgid "The ``__str__()`` method is called whenever you call ``str()`` on an object. In Python 3, Django uses ``str(obj)`` in a number of places. Most notably, to display an object in the Django admin site and as the value inserted into a template when it displays an object. Thus, you should always return a nice, human-readable representation of the model from the ``__str__()`` method."
msgstr ""

#: ../../ref/models/instances.txt:487
# 49e8b18edac34637b625d535cf4b2001
msgid "In Python 2, the main use of ``__str__`` directly inside Django is when the ``repr()`` output of a model is displayed anywhere (for example, in debugging output). It isn't required to put ``__str__()`` methods everywhere if you have sensible :meth:`~Model.__unicode__()` methods."
msgstr ""

#: ../../ref/models/instances.txt:493
# 2c34e7f1e2404821bf0455b9450bd968
msgid "The previous :meth:`~Model.__unicode__()` example could be similarly written using ``__str__()`` like this::"
msgstr ""

#: ../../ref/models/instances.txt:509
# 009def79cea04e689f8b1ce979df94d2
msgid "``__eq__``"
msgstr ""

#: ../../ref/models/instances.txt:513
# b541956e7aaf4626ae14ea0c43219cd0
msgid "The equality method is defined such that instances with the same primary key value and the same concrete class are considered equal. For proxy models, concrete class is defined as the model's first non-proxy parent; for all other models it is simply the model's class."
msgstr ""

#: ../../ref/models/instances.txt:539
# 293d91c6fa5540c7a34691f9782bf466
msgid "In previous versions only instances of the exact same class and same primary key value were considered equal."
msgstr ""

#: ../../ref/models/instances.txt:543
# 3b18d1bba3c947e4bc485391f8525a33
msgid "``__hash__``"
msgstr ""

#: ../../ref/models/instances.txt:547
# d3f12d84a6924358b58593314f0f1a82
msgid "The ``__hash__`` method is based on the instance's primary key value. It is effectively hash(obj.pk). If the instance doesn't have a primary key value then a ``TypeError`` will be raised (otherwise the ``__hash__`` method would return different values before and after the instance is saved, but changing the ``__hash__`` value of an instance `is forbidden in Python`_)."
msgstr ""

#: ../../ref/models/instances.txt:556
# c0fe8a92db574686b8234ff860b1d4f2
msgid "In previous versions instance's without primary key value were hashable."
msgstr ""

#: ../../ref/models/instances.txt:562
# 6e1aa9f046754f9596c93502184fc917
msgid "``get_absolute_url``"
msgstr ""

#: ../../ref/models/instances.txt:566
# 364df38ad774486aab3fd814cf0e7e63
msgid "Define a ``get_absolute_url()`` method to tell Django how to calculate the canonical URL for an object. To callers, this method should appear to return a string that can be used to refer to the object over HTTP."
msgstr ""

#: ../../ref/models/instances.txt:575
# d032b3694571473291209e156e9e8243
msgid "(Whilst this code is correct and simple, it may not be the most portable way to write this kind of method. The :func:`~django.core.urlresolvers.reverse` function is usually the best approach.)"
msgstr ""

#: ../../ref/models/instances.txt:585
# d42b22bbedfa42c1900dbc5ed0dffb40
msgid "One place Django uses ``get_absolute_url()`` is in the admin app. If an object defines this method, the object-editing page will have a \"View on site\" link that will jump you directly to the object's public view, as given by ``get_absolute_url()``."
msgstr ""

#: ../../ref/models/instances.txt:590
# 0dc94bd7b2da4a35a6f5e6409bd86cff
msgid "Similarly, a couple of other bits of Django, such as the :doc:`syndication feed framework </ref/contrib/syndication>`, use ``get_absolute_url()`` when it is defined. If it makes sense for your model's instances to each have a unique URL, you should define ``get_absolute_url()``."
msgstr ""

#: ../../ref/models/instances.txt:595
# 57e8762d948846e0b595d6c706297a3b
msgid "It's good practice to use ``get_absolute_url()`` in templates, instead of hard-coding your objects' URLs. For example, this template code is bad:"
msgstr ""

#: ../../ref/models/instances.txt:603
# 3d467f460b77470bb3ff05e71a281c70
msgid "This template code is much better:"
msgstr ""

#: ../../ref/models/instances.txt:609
# 48f732a374b8447197e4f1254699ec3d
msgid "The logic here is that if you change the URL structure of your objects, even for something simple such as correcting a spelling error, you don't want to have to track down every place that the URL might be created. Specify it once, in ``get_absolute_url()`` and have all your other code call that one place."
msgstr ""

#: ../../ref/models/instances.txt:615
# ccb42675a32947de85e3625921145022
msgid "The string you return from ``get_absolute_url()`` **must** contain only ASCII characters (required by the URI specification, :rfc:`2396`) and be URL-encoded, if necessary."
msgstr ""

#: ../../ref/models/instances.txt:619
# fee8896428e0421499f7118412523cd9
msgid "Code and templates calling ``get_absolute_url()`` should be able to use the result directly without any further processing. You may wish to use the ``django.utils.encoding.iri_to_uri()`` function to help with this if you are using unicode strings containing characters outside the ASCII range at all."
msgstr ""

#: ../../ref/models/instances.txt:626
# 2f79f173efeb484898cc29db217ae2ec
msgid "The ``permalink`` decorator"
msgstr ""

#: ../../ref/models/instances.txt:630
# 80db0ed7c86349b9a6b4fdc92e030ce4
msgid "The ``permalink`` decorator is no longer recommended. You should use :func:`~django.core.urlresolvers.reverse` in the body of your ``get_absolute_url`` method instead."
msgstr ""

#: ../../ref/models/instances.txt:634
# 13963cf590354146871248016f5dc513
msgid "In early versions of Django, there wasn't an easy way to use URLs defined in URLconf file inside :meth:`~django.db.models.Model.get_absolute_url`. That meant you would need to define the URL both in URLConf and :meth:`~django.db.models.Model.get_absolute_url`. The ``permalink`` decorator was added to overcome this DRY principle violation. However, since the introduction of :func:`~django.core.urlresolvers.reverse` there is no reason to use ``permalink`` any more."
msgstr ""

#: ../../ref/models/instances.txt:644
# a0f705fb371248279b92b39ae766bba6
msgid "This decorator takes the name of a URL pattern (either a view name or a URL pattern name) and a list of position or keyword arguments and uses the URLconf patterns to construct the correct, full URL. It returns a string for the correct URL, with all parameters substituted in the correct positions."
msgstr ""

#: ../../ref/models/instances.txt:649
# 264b322dac064808ab83b8d62acd34ee
msgid "The ``permalink`` decorator is a Python-level equivalent to the :ttag:`url` template tag and a high-level wrapper for the :func:`~django.core.urlresolvers.reverse` function."
msgstr ""

#: ../../ref/models/instances.txt:653
# b15c52a5df374948bbe4922066b47461
msgid "An example should make it clear how to use ``permalink()``. Suppose your URLconf contains a line such as::"
msgstr ""

#: ../../ref/models/instances.txt:658
# ddee5a3d8b0c43a6a21df6386779e2f6
msgid "...your model could have a :meth:`~django.db.models.Model.get_absolute_url` method that looked like this::"
msgstr ""

#: ../../ref/models/instances.txt:667
# 420ec318fc534c5c8bb72c515e68ddcf
msgid "Similarly, if you had a URLconf entry that looked like::"
msgstr ""

#: ../../ref/models/instances.txt:671
# 8d40d32aba414f53b75d0f929ec4a5a7
msgid "...you could reference this using ``permalink()`` as follows::"
msgstr ""

#: ../../ref/models/instances.txt:680
# 85b6c3e4088e4571a6bdfeb3a8aecdc8
msgid "Notice that we specify an empty sequence for the second parameter in this case, because we only want to pass keyword parameters, not positional ones."
msgstr ""

#: ../../ref/models/instances.txt:683
# 4bb47637d10940e4ae8d809aa62d551d
msgid "In this way, you're associating the model's absolute path with the view that is used to display it, without repeating the view's URL information anywhere. You can still use the :meth:`~django.db.models.Model.get_absolute_url()` method in templates, as before."
msgstr ""

#: ../../ref/models/instances.txt:688
# 15f7c7e36e10437e915e798779f350e6
msgid "In some cases, such as the use of generic views or the re-use of custom views for multiple models, specifying the view function may confuse the reverse URL matcher (because multiple patterns point to the same view). For that case, Django has :ref:`named URL patterns <naming-url-patterns>`. Using a named URL pattern, it's possible to give a name to a pattern, and then reference the name rather than the view function. A named URL pattern is defined by replacing the pattern tuple by a call to the ``url`` function)::"
msgstr ""

#: ../../ref/models/instances.txt:700
# 61c56fde52744606bd86fa7f45fe6233
msgid "...and then using that name to perform the reverse URL resolution instead of the view name::"
msgstr ""

#: ../../ref/models/instances.txt:709
# 1e9a5f7c9ef44044a9dbc764a17cd2e1
msgid "More details on named URL patterns are in the :doc:`URL dispatch documentation </topics/http/urls>`."
msgstr ""

#: ../../ref/models/instances.txt:713
# 9b26dbf908d640f8baeaeedcc69b6545
msgid "Extra instance methods"
msgstr ""

#: ../../ref/models/instances.txt:715
# 02a8fd80f3c849439a33dfc6a9728008
msgid "In addition to :meth:`~Model.save()`, :meth:`~Model.delete()`, a model object might have some of the following methods:"
msgstr ""

#: ../../ref/models/instances.txt:720
# 20a34d3ee01442f7bd9b47c87e35dd6f
msgid "For every field that has :attr:`~django.db.models.Field.choices` set, the object will have a ``get_FOO_display()`` method, where ``FOO`` is the name of the field. This method returns the \"human-readable\" value of the field."
msgstr ""

#: ../../ref/models/instances.txt:749
# 0267cae94e2844ac87aa6f9b3819b9ed
msgid "For every :class:`~django.db.models.DateField` and :class:`~django.db.models.DateTimeField` that does not have :attr:`null=True <django.db.models.Field.null>`, the object will have ``get_next_by_FOO()`` and ``get_previous_by_FOO()`` methods, where ``FOO`` is the name of the field. This returns the next and previous object with respect to the date field, raising a :exc:`~django.core.exceptions.DoesNotExist` exception when appropriate."
msgstr ""

#: ../../ref/models/instances.txt:756
# 033eb37fc8ad46498e94e4d989891993
msgid "Both of these methods will perform their queries using the default manager for the model. If you need to emulate filtering used by a custom manager, or want to perform one-off custom filtering, both methods also accept optional keyword arguments, which should be in the format described in :ref:`Field lookups <field-lookups>`."
msgstr ""

#: ../../ref/models/instances.txt:762
# d36fdd7309924057a279c3a34cad4580
msgid "Note that in the case of identical date values, these methods will use the primary key as a tie-breaker. This guarantees that no records are skipped or duplicated. That also means you cannot use those methods on unsaved objects."
msgstr ""

