# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../ref/clickjacking.txt:3
# a5d8e8af194843a981146d3091e52673
msgid "Clickjacking Protection"
msgstr ""

#: ../../ref/clickjacking.txt:8
# 441ff6367a28452badb6ab2989216e12
msgid "The clickjacking middleware and decorators provide easy-to-use protection against `clickjacking`_.  This type of attack occurs when a malicious site tricks a user into clicking on a concealed element of another site which they have loaded in a hidden frame or iframe."
msgstr ""

#: ../../ref/clickjacking.txt:16
# e3190cc3095c465087948db73015fa57
msgid "An example of clickjacking"
msgstr ""

#: ../../ref/clickjacking.txt:18
# 352ced498ce540b98d69e99a5eeb9359
msgid "Suppose an online store has a page where a logged in user can click \"Buy Now\" to purchase an item. A user has chosen to stay logged into the store all the time for convenience. An attacker site might create an \"I Like Ponies\" button on one of their own pages, and load the store's page in a transparent iframe such that the \"Buy Now\" button is invisibly overlaid on the \"I Like Ponies\" button. If the user visits the attacker's site, clicking \"I Like Ponies\" will cause an inadvertent click on the \"Buy Now\" button and an unknowing purchase of the item."
msgstr ""

#: ../../ref/clickjacking.txt:29
# f222f6ed68384217960b088291aac95f
msgid "Preventing clickjacking"
msgstr ""

#: ../../ref/clickjacking.txt:31
# d8ad39a498db497191221353b9c07cba
msgid "Modern browsers honor the `X-Frame-Options`_ HTTP header that indicates whether or not a resource is allowed to load within a frame or iframe. If the response contains the header with a value of ``SAMEORIGIN`` then the browser will only load the resource in a frame if the request originated from the same site. If the header is set to ``DENY`` then the browser will block the resource from loading in a frame no matter which site made the request."
msgstr ""

#: ../../ref/clickjacking.txt:40
# 6639a3a116884e398723c943f5f43d05
msgid "Django provides a few simple ways to include this header in responses from your site:"
msgstr ""

#: ../../ref/clickjacking.txt:43
# c1fc75c8d8314a7b8d7dce8ea3884578
msgid "A simple middleware that sets the header in all responses."
msgstr ""

#: ../../ref/clickjacking.txt:45
# 881223855a3143959f4eae73ad1410b6
msgid "A set of view decorators that can be used to override the middleware or to only set the header for certain views."
msgstr ""

#: ../../ref/clickjacking.txt:49
# 3c86c01007b94bf6ac8ffe109f3a808f
msgid "How to use it"
msgstr ""

#: ../../ref/clickjacking.txt:52
# 5cad1be62e0646619fcc158c883a1535
msgid "Setting X-Frame-Options for all responses"
msgstr ""

#: ../../ref/clickjacking.txt:54
# 56f1984abc474eb9a9bfdb146ea609ef
msgid "To set the same ``X-Frame-Options`` value for all responses in your site, put ``'django.middleware.clickjacking.XFrameOptionsMiddleware'`` to :setting:`MIDDLEWARE_CLASSES`::"
msgstr ""

#: ../../ref/clickjacking.txt:66
# 0e64515d55a94b57b406fb9fc4fe610b
msgid "This middleware is enabled in the settings file generated by :djadmin:`startproject`."
msgstr ""

#: ../../ref/clickjacking.txt:69
# 25d2fac8cfe245c69299af32702d41a8
msgid "By default, the middleware will set the ``X-Frame-Options`` header to ``SAMEORIGIN`` for every outgoing ``HttpResponse``. If you want ``DENY`` instead, set the :setting:`X_FRAME_OPTIONS` setting::"
msgstr ""

#: ../../ref/clickjacking.txt:75
# 58079ef0edb140f184d4d312b3123771
msgid "When using the middleware there may be some views where you do **not** want the ``X-Frame-Options`` header set. For those cases, you can use a view decorator that tells the middleware not to set the header::"
msgstr ""

#: ../../ref/clickjacking.txt:88
# 21aecb26dc7543e58e1ce0267b03816b
msgid "Setting X-Frame-Options per view"
msgstr ""

#: ../../ref/clickjacking.txt:90
# 8b65d7e58ee7458498788b02c2b99bed
msgid "To set the ``X-Frame-Options`` header on a per view basis, Django provides these decorators::"
msgstr ""

#: ../../ref/clickjacking.txt:105
# 1c352e6786da451289cc43816fdb46c1
msgid "Note that you can use the decorators in conjunction with the middleware. Use of a decorator overrides the middleware."
msgstr ""

#: ../../ref/clickjacking.txt:109
# 9bb53331502a4d2bbe2b845e8f32e055
msgid "Limitations"
msgstr ""

#: ../../ref/clickjacking.txt:111
# f2efabb384c147dd80a59e9024ebf127
msgid "The ``X-Frame-Options`` header will only protect against clickjacking in a modern browser. Older browsers will quietly ignore the header and need `other clickjacking prevention techniques`_."
msgstr ""

#: ../../ref/clickjacking.txt:116
# a7c922ee78c2473ebc5fd5469bb3e560
msgid "Browsers that support X-Frame-Options"
msgstr ""

#: ../../ref/clickjacking.txt:118
# 6117a0ebe5d54ccaa0933d8160fb42ff
msgid "Internet Explorer 8+"
msgstr ""

#: ../../ref/clickjacking.txt:119
# ecfc7fa2a8d04f1a8f3bede3b865fa77
msgid "Firefox 3.6.9+"
msgstr ""

#: ../../ref/clickjacking.txt:120
# 95ae2b3d1f31472395933f46431ed95b
msgid "Opera 10.5+"
msgstr ""

#: ../../ref/clickjacking.txt:121
# 99dc2aef99544ea2a0a2a3b62fcdfa0e
msgid "Safari 4+"
msgstr ""

#: ../../ref/clickjacking.txt:122
# 2d32ba9bfe714e7eb7d62dd01d5ea365
msgid "Chrome 4.1+"
msgstr ""

#: ../../ref/clickjacking.txt:125
# a6cff6b42365471280aaeddad1b9a4f7
msgid "See also"
msgstr ""

#: ../../ref/clickjacking.txt:127
# b2d75f8d3fc648cba433cb93026b66fa
msgid "A `complete list`_ of browsers supporting ``X-Frame-Options``."
msgstr ""

