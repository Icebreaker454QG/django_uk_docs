# 
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"Last-Translator: Volodymyr Cherepanyak <chervol@gmail.com>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"Content-Transfer-Encoding: 8bit\n"" Content-Type: text/plain; charset=UTF-8\n"
"MIME-Version: 1.0\n"
"Language: uk\n"

# 0fad516dfd3b43c3a7b35a7a2447b217
#: ../../ref/class-based-views/mixins-multiple-object.txt:3
msgid "Multiple object mixins"
msgstr ""

# 44c662da9a0143c590c14be81c8ce339
#: ../../ref/class-based-views/mixins-multiple-object.txt:6
msgid "MultipleObjectMixin"
msgstr ""

# 2792342e9a46426ba6df50db07eacd45
#: ../../ref/class-based-views/mixins-multiple-object.txt:10
msgid "A mixin that can be used to display a list of objects."
msgstr ""

# 9ac0155b0a8b4f5d83cf5df7d8c16653
#: ../../ref/class-based-views/mixins-multiple-object.txt:12
msgid ""
"If ``paginate_by`` is specified, Django will paginate the results returned "
"by this. You can specify the page number in the URL in one of two ways:"
msgstr ""

# 3bd8c0a491af472f969d905c4efb489e
#: ../../ref/class-based-views/mixins-multiple-object.txt:15
msgid ""
"Use the ``page`` parameter in the URLconf. For example, this is what your "
"URLconf might look like::"
msgstr ""

# 750740ea819040988b7ce495774c559c
#: ../../ref/class-based-views/mixins-multiple-object.txt:20
msgid ""
"Pass the page number via the ``page`` query-string parameter. For example, a"
" URL would look like this::"
msgstr ""

# 674f6e1a42da45d9a46db838d391d977
#: ../../ref/class-based-views/mixins-multiple-object.txt:25
msgid ""
"These values and lists are 1-based, not 0-based, so the first page would be "
"represented as page ``1``."
msgstr ""

# 7b80f580a4b3428bbccb96e69c07b6ee
#: ../../ref/class-based-views/mixins-multiple-object.txt:28
msgid ""
"For more on pagination, read the :doc:`pagination documentation "
"</topics/pagination>`."
msgstr ""

# 82a37f7197fa4f29bc09511086bb2ee0
#: ../../ref/class-based-views/mixins-multiple-object.txt:31
msgid ""
"As a special case, you are also permitted to use ``last`` as a value for "
"``page``::"
msgstr ""

# ca56a4359db340348c69edc8c425eddc
#: ../../ref/class-based-views/mixins-multiple-object.txt:36
msgid ""
"This allows you to access the final page of results without first having to "
"determine how many pages there are."
msgstr ""

# d1b36a7de69348d1b9b8c601d1cd333a
#: ../../ref/class-based-views/mixins-multiple-object.txt:39
msgid ""
"Note that ``page`` *must* be either a valid page number or the value "
"``last``; any other value for ``page`` will result in a 404 error."
msgstr ""

# ed7decd5a0594815b464c8085b92b2b7
# b7a8e5f00b8c484e801fd5130059414f
#: ../../ref/class-based-views/mixins-multiple-object.txt:42
#: ../../ref/class-based-views/mixins-multiple-object.txt:197
msgid "**Extends**"
msgstr ""

# 004dc357560041a3884e7a04f1ff0178
#: ../../ref/class-based-views/mixins-multiple-object.txt:44
msgid ":class:`django.views.generic.base.ContextMixin`"
msgstr ""

# 721d4cc6ba144d4f9c0dbd87d06caa26
# a3fcdcfc49d74642abd065206dd34aab
#: ../../ref/class-based-views/mixins-multiple-object.txt:46
#: ../../ref/class-based-views/mixins-multiple-object.txt:201
msgid "**Methods and Attributes**"
msgstr ""

# 746839364b754a2fa6d0b0cf9063a01f
#: ../../ref/class-based-views/mixins-multiple-object.txt:50
msgid ""
"A boolean specifying whether to display the page if no objects are "
"available. If this is ``False`` and no objects are available, the view will "
"raise a 404 instead of displaying an empty page. By default, this is "
"``True``."
msgstr ""

# 286875f8d8b24ce48de3065ea55059bc
#: ../../ref/class-based-views/mixins-multiple-object.txt:57
msgid ""
"The model that this view will display data for. Specifying ``model = Foo`` "
"is effectively the same as specifying ``queryset = Foo.objects.all()``."
msgstr ""

# 9cbcff7096c949d39970344f1485da22
#: ../../ref/class-based-views/mixins-multiple-object.txt:63
msgid ""
"A ``QuerySet`` that represents the objects. If provided, the value of "
"``queryset`` supersedes the value provided for :attr:`model`."
msgstr ""

# e53bd0f379284eb0b426bcbf294010ff
#: ../../ref/class-based-views/mixins-multiple-object.txt:68
msgid ""
"``queryset`` is a class attribute with a *mutable* value so care must be "
"taken when using it directly. Before using it, either call its "
":meth:`~django.db.models.query.QuerySet.all` method or retrieve it with "
":meth:`get_queryset` which takes care of the cloning behind the scenes."
msgstr ""

# 14f4c52fc843495aa93994c93245c8c0
#: ../../ref/class-based-views/mixins-multiple-object.txt:76
msgid ""
"An integer specifying how many objects should be displayed per page. If this"
" is given, the view will paginate objects with ``paginate_by`` objects per "
"page. The view will expect either a ``page`` query string parameter (via "
"``request.GET``) or a ``page`` variable specified in the URLconf."
msgstr ""

# 2ae702024b4d4b7a913c26c5e89ad1da
#: ../../ref/class-based-views/mixins-multiple-object.txt:86
msgid ""
"An integer specifying the number of \"overflow\" objects the last page can "
"contain. This extends the :attr:`paginate_by` limit on the last page by up "
"to ``paginate_orphans``, in order to keep the last page from having a very "
"small number of objects."
msgstr ""

# 897e0a6f716f4bf3a585f14f028209d5
#: ../../ref/class-based-views/mixins-multiple-object.txt:93
msgid ""
"A string specifying the name to use for the page parameter. The view will "
"expect this parameter to be available either as a query string parameter "
"(via ``request.GET``) or as a kwarg variable specified in the URLconf. "
"Defaults to ``page``."
msgstr ""

# a757871dceb0436d9bc56f76d3240aa6
#: ../../ref/class-based-views/mixins-multiple-object.txt:100
msgid ""
"The paginator class to be used for pagination. By default, "
":class:`django.core.paginator.Paginator` is used. If the custom paginator "
"class doesn't have the same constructor interface as "
":class:`django.core.paginator.Paginator`, you will also need to provide an "
"implementation for :meth:`get_paginator`."
msgstr ""

# e58eb5a0b6e64f24b93b68c3a5d02a88
#: ../../ref/class-based-views/mixins-multiple-object.txt:108
msgid "Designates the name of the variable to use in the context."
msgstr ""

# f9c962d9d5ee4c36b853517cc6f7874d
#: ../../ref/class-based-views/mixins-multiple-object.txt:112
msgid ""
"Get the list of items for this view. This must be an iterable and may be a "
"queryset (in which queryset-specific behavior will be enabled)."
msgstr ""

# 58af1b23955b40979f91e08f9c2071a8
#: ../../ref/class-based-views/mixins-multiple-object.txt:117
msgid ""
"Returns a 4-tuple containing (``paginator``, ``page``, ``object_list``, "
"``is_paginated``)."
msgstr ""

# abebcb5f835e488885f0f2749a2c2554
#: ../../ref/class-based-views/mixins-multiple-object.txt:120
msgid ""
"Constructed by paginating ``queryset`` into pages of size ``page_size``. If "
"the request contains a ``page`` argument, either as a captured URL argument "
"or as a GET argument, ``object_list`` will correspond to the objects from "
"that page."
msgstr ""

# b1cb4436876d47b7aea1512ebb5c751b
#: ../../ref/class-based-views/mixins-multiple-object.txt:127
msgid ""
"Returns the number of items to paginate by, or ``None`` for no pagination. "
"By default this simply returns the value of :attr:`paginate_by`."
msgstr ""

# ca741cfd7347491697204143e6124138
#: ../../ref/class-based-views/mixins-multiple-object.txt:133
msgid ""
"Returns an instance of the paginator to use for this view. By default, "
"instantiates an instance of :attr:`paginator_class`."
msgstr ""

# 89bbe3c1dc4544fb842a64bacbb50288
#: ../../ref/class-based-views/mixins-multiple-object.txt:140
msgid ""
"An integer specifying the number of \"overflow\" objects the last page can "
"contain. By default this simply returns the value of "
":attr:`paginate_orphans`."
msgstr ""

# 6c92602d2b584b51bf8521bd63193c32
#: ../../ref/class-based-views/mixins-multiple-object.txt:146
msgid ""
"Return a boolean specifying whether to display the page if no objects are "
"available. If this method returns ``False`` and no objects are available, "
"the view will raise a 404 instead of displaying an empty page. By default, "
"this is ``True``."
msgstr ""

# 51410a484ebb44d49e97519221d86d60
#: ../../ref/class-based-views/mixins-multiple-object.txt:153
msgid ""
"Return the context variable name that will be used to contain the list of "
"data that this view is manipulating. If ``object_list`` is a queryset of "
"Django objects and :attr:`context_object_name` is not set, the context name "
"will be the ``model_name`` of the model that the queryset is composed from, "
"with postfix ``'_list'`` appended. For example, the model ``Article`` would "
"have a context object named ``article_list``."
msgstr ""

# ea0c73ee1e6046f1bfbc98d6e9f53fd9
#: ../../ref/class-based-views/mixins-multiple-object.txt:164
msgid "Returns context data for displaying the list of objects."
msgstr ""

# 9afed612b76c4a74a0f13ff0a5637ed4
#: ../../ref/class-based-views/mixins-multiple-object.txt:166
msgid "**Context**"
msgstr ""

# f7d657daa3ef4ef0b2d120817f25c0ec
#: ../../ref/class-based-views/mixins-multiple-object.txt:168
msgid ""
"``object_list``: The list of objects that this view is displaying. If "
"``context_object_name`` is specified, that variable will also be set in the "
"context, with the same value as ``object_list``."
msgstr ""

# 77737201c79a42c4abe629085ce07ad9
#: ../../ref/class-based-views/mixins-multiple-object.txt:172
msgid ""
"``is_paginated``: A boolean representing whether the results are paginated. "
"Specifically, this is set to ``False`` if no page size has been specified, "
"or if the available objects do not span multiple pages."
msgstr ""

# a1b0b7bf0f144944b48fdc808a69ea0c
#: ../../ref/class-based-views/mixins-multiple-object.txt:177
msgid ""
"``paginator``: An instance of :class:`django.core.paginator.Paginator`. If "
"the page is not paginated, this context variable will be ``None``."
msgstr ""

# 0b361c972c0b458082a0ff2283c2351c
#: ../../ref/class-based-views/mixins-multiple-object.txt:181
msgid ""
"``page_obj``: An instance of :class:`django.core.paginator.Page`. If the "
"page is not paginated, this context variable will be ``None``."
msgstr ""

# 8e4175737d424f7eb3bd028f11afe933
#: ../../ref/class-based-views/mixins-multiple-object.txt:187
msgid "MultipleObjectTemplateResponseMixin"
msgstr ""

# 1315398cccf84924a407841b88d0c585
#: ../../ref/class-based-views/mixins-multiple-object.txt:191
msgid ""
"A mixin class that performs template-based response rendering for views that"
" operate upon a list of object instances. Requires that the view it is mixed"
" with provides ``self.object_list``, the list of object instances that the "
"view is operating on. ``self.object_list`` may be, but is not required to "
"be, a :class:`~django.db.models.query.QuerySet`."
msgstr ""

# bb6b905ec94649aa8fc8b7690b17f145
#: ../../ref/class-based-views/mixins-multiple-object.txt:199
msgid ":class:`~django.views.generic.base.TemplateResponseMixin`"
msgstr ""

# 0febd48ac83549198525f0add06c1b35
#: ../../ref/class-based-views/mixins-multiple-object.txt:205
msgid ""
"The suffix to append to the auto-generated candidate template name. Default "
"suffix is ``_list``."
msgstr ""

# fb801e2935c445578429c9a0fb86952b
#: ../../ref/class-based-views/mixins-multiple-object.txt:210
msgid ""
"Returns a list of candidate template names. Returns the following list:"
msgstr ""

# 1c1683079a5c4ba99d5b1cd02a83985c
#: ../../ref/class-based-views/mixins-multiple-object.txt:212
msgid "the value of ``template_name`` on the view (if provided)"
msgstr ""

# 3a7338d545db4e4ab933a0228760aec2
#: ../../ref/class-based-views/mixins-multiple-object.txt:213
msgid "``<app_label>/<model_name><template_name_suffix>.html``"
msgstr ""
