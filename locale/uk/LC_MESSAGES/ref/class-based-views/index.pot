# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../ref/class-based-views/index.txt:3
# 7cf6acda95af436bbe01bc5e67e7e7f0
msgid "Built-in Class-based views API"
msgstr ""

#: ../../ref/class-based-views/index.txt:5
# e1cedd2e797a42268769a91bf2f3c9be
msgid "Class-based views API reference. For introductory material, see :doc:`/topics/class-based-views/index`."
msgstr ""

#: ../../ref/class-based-views/index.txt:19
# f43102e14c174cf28a4d81491675290b
msgid "Specification"
msgstr ""

#: ../../ref/class-based-views/index.txt:21
# 501eebd49237404d8e96270ad5104789
msgid "Each request served by a class-based view has an independent state; therefore, it is safe to store state variables on the instance (i.e., ``self.foo = 3`` is a thread-safe operation)."
msgstr ""

#: ../../ref/class-based-views/index.txt:25
# fa3a301e5e3343b08f2d741ceaa36c16
msgid "A class-based view is deployed into a URL pattern using the :meth:`~django.views.generic.base.View.as_view()` classmethod::"
msgstr ""

#: ../../ref/class-based-views/index.txt:32
# 984c1f40f1ab47d09b4fc11237cef5d6
msgid "Thread safety with view arguments"
msgstr ""

#: ../../ref/class-based-views/index.txt:34
# 86969a291cf94727a7821585c15b39be
msgid "Arguments passed to a view are shared between every instance of a view. This means that you shouldn't use a list, dictionary, or any other mutable object as an argument to a view. If you do and the shared object is modified, the actions of one user visiting your view could have an effect on subsequent users visiting the same view."
msgstr ""

#: ../../ref/class-based-views/index.txt:40
# aa0a0104b62d4c41b7fd80a4e183547c
msgid "Arguments passed into :meth:`~django.views.generic.base.View.as_view()` will be assigned onto the instance that is used to service a request. Using the previous example, this means that every request on ``MyView`` is able to use ``self.size``. Arguments must correspond to attributes that already exist on the class (return ``True`` on a ``hasattr`` check)."
msgstr ""

#: ../../ref/class-based-views/index.txt:47
# fb184b7424844e418bc92f38fb89066a
msgid "Base vs Generic views"
msgstr ""

#: ../../ref/class-based-views/index.txt:49
# d99943336c3f489196f1578a6b0661e8
msgid "Base class-based views can be thought of as *parent* views, which can be used by themselves or inherited from. They may not provide all the capabilities required for projects, in which case there are Mixins which extend what base views can do."
msgstr ""

#: ../../ref/class-based-views/index.txt:54
# ec032802b93d4578ae04efedbbbc82e1
msgid "Django’s generic views are built off of those base views, and were developed as a shortcut for common usage patterns such as displaying the details of an object. They take certain common idioms and patterns found in view development and abstract them so that you can quickly write common views of data without having to repeat yourself."
msgstr ""

#: ../../ref/class-based-views/index.txt:60
# e5cde8820bf14e3d9b090a83e68acb44
msgid "Most generic views require the ``queryset`` key, which is a ``QuerySet`` instance; see :doc:`/topics/db/queries` for more information about ``QuerySet`` objects."
msgstr ""

