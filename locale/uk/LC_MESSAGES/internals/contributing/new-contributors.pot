# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../internals/contributing/new-contributors.txt:3
# 4d15197453704060a6fb6c78c4bcc88e
msgid "Advice for new contributors"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:5
# 930c4ff40c3f4549be4a1fbf6f7228df
msgid "New contributor and not sure what to do? Want to help but just don't know how to get started? This is the section for you."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:9
# 3e5e3290f7544837ba2e87895b6fefe5
msgid "First steps"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:11
# f461a55c8fc84b47a2da44bb1f10bd38
msgid "Start with these easy tasks to discover Django's development process."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:13
# 5f110d7a17e2426aba2171dba6488b9d
msgid "**Sign the Contributor License Agreement**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:15
# 6cfe55bd3fd547e9ac1d7ec112d7d8fc
msgid "The code that you write belongs to you or your employer. If your contribution is more than one or two lines of code, you need to sign the `CLA`_. See the `Contributor License Agreement FAQ`_ for a more thorough explanation."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:20
# 8b4ff1d50e6149c4b840ade335ddbb5b
msgid "**Triage tickets**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:22
# 79bdc9c3a1144b8197ca90fbe50672a8
msgid "If an `unreviewed ticket`_ reports a bug, try and reproduce it. If you can reproduce it and it seems valid, make a note that you confirmed the bug and accept the ticket. Make sure the ticket is filed under the correct component area. Consider writing a patch that adds a test for the bug's behavior, even if you don't fix the bug itself. See more at :ref:`how-can-i-help-with-triaging`"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:29
# 0d8058d04b2741ca893c194ce7c9e3d4
msgid "**Look for tickets that are accepted and review patches to build familiarity with the codebase and the process**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:32
# 6b9df0f60ccf4668af06966b3302b039
msgid "Mark the appropriate flags if a patch needs docs or tests. Look through the changes a patch makes, and keep an eye out for syntax that is incompatible with older but still supported versions of Python. Run the tests and make sure they pass on your system.  Where possible and relevant, try them out on a database other than SQLite. Leave comments and feedback!"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:38
# 5108ed27c8d54049a9675c6041f11ece
msgid "**Keep old patches up to date**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:40
# 74751975cf5949dd81b3792dfabe5c81
msgid "Oftentimes the codebase will change between a patch being submitted and the time it gets reviewed. Make sure it still applies cleanly and functions as expected. Simply updating a patch is both useful and important! See more on :doc:`writing-code/submitting-patches`."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:45
# 138d25502cee4460b21e3f7909145acd
msgid "**Write some documentation**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:47
# 1336df587af547179c2638a3eac0b133
msgid "Django's documentation is great but it can always be improved. Did you find a typo? Do you think that something should be clarified? Go ahead and suggest a documentation patch! See also the guide on :doc:`writing-documentation`, in particular the tips for :ref:`improving-the-documentation`."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:55
# 6e53b7253ec940e5a5b543ce32d5a30d
msgid "The `reports page`_ contains links to many useful Trac queries, including several that are useful for triaging tickets and reviewing patches as suggested above."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:67
# 3b2dafe96f324be98d234d6fa543c8ee
msgid "Guidelines"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:69
# cd422e674b6d493d9e114d59d2fa68bb
msgid "As a newcomer on a large project, it's easy to experience frustration. Here's some advice to make your work on Django more useful and rewarding."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:72
# 67250d76e92b4e55a6e9ed47b3775d4d
msgid "**Pick a subject area that you care about, that you are familiar with, or that you want to learn about**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:75
# fd35b7d2d32443bd93d79b7982a36465
msgid "You don't already have to be an expert on the area you want to work on; you become an expert through your ongoing contributions to the code."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:78
# 130ab8b52bef475da4508256b9f9ea1a
msgid "**Analyze tickets' context and history**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:80
# 6e16763483264137a1de354358a62ae2
msgid "Trac isn't an absolute; the context is just as important as the words. When reading Trac, you need to take into account who says things, and when they were said. Support for an idea two years ago doesn't necessarily mean that the idea will still have support. You also need to pay attention to who *hasn't* spoken -- for example, if a core team member hasn't been recently involved in a discussion, then a ticket may not have the support required to get into trunk."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:88
# 38f1288a73074f5b910f2c2b55c4905f
msgid "**Start small**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:90
# addc3f3dfa5b46fc8aae8069ca538f6a
msgid "It's easier to get feedback on a little issue than on a big one. See the `easy pickings`_."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:93
# f3a734eef4254d258181e72159fc59a6
msgid "**If you're going to engage in a big task, make sure that your idea has support first**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:96
# 1e9d6fc2eefe4fb8b6d096721db32b88
msgid "This means getting someone else to confirm that a bug is real before you fix the issue, and ensuring that the core team supports a proposed feature before you go implementing it."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:100
# 2549dee5875a42cfa407deb65546f159
msgid "**Be bold! Leave feedback!**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:102
# 790ae32cb592462381dabd54d2781378
msgid "Sometimes it can be scary to put your opinion out to the world and say \"this ticket is correct\" or \"this patch needs work\", but it's the only way the project moves forward. The contributions of the broad Django community ultimately have a much greater impact than that of the core developers. We can't do it without YOU!"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:108
# 66fefedd864f470d9c28b4e2045851ce
msgid "**Err on the side of caution when marking things Ready For Check-in**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:110
# 9e58b9cc1e8e4e598fef3857c05f0479
msgid "If you're really not certain if a ticket is ready, don't mark it as such. Leave a comment instead, letting others know your thoughts.  If you're mostly certain, but not completely certain, you might also try asking on IRC to see if someone else can confirm your suspicions."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:115
# 653b92293ee240d599d10cd652792b67
msgid "**Wait for feedback, and respond to feedback that you receive**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:117
# 5537c360f580453ea11bd972c9219bb7
msgid "Focus on one or two tickets, see them through from start to finish, and repeat. The shotgun approach of taking on lots of tickets and letting some fall by the wayside ends up doing more harm than good."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:121
# bd33ff117f2b4e28a4e31a0187fe2675
msgid "**Be rigorous**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:123
# ded951b94a2e4eedb443ba15d0435d49
msgid "When we say \":pep:`8`, and must have docs and tests\", we mean it. If a patch doesn't have docs and tests, there had better be a good reason. Arguments like \"I couldn't find any existing tests of this feature\" don't carry much weight--while it may be true, that means you have the extra-important job of writing the very first tests for that feature, not that you get a pass from writing tests altogether."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:135
# a3b801fb29f14b7a8f1dbf7b44103bfe
msgid "FAQ"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:137
# 0c897a3446e14a90bcee4320c5ece05b
msgid "**This ticket I care about has been ignored for days/weeks/months! What can I do to get it committed?**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:140
# 9401cbcba0234e9fb9de4f06dba25e0a
msgid "First off, it's not personal. Django is entirely developed by volunteers (even the core developers), and sometimes folks just don't have time. The best thing to do is to send a gentle reminder to the |django-developers| mailing list asking for review on the ticket, or to bring it up in the #django-dev IRC channel."
msgstr ""

#: ../../internals/contributing/new-contributors.txt:146
# ad6897e508974a64ab0e8d83756ba895
msgid "**I'm sure my ticket is absolutely 100% perfect, can I mark it as RFC myself?**"
msgstr ""

#: ../../internals/contributing/new-contributors.txt:149
# a3d7a346e9a64732a4ad6176bdd61920
msgid "Short answer: No. It's always better to get another set of eyes on a ticket. If you're having trouble getting that second set of eyes, see question 1, above."
msgstr ""

