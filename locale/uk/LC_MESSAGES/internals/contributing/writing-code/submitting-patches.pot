# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../internals/contributing/writing-code/submitting-patches.txt:3
# 7e5c4f6217ea4aeebf9bbf2a7ecb81f3
msgid "Submitting patches"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:5
# 84121ec5907444ceb134a4c9474db491
msgid "We're always grateful for patches to Django's code. Indeed, bug reports with associated patches will get fixed *far* more quickly than those without patches."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:10
# 2f6e013016bf4391a48a190abe90f94a
msgid "Typo fixes and trivial documentation changes"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:12
# c779b151c71e4897bba4b1207ad335f2
msgid "If you are fixing a really trivial issue, for example changing a word in the documentation, the preferred way to provide the patch is using GitHub pull requests without a Trac ticket. Trac tickets are still acceptable."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:16
# a3159f833fe14f4b8e4e33e5aaa570b8
msgid "See the :doc:`working-with-git` for more details on how to use pull requests."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:19
# ed5d5926c3204fc195930a1fb0a814ab
msgid "\"Claiming\" tickets"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:21
# 1e118ea36c2d4d66a5ed839526f5ba8d
msgid "In an open-source project with hundreds of contributors around the world, it's important to manage communication efficiently so that work doesn't get duplicated and contributors can be as effective as possible."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:25
# 3fd76c922a9e4c3fa3fd65ffe9263fa5
msgid "Hence, our policy is for contributors to \"claim\" tickets in order to let other developers know that a particular bug or feature is being worked on."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:28
# 1678310014ab4acf80f66e3e8df38ee2
msgid "If you have identified a contribution you want to make and you're capable of fixing it (as measured by your coding ability, knowledge of Django internals and time availability), claim it by following these steps:"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:32
# 6caf749df27442d8b652d2a35695da2d
msgid "`Create an account`_ to use in our ticket system. If you have an account but have forgotten your password, you can reset it using the `password reset page`_."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:36
# f7bafff3b16546668a5f0eb0053e1d74
msgid "If a ticket for this issue doesn't exist yet, create one in our `ticket tracker`_."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:39
# ab6e8a99f23347e7942681751a64080d
msgid "If a ticket for this issue already exists, make sure nobody else has claimed it. To do this, look at the \"Owned by\" section of the ticket. If it's assigned to \"nobody,\" then it's available to be claimed. Otherwise, somebody else is working on this ticket, and you either find another bug/feature to work on, or contact the developer working on the ticket to offer your help."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:46
# 08a5658e330944458af2e31c08373a96
msgid "Log into your account, if you haven't already, by clicking \"Login\" in the upper right of the ticket page."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:49
# d98d642bd0634276a1a821d60b9e3ff9
msgid "Claim the ticket:"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:51
# 15fb35a8516f4aedae027e6f204206c4
msgid "click the \"assign to myself\" radio button under \"Action\" near the bottom of the page,"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:53
# ec194ed617ff42aa8c60f7eb13dc50d3
msgid "then click \"Submit changes.\""
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:56
# 9aa4972c734b42f6bde921a25253c303
msgid "The Django software foundation requests that anyone contributing more than a trivial patch to Django sign and submit a `Contributor License Agreement`_, this ensures that the Django Software Foundation has clear license to all contributions allowing for a clear license for all users."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:66
# 25f390995c7b4090a638f79b0a6dc4dc
msgid "Ticket claimers' responsibility"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:68
# 9653a2407f5d4685a119234a2a1fdb80
msgid "Once you've claimed a ticket, you have a responsibility to work on that ticket in a reasonably timely fashion. If you don't have time to work on it, either unclaim it or don't claim it in the first place!"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:72
# 18cf0aaa984e442595cf510ace21bbc2
msgid "If there's no sign of progress on a particular claimed ticket for a week or two, another developer may ask you to relinquish the ticket claim so that it's no longer monopolized and somebody else can claim it."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:76
# 4839ff9a625945f58b7a1e6d58f54e21
msgid "If you've claimed a ticket and it's taking a long time (days or weeks) to code, keep everybody updated by posting comments on the ticket. If you don't provide regular updates, and you don't respond to a request for a progress report, your claim on the ticket may be revoked."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:81
# 892060123431460abca5f344f7510f6f
msgid "As always, more communication is better than less communication!"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:84
# 97b32202d9cc427ab575302e6efab634
msgid "Which tickets should be claimed?"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:86
# e26feed58c2d45a3bde18d7b37b23402
msgid "Of course, going through the steps of claiming tickets is overkill in some cases."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:89
# cfee3547e02745238887f885bd2ce5c2
msgid "In the case of small changes, such as typos in the documentation or small bugs that will only take a few minutes to fix, you don't need to jump through the hoops of claiming tickets. Just submit your patch and be done with it."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:94
# 3185e9d9523d435a9e957984fd20c531
msgid "Of course, it is *always* acceptable, regardless whether someone has claimed it or not, to submit patches to a ticket if you happen to have a patch ready."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:100
# 4e9726030c294672b9c6752bfe3bb698
msgid "Patch style"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:102
# f56aadcaf50f455ca4ce28c39e9c4611
msgid "Make sure that any contribution you do fulfills at least the following requirements:"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:105
# d10fc41ac7764f8e8f6b9e5e342972af
msgid "The code required to fix a problem or add a feature is an essential part of a patch, but it is not the only part. A good patch should also include a :doc:`regression test <unit-tests>` to validate the behavior that has been fixed and to prevent the problem from arising again. Also, if some tickets are relevant to the code that you've written, mention the ticket numbers in some comments in the test so that one can easily trace back the relevant discussions after your patch gets committed, and the tickets get closed."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:113
# 544b09422e7b4df0a5761398c6dc4667
msgid "If the code associated with a patch adds a new feature, or modifies behavior of an existing feature, the patch should also contain documentation."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:117
# 133e5953925f420c8dace05fe5cb3822
msgid "You can use either GitHub branches and pull requests or direct patches to publish your work. If you use the Git workflow, then you should announce your branch in the ticket by including a link to your branch. When you think your work is ready to be merged in create a pull request."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:122
# e5f3dc4524c04e07a778acfa68f95dd9
msgid "See the :doc:`working-with-git` documentation for mode details."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:124
# 46cc66f2839b44db9a0808ce4d588536
msgid "You can also use patches in Trac. When using this style, follow these guidelines."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:127
# 0ee9d110ebe44746b61f559675f12812
msgid "Submit patches in the format returned by the ``git diff`` command. An exception is for code changes that are described more clearly in plain English than in code. Indentation is the most common example; it's hard to read patches when the only difference in code is that it's indented."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:133
# 7a3d20878b834934ac501f247fdd291a
msgid "Attach patches to a ticket in the `ticket tracker`_, using the \"attach file\" button. Please *don't* put the patch in the ticket description or comment unless it's a single line patch."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:137
# d80804ee85c24c98aaa85620c81f9ce6
msgid "Name the patch file with a ``.diff`` extension; this will let the ticket tracker apply correct syntax highlighting, which is quite helpful."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:140
# 5f4eb05804f34ea08fd396b0117eb8f0
msgid "Regardless of the way you submit your work, follow these steps."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:142
# 1d8c89c8e76d41b08253fcb20fa72003
msgid "Make sure your code matches our :doc:`coding-style`."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:144
# b795c5711d384dc5a4ae0f813d433ebe
msgid "Check the \"Has patch\" box on the ticket details. This will make it obvious that the ticket includes a patch, and it will add the ticket to the `list of tickets with patches`_."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:150
# 12e747d236d948db88067372e5b057a3
msgid "Non-trivial patches"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:152
# 9600d2e0832d41edb5ffb2f7036233aa
msgid "A \"non-trivial\" patch is one that is more than a simple bug fix. It's a patch that introduces Django functionality and makes some sort of design decision."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:155
# 7d40e26a3c8e43628a7dd8b671643d68
msgid "If you provide a non-trivial patch, include evidence that alternatives have been discussed on |django-developers|."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:158
# eff5aa90a74a4151a0105035cb1dfb99
msgid "If you're not sure whether your patch should be considered non-trivial, just ask."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:162
# 2ee12ad3871747d7aedcc4eaf47cc243
msgid "Deprecating a feature"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:164
# 3ae11b77d6ad4130834a314c45099813
msgid "There are a couple reasons that code in Django might be deprecated:"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:166
# 10a0b25386284409a8fa0ff03356d648
msgid "If a feature has been improved or modified in a backwards-incompatible way, the old feature or behavior will be deprecated."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:169
# 33c8481c860e4b749cd4f1c95ae3aa3b
msgid "Sometimes Django will include a backport of a Python library that's not included in a version of Python that Django currently supports. When Django no longer needs to support the older version of Python that doesn't include the library, the library will be deprecated in Django."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:174
# 3feacdca35214bff827a0cc2401a1746
msgid "As the :ref:`deprecation policy<internal-release-deprecation-policy>` describes, the first release of Django that deprecates a feature (``A.B``) should raise a ``RemovedInDjangoXXWarning`` (where XX is the Django version where the feature will be removed) when the deprecated feature is invoked. Assuming we have a good test coverage, these warnings will be shown by the test suite when :ref:`running it <running-unit-tests>` with warnings enabled: ``python -Wall runtests.py``. This is annoying and the output of the test suite should remain clean. Thus, when adding a ``RemovedInDjangoXXWarning`` you need to eliminate or silence any warnings generated when running the tests."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:184
# c82054fea13348eb83ef4b2a54a79d8f
msgid "The first step is to remove any use of the deprecated behavior by Django itself. Next you can silence warnings in tests that actually test the deprecated behavior in one of two ways:"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:188
# 30e81d6a1e1546d1b305457ec676f893
msgid "In a particular test::"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:198
# 25fbf680d7fd4ead81e60fa350d36357
msgid "For an entire test case, ``django.test.utils`` contains three helpful mixins to silence warnings: ``IgnorePendingDeprecationWarningsMixin``, ``IgnoreDeprecationWarningsMixin``, and ``IgnoreAllDeprecationWarningsMixin``. For example::"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:208
# 341771bb289c4806ad7629241a48c1cb
msgid "Finally, there are a couple of updates to Django's documentation to make:"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:210
# c3c11e996b3b477a857e308b773797cb
msgid "If the existing feature is documented, mark it deprecated in documentation using the ``.. deprecated:: A.B`` annotation. Include a short description and a note about the upgrade path if applicable."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:214
# 42ea26848d1b4ffaad60e4a016baa633
msgid "Add a description of the deprecated behavior, and the upgrade path if applicable, to the current release notes (``docs/releases/A.B.txt``) under the \"Features deprecated in A.B\" heading."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:218
# a26b8fd7dad047d08cc86c3583fa3736
msgid "Add an entry in the deprecation timeline (``docs/internals/deprecation.txt``) under the ``A.B+2`` version describing what code will be removed."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:221
# 7b6dac4481524acba9759a3b2c2837f1
msgid "Once you have completed these steps, you are finished with the deprecation. In each major release, all ``RemovedInDjangoXXWarning``\\s matching the new version are removed."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:226
# b0d5e525fc3646b1a77ec70fd584caad
msgid "Javascript patches"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:228
# ee7d30b1c887405e8947640de91ca36e
msgid "Django's admin system leverages the jQuery framework to increase the capabilities of the admin interface. In conjunction, there is an emphasis on admin javascript performance and minimizing overall admin media file size. Serving compressed or \"minified\" versions of javascript files is considered best practice in this regard."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:234
# 627b41d4d95c4b2eba3d2eedd91e35cf
msgid "To that end, patches for javascript files should include both the original code for future development (e.g. ``foo.js``), and a compressed version for production use (e.g. ``foo.min.js``). Any links to the file in the codebase should point to the compressed version."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:240
# 5ca77946fc8040af9d657200c5ca5663
msgid "Compressing JavaScript"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:242
# 52420ab3d84640a596fbe2cc9813d8e0
msgid "To simplify the process of providing optimized javascript code, Django includes a handy python script which should be used to create a \"minified\" version. To run it::"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:248
# 458cbd50fbec48629c6553079ee18f86
msgid "Behind the scenes, ``compress.py`` is a front-end for Google's `Closure Compiler`_ which is written in Java. However, the Closure Compiler library is not bundled with Django directly, so those wishing to contribute complete javascript patches will need to download and install the library independently."
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:254
# b4a4390093a54e238b3d7077aafdf963
msgid "The Closure Compiler library requires Java version 6 or higher (Java 1.6 or higher on Mac OS X. Note that Mac OS X 10.5 and earlier did not ship with Java 1.6 by default, so it may be necessary to upgrade your Java installation before the tool will be functional. Also note that even after upgrading Java, the default ``/usr/bin/java`` command may remain linked to the previous Java binary, so relinking that command may be necessary as well.)"
msgstr ""

#: ../../internals/contributing/writing-code/submitting-patches.txt:261
# d575278095224c8db98a2368861ca62b
msgid "Please don't forget to run ``compress.py`` and include the ``diff`` of the minified scripts when submitting patches for Django's javascript."
msgstr ""

