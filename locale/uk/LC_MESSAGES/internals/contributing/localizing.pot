# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../internals/contributing/localizing.txt:3
# fde0ce4845ec4e3d9a2e248a73521894
msgid "Localizing Django"
msgstr ""

#: ../../internals/contributing/localizing.txt:5
# c44681d3b03c4af1801844c551ee23b3
msgid "Various parts of Django, such as the admin site and validation error messages, are internationalized. This means they display differently depending on each user's language or country. For this, Django uses the same internationalization and localization infrastructure available to Django applications, described in the :doc:`i18n documentation </topics/i18n/index>`."
msgstr ""

#: ../../internals/contributing/localizing.txt:12
# 8abcb421937a4d6cb7489d36ce18b0dc
msgid "Translations"
msgstr ""

#: ../../internals/contributing/localizing.txt:14
# c82856a3e3e64550ae42476eeed5c74d
msgid "Translations are contributed by Django users worldwide. The translation work is coordinated at `Transifex`_."
msgstr ""

#: ../../internals/contributing/localizing.txt:17
# 5a43b97672ad409cbd5f593984ce5165
msgid "If you find an incorrect translation or want to discuss specific translations, go to the `Django project page`_. If you would like to help out with translating or add a language that isn't yet translated, here's what to do:"
msgstr ""

#: ../../internals/contributing/localizing.txt:21
# a964408d93784f59adf5f1b2ea62a35a
msgid "Join the :ref:`Django i18n mailing list <django-i18n-mailing-list>` and introduce yourself."
msgstr ""

#: ../../internals/contributing/localizing.txt:24
# 812ac931848c4589a6589aef2bcb3f46
msgid "Make sure you read the notes about :ref:`specialties-of-django-i18n`."
msgstr ""

#: ../../internals/contributing/localizing.txt:26
# 4830f9ccfb984888a848198c93426e87
msgid "Sign up at `Transifex`_ and visit the `Django project page`_."
msgstr ""

#: ../../internals/contributing/localizing.txt:28
# 1c399e44081944f3859b284dbde846ac
msgid "On the `Django project page`_, choose the language you want to work on, **or** -- in case the language doesn't exist yet -- request a new language team by clicking on the \"Request language\" link and selecting the appropriate language."
msgstr ""

#: ../../internals/contributing/localizing.txt:33
# af40f82183b947d88266bb83b7caf3e7
msgid "Then, click the \"Join this Team\" button to become a member of this team. Every team has at least one coordinator who is responsible to review your membership request. You can of course also contact the team coordinator to clarify procedural problems and handle the actual translation process."
msgstr ""

#: ../../internals/contributing/localizing.txt:39
# 748a4a395ad84b9687b38e35585896aa
msgid "Once you are a member of a team choose the translation resource you want to update on the team page. For example the \"core\" resource refers to the translation catalog that contains all non-contrib translations. Each of the contrib apps also have a resource (prefixed with \"contrib\")."
msgstr ""

#: ../../internals/contributing/localizing.txt:45
# 2d41cf5840f84cf680dd85bcb1422f6f
msgid "For more information about how to use Transifex, read the `Transifex User Guide`_."
msgstr ""

#: ../../internals/contributing/localizing.txt:49
# 65945835757640ecae67d26258db6384
msgid "Formats"
msgstr ""

#: ../../internals/contributing/localizing.txt:51
# 00ed0bac4e8a46bb812199c062e03293
msgid "You can also review ``conf/locale/<locale>/formats.py``. This file describes the date, time and numbers formatting particularities of your locale. See :ref:`format-localization` for details."
msgstr ""

#: ../../internals/contributing/localizing.txt:55
# 1027b47b8ae249728b0a2bc4743e8565
msgid "The format files aren't managed by the use of Transifex. To change them, you must :doc:`create a patch<writing-code/submitting-patches>` against the Django source tree, as for any code change:"
msgstr ""

#: ../../internals/contributing/localizing.txt:59
# 862eb75439954d1aa72951c564584135
msgid "Create a diff against the current Git master branch."
msgstr ""

#: ../../internals/contributing/localizing.txt:61
# 26eeae0f086a46339dbfcc0591c9c4bd
msgid "Open a ticket in Django's ticket system, set its ``Component`` field to ``Translations``, and attach the patch to it."
msgstr ""

#: ../../internals/contributing/localizing.txt:71
# 35d11679df09494d8316b73e62722d2f
msgid "Documentation"
msgstr ""

#: ../../internals/contributing/localizing.txt:73
# 25393b1e501b4b5fa7aef6d83f9cb613
msgid "There is also an opportunity to translate the documentation, though this is a huge undertaking to complete entirely (you have been warned!). We use the same `Transifex tool <https://www.transifex.com/projects/p/django-docs/>`_. The translations will appear at ``https://docs.djangoproject.com/<language_code>/`` when at least the ``docs/intro/*`` files are fully translated in your language."
msgstr ""

