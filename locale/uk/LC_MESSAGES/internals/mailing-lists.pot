# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../internals/mailing-lists.txt:3
# c332b69917a847e5847c8ca56cf9cfd3
msgid "Mailing lists"
msgstr ""

#: ../../internals/mailing-lists.txt:7
# bed0d30f4fb94882bb21e4a7035619e1
msgid "Please report security issues **only** to security@djangoproject.com.  This is a private list only open to long-time, highly trusted Django developers, and its archives are not public. For further details, please see :doc:`our security policies </internals/security>`."
msgstr ""

#: ../../internals/mailing-lists.txt:13
# 44a1a190ba58493799c2bb1c3e76de4a
msgid "Django has several official mailing lists on Google Groups that are open to anyone."
msgstr ""

#: ../../internals/mailing-lists.txt:19
# c1a9baed450b449bbad8264b90ae562d
msgid "``django-users``"
msgstr ""

#: ../../internals/mailing-lists.txt:21
# e726b32117cd4753beb6c0a54536a127
msgid "This is the right place if you are looking to ask any question regarding the installation, usage, or debugging of Django."
msgstr ""

#: ../../internals/mailing-lists.txt:26
# e4467af3e3ab46d3b664530f49cdfb85
msgid "If it's the first time you send an email to this list, your email must be accepted first so don't worry if :ref:`your message does not appear <message-does-not-appear-on-django-users>` instantly."
msgstr ""

#: ../../internals/mailing-lists.txt:30
# ed56d5e91ac242d280284a32c858031f
msgid "`django-users mailing archive`_"
msgstr ""

#: ../../internals/mailing-lists.txt:31
# bd5d43e558dc47a3876e4094420c5f30
msgid "`django-users subscription email address`_"
msgstr ""

#: ../../internals/mailing-lists.txt:32
# 7d79fd7a33824ee79bdbca6e0accc293
msgid "`django-users posting email`_"
msgstr ""

#: ../../internals/mailing-lists.txt:41
# d963fdfc6c3c48f699cc48b1c5584b81
msgid "``django-core-mentorship``"
msgstr ""

#: ../../internals/mailing-lists.txt:43
# 652f8fcad25341499d2895e2b2ea889c
msgid "The Django Core Development Mentorship list is intended to provide a welcoming introductory environment for developers interested in contributing to core Django development."
msgstr ""

#: ../../internals/mailing-lists.txt:47
# 52e32f9162374cffab9a9572823678c8
msgid "`django-core-mentorship mailing archive`_"
msgstr ""

#: ../../internals/mailing-lists.txt:48
# 97ca4261c31f4217a3d58cd66d763b63
msgid "`django-core-mentorship subscription email address`_"
msgstr ""

#: ../../internals/mailing-lists.txt:49
# 0e731dbde3d147e88dd3cc1e3b635ad5
msgid "`django-core-mentorship posting email`_"
msgstr ""

#: ../../internals/mailing-lists.txt:58
# 7fbd99e482f84732b2d9d23c50e29114
msgid "``django-developers``"
msgstr ""

#: ../../internals/mailing-lists.txt:60
# 3847b2bbe56247dba85c718faa994f62
msgid "The discussion about the development of Django itself takes place here."
msgstr ""

#: ../../internals/mailing-lists.txt:64
# 17b8a1f0117548e0b71db8d53029aa1f
msgid "Please make use of :ref:`django-users mailing list <django-users-mailing-list>` if you want to ask for tech support, doing so in this list is inappropriate."
msgstr ""

#: ../../internals/mailing-lists.txt:68
# abbfeb787f1440b08015ef9e71a99a72
msgid "`django-developers mailing archive`_"
msgstr ""

#: ../../internals/mailing-lists.txt:69
# ba688e511e9640fb98644a43d58ef19f
msgid "`django-developers subscription email address`_"
msgstr ""

#: ../../internals/mailing-lists.txt:70
# 880cd69adf8b46ee97313a68acc6bdb5
msgid "`django-developers posting email`_"
msgstr ""

#: ../../internals/mailing-lists.txt:79
# 3c292a8aaa9f449eb7cc525c4aa84605
msgid "``django-i18n``"
msgstr ""

#: ../../internals/mailing-lists.txt:81
# b92bb210793149efa43edbbac875cad3
msgid "This is the place to discuss the internationalization and localization of Django's components."
msgstr ""

#: ../../internals/mailing-lists.txt:84
# 0759adba56954605b2fb6b9d53004ecd
msgid "`django-i18n mailing archive`_"
msgstr ""

#: ../../internals/mailing-lists.txt:85
# 996e5384d75a4c3883440e4cb227d1b8
msgid "`django-i18n subscription email address`_"
msgstr ""

#: ../../internals/mailing-lists.txt:86
# 7de5c0f6fb714e7f8887251a8131aff8
msgid "`django-i18n posting email`_"
msgstr ""

#: ../../internals/mailing-lists.txt:95
# 868957acb9a74a9aacf32d49d0db8665
msgid "``django-announce``"
msgstr ""

#: ../../internals/mailing-lists.txt:97
# 7c36ff33413840aca6c9287b503ee8fd
msgid "A (very) low-traffic list for announcing new releases of Django and important bugfixes."
msgstr ""

#: ../../internals/mailing-lists.txt:100
# bba8f2023ce04c19b7f7a3f706b5a824
msgid "`django-announce mailing archive`_"
msgstr ""

#: ../../internals/mailing-lists.txt:101
# 7a97741e67874917b0e5d7be44446755
msgid "`django-announce subscription email address`_"
msgstr ""

#: ../../internals/mailing-lists.txt:102
# db6e47218de94ec5aba53344f35f6743
msgid "`django-announce posting email`_"
msgstr ""

#: ../../internals/mailing-lists.txt:111
# 7b151e38f1604eda9b306cd680e7b603
msgid "``django-updates``"
msgstr ""

#: ../../internals/mailing-lists.txt:113
# 4bcb826d7972496ab6dc3660cfa7dd8e
msgid "All the ticket updates are mailed automatically to this list, which is tracked by developers and interested community members."
msgstr ""

#: ../../internals/mailing-lists.txt:116
# 9aeb7afbefb148f5a6067b6ad8e709f2
msgid "`django-updates mailing archive`_"
msgstr ""

#: ../../internals/mailing-lists.txt:117
# 687c73142ba749f491d47a32b8d36741
msgid "`django-updates subscription email address`_"
msgstr ""

#: ../../internals/mailing-lists.txt:118
# 1c111787206d4585a738a2c8bd1b247e
msgid "`django-updates posting email`_"
msgstr ""

