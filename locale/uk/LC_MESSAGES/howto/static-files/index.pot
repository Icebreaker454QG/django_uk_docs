# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../howto/static-files/index.txt:3
# 81613fc538054e9a9e42711a1a2a2e39
msgid "Managing static files (CSS, images)"
msgstr ""

#: ../../howto/static-files/index.txt:5
# 69a401d8b1fd428cbf40e0f77a96bc57
msgid "Websites generally need to serve additional files such as images, JavaScript, or CSS. In Django, we refer to these files as \"static files\".  Django provides :mod:`django.contrib.staticfiles` to help you manage them."
msgstr ""

#: ../../howto/static-files/index.txt:9
# d0f0f202f5c9425ab88cadfe1a1c4d61
msgid "This page describes how you can serve these static files."
msgstr ""

#: ../../howto/static-files/index.txt:12
# b97c2b80edc346e993cce2dd3b4970a2
msgid "Configuring static files"
msgstr ""

#: ../../howto/static-files/index.txt:14
# e8c37b4324fb4931b6c4339cf81fa9ee
msgid "Make sure that ``django.contrib.staticfiles`` is included in your :setting:`INSTALLED_APPS`."
msgstr ""

#: ../../howto/static-files/index.txt:17
# c0236dc496974d9bb3a3075e27b23fe5
msgid "In your settings file, define :setting:`STATIC_URL`, for example::"
msgstr ""

#: ../../howto/static-files/index.txt:21
# f275d3805bc246889c3428c2c9abf29f
msgid "In your templates, either hardcode the url like ``/static/my_app/myexample.jpg`` or, preferably, use the :ttag:`static<staticfiles-static>` template tag to build the URL for the given relative path by using the configured :setting:`STATICFILES_STORAGE` storage (this makes it much easier when you want to switch to a content delivery network (CDN) for serving static files)."
msgstr ""

#: ../../howto/static-files/index.txt:35
# 7b3e7cae1d3c4e279ba15cc95aff8920
msgid "Store your static files in a folder called ``static`` in your app. For example ``my_app/static/my_app/myimage.jpg``."
msgstr ""

#: ../../howto/static-files/index.txt:38
# 98b35541094f4ef89b84fc726f9edfe8
msgid "Serving the files"
msgstr ""

#: ../../howto/static-files/index.txt:40
# 71ced3250fe34dac940a8ceee2d238c9
msgid "In addition to these configuration steps, you'll also need to actually serve the static files."
msgstr ""

#: ../../howto/static-files/index.txt:43
# 02516fd6f85b42678ac9e8b2a581b497
msgid "During development, if you use :mod:`django.contrib.staticfiles`, this will be done automatically by :djadmin:`runserver` when :setting:`DEBUG` is set to ``True`` (see :func:`django.contrib.staticfiles.views.serve`)."
msgstr ""

#: ../../howto/static-files/index.txt:47
# 6ceb6d27f67841ef8d9786774f6a7bec
msgid "This method is **grossly inefficient** and probably **insecure**, so it is **unsuitable for production**."
msgstr ""

#: ../../howto/static-files/index.txt:50
# 2308dcbad78d4cf8befb5e3a57d6106a
msgid "See :doc:`/howto/static-files/deployment` for proper strategies to serve static files in production environments."
msgstr ""

#: ../../howto/static-files/index.txt:53
# 42ffa8b9a01540d0b8a3d1182e2c1702
msgid "Your project will probably also have static assets that aren't tied to a particular app. In addition to using a ``static/`` directory inside your apps, you can define a list of directories (:setting:`STATICFILES_DIRS`) in your settings file where Django will also look for static files. For example::"
msgstr ""

#: ../../howto/static-files/index.txt:63
# cb2077395908407d829cdf3273820517
msgid "See the documentation for the :setting:`STATICFILES_FINDERS` setting for details on how ``staticfiles`` finds your files."
msgstr ""

#: ../../howto/static-files/index.txt:66
# b617a829787443d9926169e9f4bcda3c
msgid "Static file namespacing"
msgstr ""

#: ../../howto/static-files/index.txt:68
# 5ec195e6f6bc4e739d8fd22cecd13697
msgid "Now we *might* be able to get away with putting our static files directly in ``my_app/static/`` (rather than creating another ``my_app`` subdirectory), but it would actually be a bad idea. Django will use the first static file it finds whose name matches, and if you had a static file with the same name in a *different* application, Django would be unable to distinguish between them. We need to be able to point Django at the right one, and the easiest way to ensure this is by *namespacing* them. That is, by putting those static files inside *another* directory named for the application itself."
msgstr ""

#: ../../howto/static-files/index.txt:81
# d1aafe6a09f543b28953948e12f4f27f
msgid "Serving static files during development."
msgstr ""

#: ../../howto/static-files/index.txt:83
# 6987285ac0f845b3aa45e214cf7f52e8
msgid "If you use :mod:`django.contrib.staticfiles` as explained above, :djadmin:`runserver` will do this automatically when :setting:`DEBUG` is set to ``True``. If you don't have ``django.contrib.staticfiles`` in :setting:`INSTALLED_APPS`, you can still manually serve static files using the :func:`django.contrib.staticfiles.views.serve` view."
msgstr ""

#: ../../howto/static-files/index.txt:89
#: ../../howto/static-files/index.txt:121
# 05d2e375944e4a04bda2c1bfdf5d72b6
# 01ff40fe547b4f23bc1a26e862d6f867
msgid "This is not suitable for production use! For some common deployment strategies, see :doc:`/howto/static-files/deployment`."
msgstr ""

#: ../../howto/static-files/index.txt:92
# 252b804ab0194493a6f095f434215263
msgid "For example, if your :setting:`STATIC_URL` is defined as ``/static/``, you can do this by adding the following snippet to your urls.py::"
msgstr ""

#: ../../howto/static-files/index.txt:104
# 0ef07f9139ba401ab30fba82d74efadc
msgid "This helper function works only in debug mode and only if the given prefix is local (e.g. ``/static/``) and not a URL (e.g. ``http://static.example.com/``)."
msgstr ""

#: ../../howto/static-files/index.txt:108
# f24dd93c7a4440f886dea09d5385b332
msgid "Also this helper function only serves the actual :setting:`STATIC_ROOT` folder; it doesn't perform static files discovery like :mod:`django.contrib.staticfiles`."
msgstr ""

#: ../../howto/static-files/index.txt:115
# e5531184eb6d49788d0a587b72d2d25f
msgid "Serving files uploaded by a user during development."
msgstr ""

#: ../../howto/static-files/index.txt:117
# 02f7a1c1eb244d548fce93420123e5e5
msgid "During development, you can serve user-uploaded media files from :setting:`MEDIA_ROOT` using the :func:`django.contrib.staticfiles.views.serve` view."
msgstr ""

#: ../../howto/static-files/index.txt:124
# 081f073b186c41a4a39afb9372b6733e
msgid "For example, if your :setting:`MEDIA_URL` is defined as ``/media/``, you can do this by adding the following snippet to your urls.py::"
msgstr ""

#: ../../howto/static-files/index.txt:136
# ff38fdf81ca340d0b0ecc9426736c731
msgid "This helper function works only in debug mode and only if the given prefix is local (e.g. ``/media/``) and not a URL (e.g. ``http://media.example.com/``)."
msgstr ""

#: ../../howto/static-files/index.txt:143
# fc3b5d9538ff4c69a4c48ac6bb901f58
msgid "Testing"
msgstr ""

#: ../../howto/static-files/index.txt:145
# 4498327cbea94a03b49d8e88b7270eac
msgid "When running tests that use actual HTTP requests instead of the built-in testing client (i.e. when using the built-in :class:`LiveServerTestCase <django.test.LiveServerTestCase>`) the static assets need to be served along the rest of the content so the test environment reproduces the real one as faithfully as possible, but ``LiveServerTestCase`` has only very basic static file-serving functionality: It doesn't know about the finders feature of the ``staticfiles`` application and assumes the static content has already been collected under :setting:`STATIC_ROOT`."
msgstr ""

#: ../../howto/static-files/index.txt:154
# c9f827d8845e4725996717315562d5ab
msgid "Because of this, ``staticfiles`` ships its own :class:`django.contrib.staticfiles.testing.StaticLiveServerTestCase`, a subclass of the built-in one that has the ability to transparently serve all the assets during execution of these tests in a way very similar to what we get at development time with ``DEBUG = True``, i.e. without having to collect them using :djadmin:`collectstatic` first."
msgstr ""

#: ../../howto/static-files/index.txt:163
# 32b20b8bd68c4021ad43a4aa33220613
msgid ":class:`django.contrib.staticfiles.testing.StaticLiveServerTestCase` is new in Django 1.7. Previously its functionality was provided by :class:`django.test.LiveServerTestCase`."
msgstr ""

#: ../../howto/static-files/index.txt:168
# ed195dab32ee4b57babb9e93365c4485
msgid "Deployment"
msgstr ""

#: ../../howto/static-files/index.txt:170
# 693beaaa4f394f87b3e824b87a1f56e1
msgid ":mod:`django.contrib.staticfiles` provides a convenience management command for gathering static files in a single directory so you can serve them easily."
msgstr ""

#: ../../howto/static-files/index.txt:173
# 22d041d14688432591ef3b7650b27543
msgid "Set the :setting:`STATIC_ROOT` setting to the directory from which you'd like to serve these files, for example::"
msgstr ""

#: ../../howto/static-files/index.txt:178
# 76936181bac947fdac775f64e00dd1ce
msgid "Run the :djadmin:`collectstatic` management command::"
msgstr ""

#: ../../howto/static-files/index.txt:182
# b081d1c60ac9417985c06bb102a78414
msgid "This will copy all files from your static folders into the :setting:`STATIC_ROOT` directory."
msgstr ""

#: ../../howto/static-files/index.txt:185
# 28a4429cfbfd4abba40439977dfc4e51
msgid "Use a web server of your choice to serve the files. :doc:`/howto/static-files/deployment` covers some common deployment strategies for static files."
msgstr ""

#: ../../howto/static-files/index.txt:190
# ed3bcfecc67241f3a964c6b387c62a9c
msgid "Learn more"
msgstr ""

#: ../../howto/static-files/index.txt:192
# da8a066e4b8b4f75b5f86769d9b31ce0
msgid "This document has covered the basics and some common usage patterns. For complete details on all the settings, commands, template tags, and other pieces included in :mod:`django.contrib.staticfiles`, see :doc:`the staticfiles reference </ref/contrib/staticfiles>`."
msgstr ""

