msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: 2015-05-05 13:46+0200\n"
"Last-Translator: Zoriana Zaiats <sorenabell@quintagroup.com>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: uk\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.7.3\n"

# 7807b72707a141bdb5786e2dd9ae0769
#: ../../faq/contributing.txt:2
msgid "FAQ: Contributing code"
msgstr "ЧАПИ: участь у розвитку Django "

# 3a423ccfb694458799ef8ba19e377586
#: ../../faq/contributing.txt:5
msgid "How can I get started contributing code to Django?"
msgstr "Як розпочати сприяти розвитку проекту?"

# bd8c3b627da843049fe7b7ddb2af7167
#: ../../faq/contributing.txt:7
msgid ""
"Thanks for asking! We've written an entire document devoted to this "
"question. It's titled :doc:`Contributing to Django </internals/contributing/"
"index>`."
msgstr ""
"Спасибі вам за вашу зацікавленість! Даному питанню присвячено окремий "
"документ :doc:`допомога розвитку Django </internals/contributing/index>`."

# c5a3da664ee24cde85837ed5895e561e
#: ../../faq/contributing.txt:11
msgid ""
"I submitted a bug fix in the ticket system several weeks ago. Why are you "
"ignoring my patch?"
msgstr ""
"Кілька тижнів тому я представив на розгляд свій варіант усунення помилки. "
"Чому ви ігноруєте мою пропозицію?"

# 289da895850348f19242533bf1205559
#: ../../faq/contributing.txt:13
msgid "Don't worry: We're not ignoring you!"
msgstr "Не турбуйтеся! Ми вас не ігноруємо."

# 71912ee591ac4df59ca568023e608aa8
#: ../../faq/contributing.txt:15
msgid ""
"It's important to understand there is a difference between \"a ticket is "
"being ignored\" and \"a ticket has not been attended to yet.\" Django's "
"ticket system contains hundreds of open tickets, of various degrees of "
"impact on end-user functionality, and Django's developers have to review "
"and prioritize."
msgstr ""
"Важливо розуміти різницю між двома станами, в якому може знаходиться ваша "
"пропозиція (ticket): \"ігнорується\" і \"ще не прийнято до розгляду\". В "
"системі обробки запитів, що надходять від користувачів, на розгляді "
"знаходяться сотні пропозицій. Вони розрізняються за ступенем впливу на "
"функціонал Django, що надається кінцевим користувачам. Розробники Django "
"повинні переглянути і рангувати за важливістю весь обсяг цих пропозицій."

# 38ab966656a243d6bbbd8e2cdf761f3a
#: ../../faq/contributing.txt:20
msgid ""
"On top of that: the people who work on Django are all volunteers. As a "
"result, the amount of time that we have to work on the framework is limited "
"and will vary from week to week depending on our spare time. If we're busy, "
"we may not be able to spend as much time on Django as we might want."
msgstr ""
"Крім цього, не можна випускати з уваги той факт, що люди, які працюють над "
"розвитком Django, роблять це з власної ініціативи. В результаті цього "
"кількість часу, що відводиться нами на роботу з інструментарієм, обмежена і "
"може різнитися залежно від нашої завантаженості. Якщо ми зайняті, то на "
"розробку Django доводиться приділяти не так багато часу, скільки б нам "
"хотілося."

# 1a1babe67b0c4cf7a880794ac61562eb
#: ../../faq/contributing.txt:25
msgid ""
"The best way to make sure tickets do not get hung up on the way to checkin "
"is to make it dead easy, even for someone who may not be intimately "
"familiar with that area of the code, to understand the problem and verify "
"the fix:"
msgstr ""
"Кращий спосіб добитися того, щоб ваша пропозиція не застрягла на шляху до "
"розгляду - це зробити її гранично простою і зрозумілою. Тоді будь-який "
"учасник, навіть не надто добре знайомий з конкретною частиною коду, зможе "
"зрозуміти суть проблеми та перевірити коректність вашої пропозиції. Ось "
"деякі рекомендації щодо цього: "

# 27a7963fb0684e9aaee0a4de49a5e83b
#: ../../faq/contributing.txt:29
msgid ""
"Are there clear instructions on how to reproduce the bug? If this touches a "
"dependency (such as Pillow/PIL), a contrib module, or a specific database, "
"are those instructions clear enough even for someone not familiar with it?"
msgstr ""
"Чи є вказівки на те, за яких умов виникає помилка? Якщо поява помилки "
"пов'язана з використанням бібліотек Python (таких як Pillow/PIL), модулів "
"Django, що входять до складу ``contrib``, або конкретної СУБД, то чи "
"достатньо зрозумілим є ваш опис?"

# cca65230385144b4bb19caf247dd7190
#: ../../faq/contributing.txt:34
msgid ""
"If there are several patches attached to the ticket, is it clear what each "
"one does, which ones can be ignored and which matter?"
msgstr ""
"Якщо у вашому реченні міститься кілька виправлень, то чи зрозуміле "
"призначення кожного з них окремо? Які з них можуть бути проігноровані, а "
"які становлять суть вашої пропозиції?"

# 4b7505b42d7a422ba681fda567b2e34d
#: ../../faq/contributing.txt:37
msgid ""
"Does the patch include a unit test? If not, is there a very clear "
"explanation why not? A test expresses succinctly what the problem is, and "
"shows that the patch actually fixes it."
msgstr ""
"Чи містить ваше виправлення модульний тест? Якщо не містить, то чому ні? "
"Тест лаконічно відображає суть проблеми і свідчить про те, чи дійсно ваша "
"пропозиція виправляє цю помилку."

# 2ba9ac23623346b8956796ed9ac27002
#: ../../faq/contributing.txt:41
msgid ""
"If your patch stands no chance of inclusion in Django, we won't ignore it "
"-- we'll just close the ticket. So if your ticket is still open, it doesn't "
"mean we're ignoring you; it just means we haven't had time to look at it "
"yet."
msgstr ""
"Якщо ваше виправлення не має шансів бути включеним в Django, ми не буде "
"ігнорувати його - ми просто закриємо вашу пропозицію. Тому, якщо воно все "
"ще перебуває в активному стані, то це не означає, що ми ігноруємо його. Це "
"значить всього лише те, що у нас не було часу на його розгляд."

# db037e20daf04790b8dbc249e9954618
#: ../../faq/contributing.txt:46
msgid "When and how might I remind the core team of a patch I care about?"
msgstr ""
"Коли і як я можу нагадати основній команді розробників про виправлення, які "
"мене цікавлять?"

# c4ca0ba1ab7b4754a7e31222aa5dbb24
#: ../../faq/contributing.txt:48
msgid ""
"A polite, well-timed message to the mailing list is one way to get "
"attention. To determine the right time, you need to keep an eye on the "
"schedule. If you post your message when the core developers are trying to "
"hit a feature deadline or manage a planning phase, you're not going to get "
"the sort of attention you require. However, if you draw attention to a "
"ticket when the core developers are paying particular attention to bugs -- "
"just before a bug fixing sprint, or in the lead up to a beta release for "
"example -- you're much more likely to get a productive response."
msgstr ""
"Ввічливе і своєчасне повідомлення в списку розсилки є одним із способів "
"привернути до себе увагу. Щоб визначити правильний час, необхідно "
"відслідковувати графік розробки. Якщо ви розмістите ваше повідомлення в той "
"час, коли основні розробники намагаються реалізувати необхідні функції до "
"встановленого терміну або зайняті плануванням роботи, то ваше звернення не "
"удостоїться бажаної уваги. Однак, якщо ви нагадаєте про пропозицію в той "
"час, коли основні розробники зайняті виправленням помилок, ви маєте "
"набагато більше шансів отримати продуктивну відповідь."

# 5d1e1d4e4cd84bbb926f814b1b063556
#: ../../faq/contributing.txt:57
msgid ""
"Gentle IRC reminders can also work -- again, strategically timed if "
"possible. During a bug sprint would be a very good time, for example."
msgstr ""
"Ненав'язливі нагадування в IRC, також узгоджені за часом з графіком "
"розробки, можуть принести результат. Наприклад, період усунення помилок "
"може бути дуже вдалим часом для нагадування."

# 7917ba70d31c4217817c21c865c71dd7
#: ../../faq/contributing.txt:60
msgid ""
"Another way to get traction is to pull several related tickets together. "
"When the core developers sit down to fix a bug in an area they haven't "
"touched for a while, it can take a few minutes to remember all the fine "
"details of how that area of code works. If you collect several minor bug "
"fixes together into a similarly themed group, you make an attractive "
"target, as the cost of coming up to speed on an area of code can be spread "
"over multiple tickets."
msgstr ""
"Ще один спосіб залучення уваги - зібрати воєдино кілька пов'язаних "
"пропозицій. Коли основні розробники приступають до виправлення помилок в "
"області, з якою вони не працювали деякий час, декілька хвилин йде на те, "
"щоб згадати всі нюанси цієї конкретної області. Об'єднання декількох "
"незначних виправлень, які стосуються однієї сфери, в групу - досить хороша "
"ідея. Час, що витрачається розробниками на \"занурення\" в конкретну "
"область, скорочується і дозволяє обробити більше число пропозицій, які "
"пов’язані з цією областю."

# cdef49c0aa83423ea002b84972dddb6f
#: ../../faq/contributing.txt:67
msgid ""
"Please refrain from emailing core developers personally, or repeatedly "
"raising the same issue over and over. This sort of behavior will not gain "
"you any additional attention -- certainly not the attention that you need "
"in order to get your pet bug addressed."
msgstr ""
"Будь ласка, утримайтеся від надсилання електронних листів основним "
"розробникам особисто або постійного повторення одного і того ж питання. "
"Така поведінка не забезпечить вам ніякої додаткової уваги зі сторони "
"розробників та не сприятиме розгляду вашої пропозиції."

# f78dc884f54944498beaf3ffd4e065e0
#: ../../faq/contributing.txt:73
msgid "But I've reminded you several times and you keep ignoring my patch!"
msgstr ""
"Але я вже кілька разів нагадував про свою пропозицію, а ви продовжуєте "
"ігнорувати її!"

# 0e7c2cc8a0954e32a249769ea244e285
#: ../../faq/contributing.txt:75
msgid ""
"Seriously - we're not ignoring you. If your patch stands no chance of "
"inclusion in Django, we'll close the ticket. For all the other tickets, we "
"need to prioritize our efforts, which means that some tickets will be "
"addressed before others."
msgstr ""
"Ми не ігноруємо вас, справді. Якщо ваше виправлення не має ніяких шансів на "
"включення до Django, ми закриємо його. Для всіх інших пропозицій, ми "
"повинні визначити їх важливість. Це означає, що деякі пропозиції будуть "
"розглянуті раніше за інші."

# 3de2481ee8f947c4ac94bc5fed98f582
#: ../../faq/contributing.txt:80
msgid ""
"One of the criteria that is used to prioritize bug fixes is the number of "
"people that will likely be affected by a given bug. Bugs that have the "
"potential to affect many people will generally get priority over those that "
"are edge cases."
msgstr ""
"Одним із критеріїв, використовуваних для пріоритизування пропозицій, є "
"кількість користувачів, які можуть постраждати від конкретної хиби. Хиби, "
"які потенційно можуть негативно вплинути на велике число людей, отримують "
"пріоритет над тими, які зачіпають незначну кількість користувачів."

# 5f891b6f3741496f9e1156aab09a25cd
#: ../../faq/contributing.txt:85
msgid ""
"Another reason that bugs might be ignored for while is if the bug is a "
"symptom of a larger problem. While we can spend time writing, testing and "
"applying lots of little patches, sometimes the right solution is to "
"rebuild. If a rebuild or refactor of a particular component has been "
"proposed or is underway, you may find that bugs affecting that component "
"will not get as much attention. Again, this is just a matter of "
"prioritizing scarce resources. By concentrating on the rebuild, we can "
"close all the little bugs at once, and hopefully prevent other little bugs "
"from appearing in the future."
msgstr ""
"Іншою причиною того, що помилки можуть ігноруватися протягом деякого часу, "
"є те, що вони всього лише свідчать про більш серйозну проблему. Іноді, "
"замість того, щоб витрачати час на написання, тестування і включення до "
"складу Django безлічі невеликих виправлень, правильним рішенням є зміна "
"структури на більш глибокому рівні. Якщо розглядається або вже реалізується "
"рішення щодо зміни або рефакторингу коду конкретного компонента Django, то "
"ви можете виявити, що пропозиції щодо виправлення помилок у цьому "
"компоненті не отримуватимуть належної уваги. Знову ж таки, це всього лише "
"питання розподілу обмежених ресурсів. Концентруючись на зміні структури, ми "
"можемо позбутися всіх несуттєвих помилок відразу, і, можливо, запобігти "
"появі їх в майбутньому."

# a8e5957d001f484db1b11b244d0568dc
#: ../../faq/contributing.txt:94
msgid ""
"Whatever the reason, please keep in mind that while you may hit a "
"particular bug regularly, it doesn't necessarily follow that every single "
"Django user will hit the same bug. Different users use Django in different "
"ways, stressing different parts of the code under different conditions. "
"When we evaluate the relative priorities, we are generally trying to "
"consider the needs of the entire community, not just the severity for one "
"particular user. This doesn't mean that we think your problem is "
"unimportant -- just that in the limited time we have available, we will "
"always err on the side of making 10 people happy rather than making 1 "
"person happy."
msgstr ""
"Будь ласка, майте на увазі, що ви регулярно можете зустрічати певну "
"помилку, але з цього не обов'язково випливає, що кожен користувач Django "
"отримує ту ж саму помилку. Різні користувачі використовують Django по-"
"різному, використовуючи різні компоненти в різних умовах. При розставленні "
"відносних пріоритетів ми, як правило, намагаємося врахувати потреби всієї "
"спільноти в цілому, а не окремих користувачів. Це не означає, що ми думаємо "
"ніби ваша пропозиція неістотна. В умовах обмеженості наявного у нас часу, "
"ми дотримуємося думки, що краще задовольнити потреби 10 людей, а не однієї "
"конкретної."
